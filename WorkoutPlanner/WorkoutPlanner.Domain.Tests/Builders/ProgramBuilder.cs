﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoFixture;
using WorkoutPlanner.Domain.Programs;
using WorkoutPlanner.Domain.Providers;
using WorkoutPlanner.Domain.Workouts;

namespace WorkoutPlanner.Domain.Builders
{
    public sealed class ProgramBuilder : EntityBuilder<ProgramBuilder, IProgram>
    {
        private Lazy<string> _sourceUrl = new Lazy<string>(default(string));
        private Lazy<string> _imageUrl = new Lazy<string>(default(string));
        private Lazy<IProvider> _provider = new Lazy<IProvider>(default(IProvider));
        private Lazy<IEnumerable<IWorkout>> _workouts = new Lazy<IEnumerable<IWorkout>>(default(IEnumerable<IWorkout>));


        public static ProgramBuilder Typical()
        {
            return Simple()
                   .WithSourceUrl(Fixture.Create<string>())
                   .WithImageUrl(Fixture.Create<string>())
                   .WithProvider((IProvider) null)
                   .WithWorkouts(
                       new[]
                       {
                           WorkoutBuilder.Typical().Build(),
                           WorkoutBuilder.Typical().Build(),
                           WorkoutBuilder.Typical().Build()
                       }
                   );
        }


        protected override IProgram GenerateInstance()
        {
            var instance = new Program
            {
                Name = Name.Value,
                SourceUrl = _sourceUrl.Value,
                ImageUrl = _imageUrl.Value,
                Provider = _provider.Value
            };

            foreach (var workout in _workouts.Value ?? Enumerable.Empty<IWorkout>())
                instance.AddWorkout(workout);

            return instance;
        }


        protected override void PostBuild(IProgram instance)
        {
            base.PostBuild(instance);
            instance.Provider?.AddProgram(instance);
        }


        public ProgramBuilder WithSourceUrl(string value)
        {
            return WithSourceUrl(() => value);
        }


        public ProgramBuilder WithSourceUrl(Func<string> func)
        {
            _sourceUrl = new Lazy<string>(func);
            return this;
        }


        public ProgramBuilder WithImageUrl(string value)
        {
            return WithImageUrl(() => value);
        }


        public ProgramBuilder WithImageUrl(Func<string> func)
        {
            _imageUrl = new Lazy<string>(func);
            return this;
        }


        public ProgramBuilder WithProvider(IProvider value)
        {
            return WithProvider(() => value);
        }


        public ProgramBuilder WithProvider(Func<IProvider> func)
        {
            _provider = new Lazy<IProvider>(func);
            return this;
        }


        public ProgramBuilder WithWorkouts(IEnumerable<IWorkout> value)
        {
            return WithWorkouts(() => value);
        }


        public ProgramBuilder WithWorkouts(Func<IEnumerable<IWorkout>> func)
        {
            _workouts = new Lazy<IEnumerable<IWorkout>>(func);
            return this;
        }
    }
}
