﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoFixture;
using WorkoutPlanner.Domain.Programs;
using WorkoutPlanner.Domain.Providers;

namespace WorkoutPlanner.Domain.Builders
{
    public sealed class ProviderBuilder : EntityBuilder<ProviderBuilder, IProvider>
    {
        private Lazy<string> _sourceUrl = new Lazy<string>(default(string));
        private Lazy<string> _imageUrl = new Lazy<string>(default(string));
        private Lazy<IList<IProgram>> _programs = new Lazy<IList<IProgram>>(default(IList<IProgram>));


        public static ProviderBuilder Typical()
        {
            return Simple()
                   .WithSourceUrl(Fixture.Create<string>())
                   .WithImageUrl(Fixture.Create<string>())
                   .WithPrograms(
                       new List<IProgram>
                       {
                           ProgramBuilder.Typical().Build(),
                           ProgramBuilder.Typical().Build(),
                           ProgramBuilder.Typical().Build()
                       }
                   );
        }


        protected override IProvider GenerateInstance()
        {
            var instance = new Provider
            {
                Name = Name.Value,
                SourceUrl = _sourceUrl.Value,
                ImageUrl = _imageUrl.Value
            };

            foreach (var program in _programs.Value ?? Enumerable.Empty<IProgram>())
                instance.AddProgram(program);

            return instance;
        }


        public ProviderBuilder WithSourceUrl(string value)
        {
            return WithSourceUrl(() => value);
        }


        public ProviderBuilder WithSourceUrl(Func<string> func)
        {
            _sourceUrl = new Lazy<string>(func);
            return this;
        }


        public ProviderBuilder WithImageUrl(string value)
        {
            return WithImageUrl(() => value);
        }


        public ProviderBuilder WithImageUrl(Func<string> func)
        {
            _imageUrl = new Lazy<string>(func);
            return this;
        }


        public ProviderBuilder WithPrograms(IList<IProgram> value)
        {
            return WithPrograms(() => value);
        }


        public ProviderBuilder WithPrograms(Func<IList<IProgram>> func)
        {
            _programs = new Lazy<IList<IProgram>>(func);
            return this;
        }
    }
}
