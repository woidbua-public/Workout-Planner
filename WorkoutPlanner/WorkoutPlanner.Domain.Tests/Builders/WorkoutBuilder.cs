﻿using System;
using AutoFixture;
using WorkoutPlanner.Domain.Programs;
using WorkoutPlanner.Domain.Workouts;

namespace WorkoutPlanner.Domain.Builders
{
    public sealed class WorkoutBuilder : EntityBuilder<WorkoutBuilder, IWorkout>
    {
        private Lazy<DateTime> _date = new Lazy<DateTime>(default(DateTime));
        private Lazy<string> _content = new Lazy<string>(default(string));
        private Lazy<string> _url = new Lazy<string>(default(string));
        private Lazy<string> _sourceUrl = new Lazy<string>(default(string));
        private Lazy<string> _imageUrl = new Lazy<string>(default(string));
        private Lazy<IProgram> _program = new Lazy<IProgram>(default(IProgram));


        public new static WorkoutBuilder Simple()
        {
            return EntityBuilder<WorkoutBuilder, IWorkout>.Simple()
                                                          .WithDate(Fixture.Create<DateTime>().Date)
                                                          .WithContent(Fixture.Create<string>())
                                                          .WithUrl(Fixture.Create<string>())
                                                          .WithSourceUrl(Fixture.Create<string>())
                                                          .WithImageUrl(Fixture.Create<string>());
        }


        public static WorkoutBuilder Typical()
        {
            return Simple().WithProgram((IProgram) null);
        }


        protected override IWorkout GenerateInstance()
        {
            return new Workout
            {
                Name = Name.Value,
                Date = _date.Value,
                Content = _content.Value,
                Url = _url.Value,
                SourceUrl = _sourceUrl.Value,
                ImageUrl = _imageUrl.Value,
                Program = _program.Value
            };
        }


        protected override void PostBuild(IWorkout instance)
        {
            base.PostBuild(instance);
            instance.Program?.AddWorkout(instance);
        }


        public WorkoutBuilder WithDate(Func<DateTime> func)
        {
            _date = new Lazy<DateTime>(func);
            return this;
        }


        public WorkoutBuilder WithDate(DateTime value)
        {
            return WithDate(() => value);
        }


        public WorkoutBuilder WithContent(string value)
        {
            return WithContent(() => value);
        }


        public WorkoutBuilder WithContent(Func<string> func)
        {
            _content = new Lazy<string>(func);
            return this;
        }


        public WorkoutBuilder WithUrl(string value)
        {
            return WithUrl(() => value);
        }


        public WorkoutBuilder WithUrl(Func<string> func)
        {
            _url = new Lazy<string>(func);
            return this;
        }


        public WorkoutBuilder WithSourceUrl(string value)
        {
            return WithSourceUrl(() => value);
        }


        public WorkoutBuilder WithSourceUrl(Func<string> func)
        {
            _sourceUrl = new Lazy<string>(func);
            return this;
        }


        public WorkoutBuilder WithImageUrl(string value)
        {
            return WithImageUrl(() => value);
        }


        public WorkoutBuilder WithImageUrl(Func<string> func)
        {
            _imageUrl = new Lazy<string>(func);
            return this;
        }


        public WorkoutBuilder WithProgram(IProgram value)
        {
            return WithProgram(() => value);
        }


        public WorkoutBuilder WithProgram(Func<IProgram> func)
        {
            _program = new Lazy<IProgram>(func);
            return this;
        }
    }
}
