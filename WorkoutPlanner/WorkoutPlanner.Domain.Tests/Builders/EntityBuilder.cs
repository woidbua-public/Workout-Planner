﻿using System;
using AutoFixture;
using WorkoutPlanner.Core.Fixtures.Builders;
using WorkoutPlanner.Domain.Common;

namespace WorkoutPlanner.Domain.Builders
{
    public abstract class EntityBuilder<TBuilder, TEntity> : Builder<TEntity>
        where TEntity : IEntity<TEntity> where TBuilder : EntityBuilder<TBuilder, TEntity>, new()
    {
        protected Lazy<string> Name { get; private set; } = new Lazy<string>(default(string));


        public static TBuilder Default()
        {
            return new TBuilder();
        }


        public static TBuilder Simple()
        {
            return Default().WithName(Fixture.Create<string>());
        }


        public TBuilder WithName(string value)
        {
            return WithName(() => value);
        }


        public TBuilder WithName(Func<string> func)
        {
            Name = new Lazy<string>(func);
            return (TBuilder) this;
        }
    }
}
