﻿using System.Collections.Generic;
using FluentAssertions;
using NUnit.Framework;
using WorkoutPlanner.Domain.Builders;
using WorkoutPlanner.Domain.Programs;

namespace WorkoutPlanner.Domain.Providers
{
    [TestFixture]
    public class ProviderTests
    {
        [SetUp]
        public void SetUp()
        {
            _sut = ProviderBuilder.Default().Build();
        }


        private IProvider _sut;


        [Test]
        public void IdShouldBeGeneratedCorrectly()
        {
            // Arrange
            const string name = "Name Slug";
            const string expected = "name-slug";

            // Act
            _sut.Name = name;

            // Assert
            _sut.Id.Should().Be(expected);
        }


        [Test]
        public void IdShouldBeGeneratedCorrectlyWhenNameNotGiven()
        {
            // Arrange

            // Act

            // Assert
            _sut.Id.Should().BeEmpty();
        }


        [Test]
        public void NameShouldBeSet()
        {
            // Arrange
            const string expected = "Invictus";

            // Act
            _sut.Name = expected;

            // Assert
            _sut.Name.Should().Be(expected);
        }


        [Test]
        public void SourceUrlShouldBeSet()
        {
            // Arrange
            const string expected = "sourceUrl";

            // Act
            _sut.SourceUrl = expected;

            // Assert
            _sut.SourceUrl.Should().Be(expected);
        }


        [Test]
        public void ImageUrlShouldBeSet()
        {
            // Arrange
            const string expected = "imageUrl";

            // Act
            _sut.ImageUrl = expected;

            // Assert
            _sut.ImageUrl.Should().Be(expected);
        }


        [Test]
        public void ProgramsShouldBeInitialized()
        {
            // Arrange

            // Act

            // Assert
            _sut.Programs.Should().BeEmpty();
        }


        [Test]
        public void ProgramsShouldBeAdded()
        {
            // Arrange
            var programs = new List<IProgram>
            {
                ProgramBuilder.Typical().Build(),
                ProgramBuilder.Typical().Build(),
                ProgramBuilder.Typical().Build()
            };

            // Act
            programs.ForEach(p => _sut.AddProgram(p));

            // Assert
            for (var i = 0; i < _sut.Programs.Count; i++)
            {
                _sut.Programs[i].Provider.Should().Be(_sut);
                _sut.Programs[i].Name.Should().Be(programs[i].Name);
            }
        }


        [Test]
        public void ProgramsShouldBeCleared()
        {
            // Arrange
            var programs = new List<IProgram>
            {
                ProgramBuilder.Typical().Build(),
                ProgramBuilder.Typical().Build(),
                ProgramBuilder.Typical().Build()
            };

            // Act
            programs.ForEach(p => _sut.AddProgram(p));
            _sut.ClearPrograms();

            // Assert
            _sut.Programs.Should().BeEmpty();
        }


        [Test]
        public void CloneShouldCreateCorrectCopy()
        {
            // Arrange
            var provider = ProviderBuilder.Typical().Build();

            // Act
            var actual = provider.Clone();

            // Assert
            actual.Should().BeEquivalentTo(provider);
        }
    }
}
