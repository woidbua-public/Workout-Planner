﻿using System;
using FluentAssertions;
using NUnit.Framework;
using WorkoutPlanner.Domain.Builders;

namespace WorkoutPlanner.Domain.Workouts
{
    [TestFixture]
    public class WorkoutTests
    {
        [SetUp]
        public void SetUp()
        {
            _sut = WorkoutBuilder.Default().Build();
        }


        private IWorkout _sut;


        [Test]
        public void IdShouldBeGeneratedCorrectly()
        {
            // Arrange
            const string title = "    Title Slug";
            var date = new DateTime(2020, 11, 18);
            var program = ProgramBuilder.Default().WithName("Program Title").Build();
            const string expected = "program-title-18112020-title-slug";

            // Act
            _sut.Name = title;
            _sut.Date = date;
            _sut.Program = program;

            // Assert
            _sut.Id.Should().Be(expected);
        }


        [Test]
        public void IdShouldBeGeneratedCorrectlyWhenTitleNotGiven()
        {
            // Arrange
            var date = new DateTime(2020, 11, 18);
            var program = ProgramBuilder.Default().WithName("Program Title").Build();
            const string expected = "program-title-18112020";

            // Act
            _sut.Date = date;
            _sut.Program = program;

            // Assert
            _sut.Id.Should().Be(expected);
        }


        [Test]
        public void IdShouldBeGeneratedCorrectlyWhenDateNotGiven()
        {
            // Arrange
            const string title = "Title Slug";
            var program = ProgramBuilder.Default().WithName("Program Title").Build();
            const string expected = "program-title-title-slug";

            // Act
            _sut.Name = title;
            _sut.Program = program;

            // Assert
            _sut.Id.Should().Be(expected);
        }


        [Test]
        public void IdShouldBeGeneratedCorrectlyWhenProgramNotGiven()
        {
            // Arrange
            const string title = "Title Slug";
            var date = new DateTime(2020, 11, 18);
            const string expected = "18112020-title-slug";

            // Act
            _sut.Name = title;
            _sut.Date = date;

            // Assert
            _sut.Id.Should().Be(expected);
        }


        [Test]
        public void IdShouldBeGeneratedCorrectlyWhenNothingGiven()
        {
            // Arrange

            // Act

            // Assert
            _sut.Id.Should().BeEmpty();
        }


        [Test]
        public void NameShouldBeSet()
        {
            // Arrange
            const string expected = "New Title";

            // Act
            _sut.Name = expected;

            // Assert
            _sut.Name.Should().Be(expected);
        }


        [Test]
        public void DateShouldBeSet()
        {
            // Arrange
            var expected = new DateTime(2020, 01, 01);

            // Act
            _sut.Date = expected;

            // Assert
            _sut.Date.Should().Be(expected);
        }


        [Test]
        public void ContentShouldBeSet()
        {
            // Arrange
            const string expected = "This is some content.";

            // Act
            _sut.Content = expected;

            // Assert
            _sut.Content.Should().Be(expected);
        }


        [Test]
        public void UrlShouldBeSet()
        {
            // Arrange
            const string expected = "https://sample.com";

            // Act
            _sut.Url = expected;

            // Assert
            _sut.Url.Should().Be(expected);
        }


        [Test]
        public void SourceUrlShouldBeSet()
        {
            // Arrange
            const string expected = "sourceUrl";

            // Act
            _sut.SourceUrl = expected;

            // Assert
            _sut.SourceUrl.Should().Be(expected);
        }


        [Test]
        public void ImageUrlShouldBeSet()
        {
            // Arrange
            const string expected = "imageUrl";

            // Act
            _sut.ImageUrl = expected;

            // Assert
            _sut.ImageUrl.Should().Be(expected);
        }


        [Test]
        public void ProgramShouldNotBeInitialized()
        {
            // Arrange

            // Act

            // Assert
            _sut.Program.Should().BeNull();
        }


        [Test]
        public void ProgramShouldBeSet()
        {
            // Arrange
            var expected = ProgramBuilder.Simple().Build();

            // Act
            _sut.Program = expected;

            // Assert
            _sut.Program.Should().Be(expected);
        }


        [Test]
        public void CloneShouldCreateCorrectCopy()
        {
            // Arrange
            var provider = ProviderBuilder.Simple().Build();
            var program = ProgramBuilder.Simple().WithProvider(provider).Build();
            var workout = WorkoutBuilder.Typical().WithProgram(program).Build();

            // Act
            var actual = workout.Clone();

            // Assert
            actual.Should().BeEquivalentTo(workout);
        }
    }
}
