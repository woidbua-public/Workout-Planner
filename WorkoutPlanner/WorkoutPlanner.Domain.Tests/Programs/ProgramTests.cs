﻿using System.Collections.Generic;
using FluentAssertions;
using NUnit.Framework;
using WorkoutPlanner.Domain.Builders;
using WorkoutPlanner.Domain.Workouts;

namespace WorkoutPlanner.Domain.Programs
{
    [TestFixture]
    public class ProgramTests
    {
        [SetUp]
        public void SetUp()
        {
            _sut = ProgramBuilder.Default().Build();
        }


        private IProgram _sut;


        [Test]
        public void IdShouldBeGeneratedCorrectly()
        {
            // Arrange
            const string title = "Title Slug";
            var provider = ProviderBuilder.Default().WithName("Provider Name").Build();
            const string expected = "provider-name-title-slug";

            // Act
            _sut.Name = title;
            _sut.Provider = provider;

            // Assert
            _sut.Id.Should().Be(expected);
        }


        [Test]
        public void IdShouldBeGeneratedCorrectlyWhenTitleNotGiven()
        {
            // Arrange
            var provider = ProviderBuilder.Default().WithName("Provider Name").Build();
            const string expected = "provider-name";

            // Act
            _sut.Provider = provider;

            // Assert
            _sut.Id.Should().Be(expected);
        }


        [Test]
        public void IdShouldBeGeneratedCorrectlyWhenProviderNotGiven()
        {
            // Arrange
            const string title = "Title Slug     ";
            const string expected = "title-slug";

            // Act
            _sut.Name = title;

            // Assert
            _sut.Id.Should().Be(expected);
        }


        [Test]
        public void IdShouldBeGeneratedCorrectlyWhenNothingGiven()
        {
            // Arrange

            // Act

            // Assert
            _sut.Id.Should().BeEmpty();
        }


        [Test]
        public void NameShouldBeSet()
        {
            // Arrange
            const string expected = "Competition Program";

            // Act
            _sut.Name = expected;

            // Assert
            _sut.Name.Should().Be(expected);
        }


        [Test]
        public void SourceUrlShouldBeSet()
        {
            // Arrange
            const string expected = "sourceUrl";

            // Act
            _sut.SourceUrl = expected;

            // Assert
            _sut.SourceUrl.Should().Be(expected);
        }


        [Test]
        public void ImageUrlShouldBeSet()
        {
            // Arrange
            const string expected = "imageUrl";

            // Act
            _sut.ImageUrl = expected;

            // Assert
            _sut.ImageUrl.Should().Be(expected);
        }


        [Test]
        public void ProviderShouldNotBeInitialized()
        {
            // Arrange

            // Act

            // Assert
            _sut.Provider.Should().BeNull();
        }


        [Test]
        public void ProviderShouldBeSet()
        {
            // Arrange
            var expected = ProviderBuilder.Simple().Build();

            // Act
            _sut.Provider = expected;

            // Assert
            _sut.Provider.Should().Be(expected);
        }


        [Test]
        public void WorkoutsShouldBeInitialized()
        {
            // Arrange

            // Act

            // Assert
            _sut.Workouts.Should().BeEmpty();
        }


        [Test]
        public void WorkoutsShouldBeSet()
        {
            // Arrange
            var workouts = new List<IWorkout>
            {
                WorkoutBuilder.Simple().Build(),
                WorkoutBuilder.Simple().Build(),
                WorkoutBuilder.Simple().Build()
            };

            // Act
            workouts.ForEach(w => _sut.AddWorkout(w));
            _sut.ClearWorkouts();

            // Assert
            _sut.Workouts.Should().BeEmpty();
        }


        [Test]
        public void WorkoutsShouldBeCleared()
        {
            // Arrange
            var workouts = new List<IWorkout>
            {
                WorkoutBuilder.Simple().Build(),
                WorkoutBuilder.Simple().Build(),
                WorkoutBuilder.Simple().Build()
            };

            // Act
            workouts.ForEach(w => _sut.AddWorkout(w));

            // Assert
            for (var i = 0; i < _sut.Workouts.Count; i++)
            {
                _sut.Workouts[i].Program.Should().Be(_sut);
                _sut.Workouts[i].Name.Should().Be(workouts[i].Name);
            }
        }


        [Test]
        public void CloneShouldCreateCorrectCopy()
        {
            // Arrange
            var provider = ProviderBuilder.Simple().Build();
            var program = ProgramBuilder.Typical().WithProvider(provider).Build();

            // Act
            var actual = program.Clone();

            // Assert
            actual.Should().BeEquivalentTo(program);
        }
    }
}
