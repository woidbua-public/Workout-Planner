﻿using AutoMapper;
using WorkoutPlanner.Domain.Programs;
using WorkoutPlanner.Domain.Providers;
using WorkoutPlanner.Domain.Workouts;
using WorkoutPlanner.Persistence.Entities.Firestore;

namespace WorkoutPlanner.Persistence.Mappings.Firestore
{
    public sealed class FirestoreMapping : IFirestoreMapping
    {
        private readonly Mapper _mapper;


        public FirestoreMapping()
        {
            _mapper = CreateMapper();
        }


        public FirestoreProvider ProviderToFirestore(IProvider provider)
        {
            return _mapper.Map<FirestoreProvider>(provider);
        }


        public IProvider FirestoreToProvider(FirestoreProvider provider)
        {
            return _mapper.Map<Provider>(provider);
        }


        public FirestoreProgram ProgramToFirestore(IProgram program)
        {
            return _mapper.Map<FirestoreProgram>(program);
        }


        public IProgram FirestoreToProgram(FirestoreProgram program)
        {
            return _mapper.Map<Program>(program);
        }


        public FirestoreWorkout WorkoutToFirestore(IWorkout workout)
        {
            return _mapper.Map<FirestoreWorkout>(workout);
        }


        public IWorkout FirestoreToWorkout(FirestoreWorkout workout)
        {
            return _mapper.Map<Workout>(workout);
        }


        private static Mapper CreateMapper()
        {
            var config = new MapperConfiguration(
                cfg =>
                {
                    cfg.CreateMap<Provider, FirestoreProvider>().ReverseMap();
                    cfg.CreateMap<Program, FirestoreProgram>()
                       .ForMember(
                           dest => dest.Provider,
                           src => src.MapFrom(
                               p => new FirestoreProviderInfo {Id = p.Provider.Id, Name = p.Provider.Name}
                           )
                       );
                    cfg.CreateMap<FirestoreProgram, Program>().ForMember(dest => dest.Provider, src => src.Ignore());
                    cfg.CreateMap<Workout, FirestoreWorkout>()
                       .ForMember(
                           dest => dest.Provider,
                           src => src.MapFrom(
                               w => new FirestoreProviderInfo
                               {
                                   Id = w.Program.Provider.Id, Name = w.Program.Provider.Name
                               }
                           )
                       )
                       .ForMember(
                           dest => dest.Program,
                           src => src.MapFrom(w => new FirestoreProgramInfo {Id = w.Program.Id, Name = w.Program.Name})
                       );
                    cfg.CreateMap<FirestoreWorkout, Workout>().ForMember(dest => dest.Program, src => src.Ignore());
                }
            );
            return new Mapper(config);
        }
    }
}
