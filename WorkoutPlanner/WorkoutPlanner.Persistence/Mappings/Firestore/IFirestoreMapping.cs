﻿using WorkoutPlanner.Domain.Programs;
using WorkoutPlanner.Domain.Providers;
using WorkoutPlanner.Domain.Workouts;
using WorkoutPlanner.Persistence.Entities.Firestore;

namespace WorkoutPlanner.Persistence.Mappings.Firestore
{
    public interface IFirestoreMapping
    {
        FirestoreProvider ProviderToFirestore(IProvider provider);

        IProvider FirestoreToProvider(FirestoreProvider provider);

        FirestoreProgram ProgramToFirestore(IProgram program);

        IProgram FirestoreToProgram(FirestoreProgram program);

        FirestoreWorkout WorkoutToFirestore(IWorkout workout);

        IWorkout FirestoreToWorkout(FirestoreWorkout workout);
    }
}
