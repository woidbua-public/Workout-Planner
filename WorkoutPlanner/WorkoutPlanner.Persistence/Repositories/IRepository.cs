﻿using System.Threading.Tasks;
using WorkoutPlanner.Domain.Programs;
using WorkoutPlanner.Domain.Providers;
using WorkoutPlanner.Domain.Workouts;

namespace WorkoutPlanner.Persistence.Repositories
{
    public interface IRepository
    {
        Task<bool> IsConnected { get; }

        Task<IProvider> GetProvider(string id);

        Task<bool> AddProvider(IProvider provider);

        Task<IProgram> GetProgram(string id);

        Task<bool> AddProgram(IProgram program);

        Task<IWorkout> GetWorkout(string id);

        Task<bool> AddWorkout(IWorkout workout);
    }
}
