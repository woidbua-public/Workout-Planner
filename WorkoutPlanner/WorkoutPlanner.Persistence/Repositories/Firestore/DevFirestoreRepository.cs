﻿using WorkoutPlanner.Persistence.Mappings.Firestore;

namespace WorkoutPlanner.Persistence.Repositories.Firestore
{
    public sealed class DevFirestoreRepository : FirestoreRepository
    {
        public DevFirestoreRepository(IFirestoreMapping mapping) : base(mapping)
        {
        }


        protected override string RootDocumentName => "dev";
    }
}
