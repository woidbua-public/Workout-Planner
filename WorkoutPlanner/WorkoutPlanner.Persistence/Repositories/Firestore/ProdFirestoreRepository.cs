﻿using WorkoutPlanner.Persistence.Mappings.Firestore;

namespace WorkoutPlanner.Persistence.Repositories.Firestore
{
    public sealed class ProdFirestoreRepository : FirestoreRepository
    {
        public ProdFirestoreRepository(IFirestoreMapping mapping) : base(mapping)
        {
        }


        protected override string RootDocumentName => "prod";
    }
}
