﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Google.Cloud.Firestore;
using WorkoutPlanner.Domain.Programs;
using WorkoutPlanner.Domain.Providers;
using WorkoutPlanner.Domain.Workouts;
using WorkoutPlanner.Persistence.Entities.Firestore;
using WorkoutPlanner.Persistence.Mappings.Firestore;

namespace WorkoutPlanner.Persistence.Repositories.Firestore
{
    public abstract class FirestoreRepository : IRepository
    {
        private const string RootCollectionName = "environment";
        private const string ProvidersCollectionName = "providers";
        private const string ProgramsCollectionName = "programs";
        private const string WorkoutsCollectionName = "workouts";

        private readonly IFirestoreMapping _mapping;
        private readonly FirestoreDb _db;

        private string _firestoreJsonKey;


        protected FirestoreRepository(IFirestoreMapping mapping)
        {
            _mapping = mapping;

            SetupConnection();
            _db = FirestoreDb.Create(Resource.ProjectName_Firestore);
        }


        protected abstract string RootDocumentName { get; }

        private string FirestoreJsonKey
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_firestoreJsonKey))
                {
                    var rootPath = CreateFirestoreJsonKeyRootPath();
                    _firestoreJsonKey = Path.Combine(rootPath, Resource.FileName_FirestoreJsonKey);
                }

                return _firestoreJsonKey;
            }
        }

        public Task<bool> IsConnected => Task.FromResult(!string.IsNullOrWhiteSpace(_db?.ProjectId));


        public async Task<IProvider> GetProvider(string id)
        {
            var snapshot = await GetProvidersCollectionReference().Document(id).GetSnapshotAsync();
            if (!snapshot.Exists)
                throw new ArgumentException($"No {nameof(Provider)} with id '{id}' in database.");

            var firestore = snapshot.ConvertTo<FirestoreProvider>();
            return _mapping.FirestoreToProvider(firestore);
        }


        public async Task<bool> AddProvider(IProvider provider)
        {
            var dictionary = _mapping.ProviderToFirestore(provider);
            var result = await GetProvidersCollectionReference()
                               .Document(provider.Id)
                               .SetAsync(dictionary, SetOptions.MergeAll);
            return result != null;
        }


        public async Task<IProgram> GetProgram(string id)
        {
            var snapshot = await _db.CollectionGroup(ProgramsCollectionName)
                                    .WhereEqualTo(nameof(id), id)
                                    .Limit(1)
                                    .GetSnapshotAsync();

            if (snapshot.Count == 0)
                throw new ArgumentException($"No {nameof(Program)} with id '{id}' in database.");

            var firestore = snapshot.First().ConvertTo<FirestoreProgram>();
            return _mapping.FirestoreToProgram(firestore);
        }


        public async Task<bool> AddProgram(IProgram program)
        {
            var dictionary = _mapping.ProgramToFirestore(program);
            var result = await GetProgramsCollectionReference(program)
                               .Document(program.Id)
                               .SetAsync(dictionary, SetOptions.MergeAll);
            return result != null;
        }


        public async Task<IWorkout> GetWorkout(string id)
        {
            var snapshot = await _db.CollectionGroup(WorkoutsCollectionName)
                                    .WhereEqualTo(nameof(id), id)
                                    .Limit(1)
                                    .GetSnapshotAsync();

            if (snapshot.Count == 0)
                throw new ArgumentException($"No {nameof(Workout)} with id '{id}' in database.");

            var firestore = snapshot.First().ConvertTo<FirestoreWorkout>();
            return _mapping.FirestoreToWorkout(firestore);
        }


        public async Task<bool> AddWorkout(IWorkout workout)
        {
            var dictionary = _mapping.WorkoutToFirestore(workout);
            var result = await GetWorkoutsCollectionReference(workout)
                               .Document(workout.Id)
                               .SetAsync(dictionary, SetOptions.MergeAll);
            return result != null;
        }


        private void SetupConnection()
        {
            Environment.SetEnvironmentVariable(Resource.EnvironmentVariable_Firestore, FirestoreJsonKey);
        }


        private string CreateFirestoreJsonKeyRootPath()
        {
            var rootPath = Path.GetDirectoryName(GetType().Assembly.Location);
            if (rootPath == null)
                throw new ArgumentNullException($"Could not initialize '${nameof(rootPath)}' from assembly location.");
            return rootPath;
        }


        private CollectionReference GetRootCollectionReference()
        {
            return _db.Collection(RootCollectionName);
        }


        private DocumentReference GetRootDocumentReference()
        {
            return GetRootCollectionReference().Document(RootDocumentName);
        }


        protected CollectionReference GetProvidersCollectionReference()
        {
            return GetRootDocumentReference().Collection(ProvidersCollectionName);
        }


        private CollectionReference GetProgramsCollectionReference(IProgram program)
        {
            return GetProvidersCollectionReference().Document(program.Provider.Id).Collection(ProgramsCollectionName);
        }


        private CollectionReference GetWorkoutsCollectionReference(IWorkout workout)
        {
            return GetProgramsCollectionReference(workout.Program)
                   .Document(workout.Program.Id)
                   .Collection(WorkoutsCollectionName);
        }
    }
}
