﻿using Google.Cloud.Firestore;

namespace WorkoutPlanner.Persistence.Entities.Firestore
{
    [FirestoreData]
    public sealed class FirestoreProviderInfo : FirestoreEntityInfo
    {
    }
}
