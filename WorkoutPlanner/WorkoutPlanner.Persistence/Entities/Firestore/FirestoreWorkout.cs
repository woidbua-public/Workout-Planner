﻿using System;
using Google.Cloud.Firestore;

namespace WorkoutPlanner.Persistence.Entities.Firestore
{
    [FirestoreData]
    public sealed class FirestoreWorkout : FirestoreEntity, IFirestoreWorkout
    {
        private DateTime _date;

        [FirestoreProperty(Name = "date")]
        public DateTime Date
        {
            get => DateTime.SpecifyKind(_date, DateTimeKind.Utc);
            set => _date = value;
        }

        [FirestoreProperty(Name = "content")]
        public string Content { get; set; }

        [FirestoreProperty(Name = "url")]
        public string Url { get; set; }

        [FirestoreProperty(Name = "provider")]
        public FirestoreProviderInfo Provider { get; set; }

        [FirestoreProperty(Name = "program")]
        public FirestoreProgramInfo Program { get; set; }
    }
}
