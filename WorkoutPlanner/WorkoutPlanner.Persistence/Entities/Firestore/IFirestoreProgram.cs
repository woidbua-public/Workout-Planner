﻿using Newtonsoft.Json;

namespace WorkoutPlanner.Persistence.Entities.Firestore
{
    public interface IFirestoreProgram : IFirestoreEntity
    {
        [JsonProperty(PropertyName = "provider")]
        FirestoreProviderInfo Provider { get; set; }
    }
}
