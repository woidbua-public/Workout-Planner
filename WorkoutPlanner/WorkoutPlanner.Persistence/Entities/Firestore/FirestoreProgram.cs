﻿using Google.Cloud.Firestore;

namespace WorkoutPlanner.Persistence.Entities.Firestore
{
    [FirestoreData]
    public sealed class FirestoreProgram : FirestoreEntity, IFirestoreProgram
    {
        [FirestoreProperty(Name = "provider")]
        public FirestoreProviderInfo Provider { get; set; }
    }
}
