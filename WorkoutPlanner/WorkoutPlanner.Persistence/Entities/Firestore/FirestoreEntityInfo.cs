﻿using Google.Cloud.Firestore;

namespace WorkoutPlanner.Persistence.Entities.Firestore
{
    [FirestoreData]
    public abstract class FirestoreEntityInfo : IFirestoreEntityInfo
    {
        [FirestoreProperty(Name = "id")]
        public string Id { get; set; }

        [FirestoreProperty(Name = "name")]
        public string Name { get; set; }
    }
}
