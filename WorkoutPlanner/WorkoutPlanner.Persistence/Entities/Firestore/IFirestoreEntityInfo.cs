﻿using Newtonsoft.Json;

namespace WorkoutPlanner.Persistence.Entities.Firestore
{
    public interface IFirestoreEntityInfo
    {
        [JsonProperty(PropertyName = "id")]
        string Id { get; set; }

        [JsonProperty(PropertyName = "name")]
        string Name { get; set; }
    }
}
