﻿using System;
using Newtonsoft.Json;

namespace WorkoutPlanner.Persistence.Entities.Firestore
{
    public interface IFirestoreWorkout : IFirestoreEntity
    {
        [JsonProperty(PropertyName = "date")]
        DateTime Date { get; set; }

        [JsonProperty(PropertyName = "content")]
        string Content { get; set; }

        [JsonProperty(PropertyName = "url")]
        string Url { get; set; }

        [JsonProperty(PropertyName = "provider")]
        FirestoreProviderInfo Provider { get; set; }

        [JsonProperty(PropertyName = "program")]
        FirestoreProgramInfo Program { get; set; }
    }
}
