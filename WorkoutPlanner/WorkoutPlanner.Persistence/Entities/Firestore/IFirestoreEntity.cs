﻿using Newtonsoft.Json;

namespace WorkoutPlanner.Persistence.Entities.Firestore
{
    public interface IFirestoreEntity : IFirestoreEntityInfo
    {
        [JsonProperty(PropertyName = "sourceUrl")]
        string SourceUrl { get; set; }

        [JsonProperty(PropertyName = "imageUrl")]
        string ImageUrl { get; set; }
    }
}
