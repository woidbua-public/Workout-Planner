﻿using Google.Cloud.Firestore;

namespace WorkoutPlanner.Persistence.Entities.Firestore
{
    [FirestoreData]
    public abstract class FirestoreEntity : FirestoreEntityInfo, IFirestoreEntity
    {
        [FirestoreProperty(Name = "sourceUrl")]
        public string SourceUrl { get; set; }

        [FirestoreProperty(Name = "imageUrl")]
        public string ImageUrl { get; set; }
    }
}
