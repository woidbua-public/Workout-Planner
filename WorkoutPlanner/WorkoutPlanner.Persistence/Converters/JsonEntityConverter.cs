﻿using System.Linq;
using WorkoutPlanner.Core.Converters;
using WorkoutPlanner.Domain.Programs;
using WorkoutPlanner.Domain.Providers;
using WorkoutPlanner.Domain.Workouts;

namespace WorkoutPlanner.Persistence.Converters
{
    public sealed class JsonEntityConverter : JsonConverter, IJsonEntityConverter
    {
        public IProvider ConvertToProvider(string value)
        {
            var provider = DeserializeObject<Provider>(value);
            SetProviderReferenceToPrograms(provider);

            return provider;
        }


        public IProgram ConvertToProgram(string value)
        {
            var program = DeserializeObject<Program>(value);
            SetProgramReferenceToWorkouts(program);
            return program;
        }


        public IWorkout ConvertToWorkout(string value)
        {
            var workout = DeserializeObject<Workout>(value);
            return workout;
        }


        private static void SetProviderReferenceToPrograms(IProvider provider)
        {
            foreach (var program in provider.Programs ?? Enumerable.Empty<IProgram>())
            {
                program.Provider = provider;
                SetProgramReferenceToWorkouts(program);
            }
        }


        private static void SetProgramReferenceToWorkouts(IProgram program)
        {
            foreach (var workout in program.Workouts ?? Enumerable.Empty<IWorkout>())
                workout.Program = program;
        }
    }
}
