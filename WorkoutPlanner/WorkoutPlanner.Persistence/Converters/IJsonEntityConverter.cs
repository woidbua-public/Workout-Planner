﻿using WorkoutPlanner.Domain.Programs;
using WorkoutPlanner.Domain.Providers;
using WorkoutPlanner.Domain.Workouts;

namespace WorkoutPlanner.Persistence.Converters
{
    public interface IJsonEntityConverter
    {
        IProvider ConvertToProvider(string value);

        IProgram ConvertToProgram(string value);

        IWorkout ConvertToWorkout(string value);
    }
}
