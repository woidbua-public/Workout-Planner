﻿using WorkoutPlanner.Scraper.Drivers;

namespace WorkoutPlanner.Scraper.Crawlers.PushJerk
{
    public sealed class PushJerkStandardCombinedCrawler : CombinedCrawler
    {
        public PushJerkStandardCombinedCrawler(
            IDriverFactory driverFactory,
            PushJerkStandardUrlCrawler urlCrawler,
            PushJerkStandardHtmlCrawler htmlCrawler
        ) : base(driverFactory, urlCrawler, htmlCrawler)
        {
        }
    }
}
