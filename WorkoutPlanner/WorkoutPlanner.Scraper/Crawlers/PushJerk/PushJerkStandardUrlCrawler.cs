﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using WorkoutPlanner.Core.Services.FileSystems;
using WorkoutPlanner.Core.Services.SleepServices;
using WorkoutPlanner.Core.Writers;
using WorkoutPlanner.Scraper.Configs.PushJerk;
using WorkoutPlanner.Scraper.Drivers;

namespace WorkoutPlanner.Scraper.Crawlers.PushJerk
{
    public sealed class PushJerkStandardUrlCrawler : UrlCrawler
    {
        private int _pageIndex = 1;


        public PushJerkStandardUrlCrawler(
            PushJerkStandardConfig config,
            IFileSystemService fileSystemService,
            IDriverFactory driverFactory,
            IWriterFactory writerFactory,
            ISleepService sleepService
        ) : base(config, fileSystemService, driverFactory, writerFactory, sleepService)
        {
        }


        public int TimeoutNextPage => 1000;


        protected override IEnumerable<string> FetchPageUrls(IDriver driver)
        {
            var articleNodes = driver.FindElementsByCssSelector("h2.entry-title a");
            foreach (var articleNode in articleNodes)
            {
                var url = articleNode.GetAttribute("href");
                yield return url;
            }
        }


        [SuppressMessage("ReSharper", "StringLiteralTypo")]
        protected override bool NavigateToNextPage(IDriver driver)
        {
            var nextButton = driver.FindElementByCssSelector("a.nextpostslink");
            if (nextButton == null)
                return false;

            driver.NavigateTo($"{Config.StartUrl}/page/{++_pageIndex}/");
            SleepService.SleepMilliseconds(TimeoutNextPage);
            return true;
        }
    }
}
