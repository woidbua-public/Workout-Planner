﻿using WorkoutPlanner.Scraper.Drivers;

namespace WorkoutPlanner.Scraper.Crawlers.CompTrain
{
    public sealed class CompTrainIndividualsCombinedCrawler : CombinedCrawler
    {
        public CompTrainIndividualsCombinedCrawler(
            IDriverFactory driverFactory,
            CompTrainIndividualsUrlCrawler urlCrawler,
            CompTrainIndividualsHtmlCrawler htmlCrawler
        ) : base(driverFactory, urlCrawler, htmlCrawler)
        {
        }
    }
}
