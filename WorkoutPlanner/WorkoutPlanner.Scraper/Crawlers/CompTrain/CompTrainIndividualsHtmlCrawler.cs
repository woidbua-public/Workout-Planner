﻿using WorkoutPlanner.Core.Readers;
using WorkoutPlanner.Core.Services.FileSystems;
using WorkoutPlanner.Core.Writers;
using WorkoutPlanner.Scraper.Configs.CompTrain;
using WorkoutPlanner.Scraper.Drivers;

namespace WorkoutPlanner.Scraper.Crawlers.CompTrain
{
    public sealed class CompTrainIndividualsHtmlCrawler : HtmlCrawler
    {
        public CompTrainIndividualsHtmlCrawler(
            CompTrainIndividualsConfig config,
            IFileSystemService fileSystemService,
            IDriverFactory driverFactory,
            IReaderFactory readerFactory,
            IWriterFactory writerFactory
        ) : base(config, fileSystemService, driverFactory, readerFactory, writerFactory)
        {
        }
    }
}
