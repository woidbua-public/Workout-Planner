﻿using System.Collections.Generic;
using OpenQA.Selenium;
using WorkoutPlanner.Core.Services.FileSystems;
using WorkoutPlanner.Core.Services.SleepServices;
using WorkoutPlanner.Core.Writers;
using WorkoutPlanner.Scraper.Configs.CompTrain;
using WorkoutPlanner.Scraper.Drivers;

namespace WorkoutPlanner.Scraper.Crawlers.CompTrain
{
    public sealed class CompTrainIndividualsUrlCrawler : UrlCrawler
    {
        private const int TimeOutFetchPageUrls = 1500;
        private int _pageIndex = 1;


        public CompTrainIndividualsUrlCrawler(
            CompTrainIndividualsConfig config,
            IFileSystemService fileSystemService,
            IDriverFactory driverFactory,
            IWriterFactory writerFactory,
            ISleepService sleepService
        ) : base(config, fileSystemService, driverFactory, writerFactory, sleepService)
        {
        }


        protected override IEnumerable<string> FetchPageUrls(IDriver driver)
        {
            var tableRows = FindTableRows(driver);
            foreach (var tableRow in tableRows)
            {
                tableRow.Click();
                SleepService.SleepMilliseconds(TimeOutFetchPageUrls / 2);
                var webVersionElement = driver.FindElementByXPath("//a[contains(text(), 'Web Version')]");
                var webVersionUrl = webVersionElement.GetAttribute("href");

                var titleElement = driver.FindElementByCssSelector("td table tr td p a");
                var title = titleElement.Text.Replace(" ", string.Empty).Replace("//", "_");

                driver.NavigateBack();
                SleepService.SleepMilliseconds(TimeOutFetchPageUrls / 2);

                var url = $"{webVersionUrl}#{title}";
                yield return url;
            }
        }


        private static IReadOnlyCollection<IWebElement> FindTableRows(IDriver driver)
        {
            return driver.FindElementsByCssSelector("div.Cp div table tbody tr");
        }


        protected override bool NavigateToNextPage(IDriver driver)
        {
            try
            {
                driver.NavigateTo(Config.StartUrl + $"/p{++_pageIndex}");
                SleepService.SleepMilliseconds(Timeout);
                return FindTableRows(driver).Count > 0;
            }
            catch
            {
                return false;
            }
        }
    }
}
