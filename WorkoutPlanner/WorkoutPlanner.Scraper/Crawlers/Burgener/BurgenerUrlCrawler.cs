﻿using System.Collections.Generic;
using OpenQA.Selenium;
using WorkoutPlanner.Core.Services.FileSystems;
using WorkoutPlanner.Core.Services.SleepServices;
using WorkoutPlanner.Core.Writers;
using WorkoutPlanner.Scraper.Configs.Burgener;
using WorkoutPlanner.Scraper.Drivers;

namespace WorkoutPlanner.Scraper.Crawlers.Burgener
{
    public abstract class BurgenerUrlCrawler : UrlCrawler
    {
        protected BurgenerUrlCrawler(
            BurgenerConfig config,
            IFileSystemService fileSystemService,
            IDriverFactory driverFactory,
            IWriterFactory writerFactory,
            ISleepService sleepService
        ) : base(config, fileSystemService, driverFactory, writerFactory, sleepService)
        {
        }


        protected override IEnumerable<string> FetchPageUrls(IDriver driver)
        {
            var pageUrlElement = driver.FindElementByCssSelector("h1.entry-title a.u-url");
            if (pageUrlElement != null)
                yield return pageUrlElement.GetAttribute("href");
        }


        protected override bool NavigateToNextPage(IDriver driver)
        {
            var url = GetNextPageUrl(driver);
            return !string.IsNullOrWhiteSpace(url) && driver.NavigateTo(url);
        }


        private static string GetNextPageUrl(IDriver driver)
        {
            var nextPageNode = FindNextPageElement(driver);
            return nextPageNode?.GetAttribute("href");
        }


        private static IWebElement FindNextPageElement(IDriver driver)
        {
            return driver.FindElementByCssSelector("div.older a");
        }
    }
}
