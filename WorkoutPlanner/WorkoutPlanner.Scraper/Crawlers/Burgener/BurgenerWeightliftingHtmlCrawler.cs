﻿using WorkoutPlanner.Core.Readers;
using WorkoutPlanner.Core.Services.FileSystems;
using WorkoutPlanner.Core.Writers;
using WorkoutPlanner.Scraper.Configs.Burgener;
using WorkoutPlanner.Scraper.Drivers;

namespace WorkoutPlanner.Scraper.Crawlers.Burgener
{
    public sealed class BurgenerWeightliftingHtmlCrawler : HtmlCrawler
    {
        public BurgenerWeightliftingHtmlCrawler(
            BurgenerWeightliftingConfig config,
            IFileSystemService fileSystemService,
            IDriverFactory driverFactory,
            IReaderFactory readerFactory,
            IWriterFactory writerFactory
        ) : base(config, fileSystemService, driverFactory, readerFactory, writerFactory)
        {
        }
    }
}
