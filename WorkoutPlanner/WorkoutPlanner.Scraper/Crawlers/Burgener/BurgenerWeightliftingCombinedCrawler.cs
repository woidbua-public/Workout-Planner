﻿using WorkoutPlanner.Scraper.Drivers;

namespace WorkoutPlanner.Scraper.Crawlers.Burgener
{
    public sealed class BurgenerWeightliftingCombinedCrawler : CombinedCrawler
    {
        public BurgenerWeightliftingCombinedCrawler(
            IDriverFactory driverFactory,
            BurgenerWeightliftingUrlCrawler urlCrawler,
            BurgenerWeightliftingHtmlCrawler htmlCrawler
        ) : base(driverFactory, urlCrawler, htmlCrawler)
        {
        }
    }
}
