﻿using WorkoutPlanner.Scraper.Drivers;

namespace WorkoutPlanner.Scraper.Crawlers.Burgener
{
    public sealed class BurgenerPerformanceCombinedCrawler : CombinedCrawler
    {
        public BurgenerPerformanceCombinedCrawler(
            IDriverFactory driverFactory,
            BurgenerPerformanceUrlCrawler urlCrawler,
            BurgenerPerformanceHtmlCrawler htmlCrawler
        ) : base(driverFactory, urlCrawler, htmlCrawler)
        {
        }
    }
}
