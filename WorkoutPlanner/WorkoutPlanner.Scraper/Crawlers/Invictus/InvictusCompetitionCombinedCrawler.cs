﻿using WorkoutPlanner.Scraper.Drivers;

namespace WorkoutPlanner.Scraper.Crawlers.Invictus
{
    public sealed class InvictusCompetitionCombinedCrawler : CombinedCrawler
    {
        public InvictusCompetitionCombinedCrawler(
            IDriverFactory driverFactory,
            InvictusCompetitionUrlCrawler urlCrawler,
            InvictusCompetitionHtmlCrawler htmlCrawler
        ) : base(driverFactory, urlCrawler, htmlCrawler)
        {
        }
    }
}
