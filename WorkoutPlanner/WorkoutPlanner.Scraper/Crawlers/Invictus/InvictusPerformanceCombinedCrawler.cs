﻿using WorkoutPlanner.Scraper.Drivers;

namespace WorkoutPlanner.Scraper.Crawlers.Invictus
{
    public sealed class InvictusPerformanceCombinedCrawler : CombinedCrawler
    {
        public InvictusPerformanceCombinedCrawler(
            IDriverFactory driverFactory,
            InvictusPerformanceUrlCrawler urlCrawler,
            InvictusPerformanceHtmlCrawler htmlCrawler
        ) : base(driverFactory, urlCrawler, htmlCrawler)
        {
        }
    }
}
