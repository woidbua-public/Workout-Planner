﻿using WorkoutPlanner.Core.Services.FileSystems;
using WorkoutPlanner.Core.Services.SleepServices;
using WorkoutPlanner.Core.Writers;
using WorkoutPlanner.Scraper.Configs.Invictus;
using WorkoutPlanner.Scraper.Drivers;

namespace WorkoutPlanner.Scraper.Crawlers.Invictus
{
    public sealed class InvictusFitnessUrlCrawler : InvictusUrlCrawler
    {
        public InvictusFitnessUrlCrawler(
            InvictusFitnessConfig config,
            IFileSystemService fileSystemService,
            IDriverFactory driverFactory,
            IWriterFactory writerFactory,
            ISleepService sleepService
        ) : base(config, fileSystemService, driverFactory, writerFactory, sleepService)
        {
        }
    }
}
