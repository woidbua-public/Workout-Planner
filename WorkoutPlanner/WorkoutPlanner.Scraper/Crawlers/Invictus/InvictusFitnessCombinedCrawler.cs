﻿using WorkoutPlanner.Scraper.Drivers;

namespace WorkoutPlanner.Scraper.Crawlers.Invictus
{
    public sealed class InvictusFitnessCombinedCrawler : CombinedCrawler
    {
        public InvictusFitnessCombinedCrawler(
            IDriverFactory driverFactory,
            InvictusFitnessUrlCrawler urlCrawler,
            InvictusFitnessHtmlCrawler htmlCrawler
        ) : base(driverFactory, urlCrawler, htmlCrawler)
        {
        }
    }
}
