﻿using WorkoutPlanner.Core.Readers;
using WorkoutPlanner.Core.Services.FileSystems;
using WorkoutPlanner.Core.Writers;
using WorkoutPlanner.Scraper.Configs.Invictus;
using WorkoutPlanner.Scraper.Drivers;

namespace WorkoutPlanner.Scraper.Crawlers.Invictus
{
    public sealed class InvictusFitnessHtmlCrawler : HtmlCrawler
    {
        public InvictusFitnessHtmlCrawler(
            InvictusFitnessConfig config,
            IFileSystemService fileSystemService,
            IDriverFactory driverFactory,
            IReaderFactory readerFactory,
            IWriterFactory writerFactory
        ) : base(config, fileSystemService, driverFactory, readerFactory, writerFactory)
        {
        }
    }
}
