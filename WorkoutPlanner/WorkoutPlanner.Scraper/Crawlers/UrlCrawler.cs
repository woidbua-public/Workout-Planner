﻿using System.Collections.Generic;
using System.Linq;
using WorkoutPlanner.Core.Services.FileSystems;
using WorkoutPlanner.Core.Services.SleepServices;
using WorkoutPlanner.Core.Writers;
using WorkoutPlanner.Scraper.Configs;
using WorkoutPlanner.Scraper.Drivers;

namespace WorkoutPlanner.Scraper.Crawlers
{
    public abstract class UrlCrawler : IUrlCrawler
    {
        private readonly IFileSystemService _fileSystemService;
        private readonly IDriverFactory _driverFactory;
        private readonly IWriterFactory _writerFactory;


        protected UrlCrawler(
            IConfig config,
            IFileSystemService fileSystemService,
            IDriverFactory driverFactory,
            IWriterFactory writerFactory,
            ISleepService sleepService
        )
        {
            Config = config;
            _fileSystemService = fileSystemService;
            _driverFactory = driverFactory;
            _writerFactory = writerFactory;
            SleepService = sleepService;
        }


        protected IConfig Config { get; }

        protected ISleepService SleepService { get; }

        protected int Timeout => 5000;


        public void Setup()
        {
            SetupFolder();
        }


        public bool Login(IDriver driver)
        {
            return Config.Login(driver);
        }


        public void Fetch(IDriver driver)
        {
            NavigateToStartUrl(driver);

            var htmlFiles = _fileSystemService.GetFilePaths(Config.FolderPathHtml).ToList();
            using var writer = _writerFactory.Create(Config.FilePathUrls);
            bool hasPageUrl;

            do
            {
                hasPageUrl = false;
                foreach (var pageUrl in FetchPageUrls(driver))
                {
                    if (!hasPageUrl)
                        hasPageUrl = true;

                    var newHtmlFile = Config.GenerateFilePath(pageUrl);
                    if (htmlFiles.Contains(newHtmlFile))
                        return;

                    writer.WriteLine(pageUrl);
                }
            } while (hasPageUrl && NavigateToNextPage(driver));
        }


        public void Crawl()
        {
            Setup();

            using var driver = _driverFactory.Create();
            if (!Login(driver))
                return;

            Fetch(driver);
        }


        private void SetupFolder()
        {
            var folderPath = Config.FolderPath;
            if (!_fileSystemService.DirectoryExists(folderPath))
                _fileSystemService.CreateDirectory(folderPath);
        }


        private void NavigateToStartUrl(IDriver driver)
        {
            driver.NavigateTo(Config.StartUrl);
            SleepService.SleepMilliseconds(Timeout);
        }


        protected abstract IEnumerable<string> FetchPageUrls(IDriver driver);

        protected abstract bool NavigateToNextPage(IDriver driver);
    }
}
