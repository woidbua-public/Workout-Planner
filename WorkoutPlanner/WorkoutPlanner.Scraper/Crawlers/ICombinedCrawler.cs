﻿namespace WorkoutPlanner.Scraper.Crawlers
{
    public interface ICombinedCrawler
    {
        void CrawlUrls();

        void CrawlHtml();

        void Crawl();
    }
}
