﻿using WorkoutPlanner.Core.Readers;
using WorkoutPlanner.Core.Services.FileSystems;
using WorkoutPlanner.Core.Writers;
using WorkoutPlanner.Scraper.Configs;
using WorkoutPlanner.Scraper.Drivers;

namespace WorkoutPlanner.Scraper.Crawlers
{
    public abstract class HtmlCrawler : IHtmlCrawler
    {
        private readonly IConfig _config;
        private readonly IFileSystemService _fileSystemService;
        private readonly IDriverFactory _driverFactory;
        private readonly IReaderFactory _readerFactory;
        private readonly IWriterFactory _writerFactory;


        protected HtmlCrawler(
            IConfig config,
            IFileSystemService fileSystemService,
            IDriverFactory driverFactory,
            IReaderFactory readerFactory,
            IWriterFactory writerFactory
        )
        {
            _config = config;
            _fileSystemService = fileSystemService;
            _driverFactory = driverFactory;
            _readerFactory = readerFactory;
            _writerFactory = writerFactory;
        }


        public void Setup()
        {
            SetupFolder();
        }


        public bool Login(IDriver driver)
        {
            return _config.Login(driver);
        }


        public void Fetch(IDriver driver)
        {
            NavigateToStartUrl(driver);

            using var reader = _readerFactory.Create(_config.FilePathUrls);
            foreach (var url in reader.ReadLines())
            {
                driver.NavigateTo(url);
                var html = driver.GetHtml();
                var filePath = _config.GenerateFilePath(url);

                using var writer = _writerFactory.Create(filePath);
                writer.Write(html);
            }
        }


        public void Crawl()
        {
            Setup();

            using var driver = _driverFactory.Create();
            if (!Login(driver))
                return;

            Fetch(driver);
        }


        private void SetupFolder()
        {
            var folderPath = _config.FolderPathHtml;
            if (!_fileSystemService.DirectoryExists(folderPath))
                _fileSystemService.CreateDirectory(folderPath);
        }


        private void NavigateToStartUrl(IDriver driver)
        {
            driver.NavigateTo(_config.StartUrl);
        }
    }
}
