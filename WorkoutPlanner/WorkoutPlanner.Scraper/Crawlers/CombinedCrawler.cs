﻿using WorkoutPlanner.Scraper.Drivers;

namespace WorkoutPlanner.Scraper.Crawlers
{
    public abstract class CombinedCrawler : ICombinedCrawler
    {
        private readonly IDriverFactory _driverFactory;
        private readonly IUrlCrawler _urlCrawler;
        private readonly IHtmlCrawler _htmlCrawler;


        protected CombinedCrawler(IDriverFactory driverFactory, IUrlCrawler urlCrawler, IHtmlCrawler htmlCrawler)
        {
            _driverFactory = driverFactory;
            _urlCrawler = urlCrawler;
            _htmlCrawler = htmlCrawler;
        }


        public void CrawlUrls()
        {
            _urlCrawler.Crawl();
        }


        public void CrawlHtml()
        {
            _htmlCrawler.Crawl();
        }


        public void Crawl()
        {
            using var driver = _driverFactory.Create();
            _urlCrawler.Setup();
            _urlCrawler.Login(driver);
            _urlCrawler.Fetch(driver);

            _htmlCrawler.Setup();
            _htmlCrawler.Fetch(driver);
        }
    }
}
