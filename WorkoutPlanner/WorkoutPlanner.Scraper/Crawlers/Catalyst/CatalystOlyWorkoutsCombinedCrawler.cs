﻿using WorkoutPlanner.Scraper.Drivers;

namespace WorkoutPlanner.Scraper.Crawlers.Catalyst
{
    public sealed class CatalystOlyWorkoutsCombinedCrawler : CombinedCrawler
    {
        public CatalystOlyWorkoutsCombinedCrawler(
            IDriverFactory driverFactory,
            CatalystOlyWorkoutsUrlCrawler urlCrawler,
            CatalystOlyWorkoutsHtmlCrawler htmlCrawler
        ) : base(driverFactory, urlCrawler, htmlCrawler)
        {
        }
    }
}
