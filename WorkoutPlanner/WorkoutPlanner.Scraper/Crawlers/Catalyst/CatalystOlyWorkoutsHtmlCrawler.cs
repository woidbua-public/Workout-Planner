﻿using WorkoutPlanner.Core.Readers;
using WorkoutPlanner.Core.Services.FileSystems;
using WorkoutPlanner.Core.Writers;
using WorkoutPlanner.Scraper.Configs.Catalyst;
using WorkoutPlanner.Scraper.Drivers;

namespace WorkoutPlanner.Scraper.Crawlers.Catalyst
{
    public sealed class CatalystOlyWorkoutsHtmlCrawler : HtmlCrawler
    {
        public CatalystOlyWorkoutsHtmlCrawler(
            CatalystOlyWorkoutsConfig config,
            IFileSystemService fileSystemService,
            IDriverFactory driverFactory,
            IReaderFactory readerFactory,
            IWriterFactory writerFactory
        ) : base(config, fileSystemService, driverFactory, readerFactory, writerFactory)
        {
        }
    }
}
