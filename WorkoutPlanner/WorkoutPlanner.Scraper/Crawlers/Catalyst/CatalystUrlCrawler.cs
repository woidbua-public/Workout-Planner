﻿using System.Collections.Generic;
using OpenQA.Selenium;
using WorkoutPlanner.Core.Services.FileSystems;
using WorkoutPlanner.Core.Services.SleepServices;
using WorkoutPlanner.Core.Writers;
using WorkoutPlanner.Scraper.Configs.Catalyst;
using WorkoutPlanner.Scraper.Drivers;

namespace WorkoutPlanner.Scraper.Crawlers.Catalyst
{
    public abstract class CatalystUrlCrawler : UrlCrawler
    {
        protected CatalystUrlCrawler(
            CatalystConfig config,
            IFileSystemService fileSystemService,
            IDriverFactory driverFactory,
            IWriterFactory writerFactory,
            ISleepService sleepService
        ) : base(config, fileSystemService, driverFactory, writerFactory, sleepService)
        {
        }


        protected override IEnumerable<string> FetchPageUrls(IDriver driver)
        {
            var articleNodes = driver.FindElementsByCssSelector("div.workouts_list_text .workouts_list_date");
            foreach (var articleNode in articleNodes)
            {
                var url = articleNode.FindElement(By.CssSelector("a")).GetAttribute("href");
                yield return url;
            }
        }


        protected override bool NavigateToNextPage(IDriver driver)
        {
            var url = GetNextPageUrl(driver);
            return !string.IsNullOrWhiteSpace(url) && driver.NavigateTo(url);
        }


        private static string GetNextPageUrl(IDriver driver)
        {
            var nextPageNode = FindNextPageElement(driver);
            return nextPageNode?.GetAttribute("href");
        }


        private static IWebElement FindNextPageElement(IDriver driver)
        {
            return driver.FindElementByXPath("//a[@class='paginationButton' and text() = 'Next']");
        }
    }
}
