﻿using WorkoutPlanner.Core.Services.FileSystems;
using WorkoutPlanner.Core.Services.SleepServices;
using WorkoutPlanner.Core.Writers;
using WorkoutPlanner.Scraper.Configs.Catalyst;
using WorkoutPlanner.Scraper.Drivers;

namespace WorkoutPlanner.Scraper.Crawlers.Catalyst
{
    public sealed class CatalystOlyWorkoutsUrlCrawler : CatalystUrlCrawler
    {
        public CatalystOlyWorkoutsUrlCrawler(
            CatalystOlyWorkoutsConfig config,
            IFileSystemService fileSystemService,
            IDriverFactory driverFactory,
            IWriterFactory writerFactory,
            ISleepService sleepService
        ) : base(config, fileSystemService, driverFactory, writerFactory, sleepService)
        {
        }
    }
}
