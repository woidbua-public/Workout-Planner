﻿using WorkoutPlanner.Scraper.Drivers;

namespace WorkoutPlanner.Scraper.Crawlers
{
    public interface ICrawler
    {
        void Setup();

        bool Login(IDriver driver);

        void Fetch(IDriver driver);

        void Crawl();
    }
}
