﻿using System;
using WorkoutPlanner.Core.Converters;
using WorkoutPlanner.Core.Readers;
using WorkoutPlanner.Core.Services.FileSystems;
using WorkoutPlanner.Core.Writers;
using WorkoutPlanner.Domain.Programs;
using WorkoutPlanner.Domain.Providers;
using WorkoutPlanner.Domain.Workouts;
using WorkoutPlanner.Scraper.Configs;
using WorkoutPlanner.Scraper.Extractors;

namespace WorkoutPlanner.Scraper.Parsers
{
    public abstract class HtmlParser : IHtmlParser
    {
        private readonly IFileSystemService _fileSystemService;
        private readonly IReaderFactory _readerFactory;
        private readonly IExtractorFactory _extractorFactory;
        private readonly IWriterFactory _writerFactory;


        protected HtmlParser(
            IConfig config,
            IFileSystemService fileSystemService,
            IReaderFactory readerFactory,
            IExtractorFactory extractorFactory,
            IWriterFactory writerFactory
        )
        {
            Config = config;
            _fileSystemService = fileSystemService;
            _readerFactory = readerFactory;
            _extractorFactory = extractorFactory;
            _writerFactory = writerFactory;
        }


        protected IConfig Config { get; }


        public void Parse()
        {
            var htmlFiles = _fileSystemService.GetFilePaths(Config.FolderPathHtml);

            var program = new Program
            {
                Name = Config.ProgramName,
                SourceUrl = Config.ProgramSourceUrl,
                ImageUrl = Config.ProgramImageUrl
            };
            var provider = new Provider
            {
                Name = Config.ProviderName,
                SourceUrl = Config.ProviderSourceUrl,
                ImageUrl = Config.ProviderImageUrl
            };
            provider.AddProgram(program);

            using var errorWriter = _writerFactory.Create(Config.FilePathParseErrors);
            foreach (var htmlFile in htmlFiles)
            {
                using var reader = _readerFactory.Create(htmlFile);
                var html = reader.Read();
                IWorkout workout;
                try
                {
                    workout = GenerateWorkout(html);
                }
                catch (Exception e)
                {
                    errorWriter.WriteLine($"{htmlFile} ({e})");
                    continue;
                }

                program.AddWorkout(workout);
            }

            using var writer = _writerFactory.Create(Config.FilePathJson);
            writer.Write(JsonConverter.SerializeObject(provider));
        }


        private Workout GenerateWorkout(string html)
        {
            using var extractor = _extractorFactory.Create(html);
            return new Workout
            {
                Name = ExtractName(extractor),
                Date = ExtractDate(extractor),
                Content = ExtractContent(extractor),
                Url = ExtractUrl(extractor),
                SourceUrl = Config.ProgramSourceUrl,
                ImageUrl = Config.ProgramImageUrl
            };
        }


        protected abstract string ExtractName(IExtractor extractor);

        protected abstract DateTime ExtractDate(IExtractor extractor);

        protected abstract string ExtractContent(IExtractor extractor);

        protected abstract string ExtractUrl(IExtractor extractor);


        protected static string Combine(string url1, string url2)
        {
            url1 = url1.TrimEnd('/');
            url2 = url2.TrimStart('/');
            return $"{url1}/{url2}";
        }
    }
}
