﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using WorkoutPlanner.Core.Readers;
using WorkoutPlanner.Core.Services.FileSystems;
using WorkoutPlanner.Core.Writers;
using WorkoutPlanner.Scraper.Configs.Burgener;
using WorkoutPlanner.Scraper.Extractors;

namespace WorkoutPlanner.Scraper.Parsers.Burgener
{
    public abstract class BurgenerHtmlParser : HtmlParser
    {
        protected BurgenerHtmlParser(
            BurgenerConfig config,
            IFileSystemService fileSystemService,
            IReaderFactory readerFactory,
            IExtractorFactory extractorFactory,
            IWriterFactory writerFactory
        ) : base(config, fileSystemService, readerFactory, extractorFactory, writerFactory)
        {
        }


        protected override string ExtractName(IExtractor extractor)
        {
            return extractor.GetInnerHtml("h1.entry-title a");
        }


        [SuppressMessage("ReSharper", "StringLiteralTypo")]
        protected override DateTime ExtractDate(IExtractor extractor)
        {
            var dateString = extractor.GetInnerHtml("a.entry-dateline-link");
            return DateTime.ParseExact(dateString, "MMMM d, yyyy", new CultureInfo("en-US"));
        }


        protected override string ExtractContent(IExtractor extractor)
        {
            return extractor.GetInnerHtml("div.entry-content");
        }


        protected override string ExtractUrl(IExtractor extractor)
        {
            return Combine(Config.BaseUrl, extractor.GetAttributeValue("a.entry-dateline-link", "href"));
        }
    }
}
