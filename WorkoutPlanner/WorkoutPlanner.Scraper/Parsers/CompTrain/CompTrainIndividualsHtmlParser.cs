﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using WorkoutPlanner.Core.Readers;
using WorkoutPlanner.Core.Services.FileSystems;
using WorkoutPlanner.Core.Writers;
using WorkoutPlanner.Scraper.Configs.CompTrain;
using WorkoutPlanner.Scraper.Extractors;

namespace WorkoutPlanner.Scraper.Parsers.CompTrain
{
    public sealed class CompTrainIndividualsHtmlParser : HtmlParser
    {
        private static readonly Regex DateRegex = new Regex(@"(\d+\.\d+\.\d+)");


        public CompTrainIndividualsHtmlParser(
            CompTrainIndividualsConfig config,
            IFileSystemService fileSystemService,
            IReaderFactory readerFactory,
            IExtractorFactory extractorFactory,
            IWriterFactory writerFactory
        ) : base(config, fileSystemService, readerFactory, extractorFactory, writerFactory)
        {
        }


        protected override string ExtractName(IExtractor extractor)
        {
            return extractor.GetInnerHtml("p.article-title a");
        }


        protected override DateTime ExtractDate(IExtractor extractor)
        {
            var title = ExtractName(extractor);
            var match = DateRegex.Match(title);

            var dateFormats = new[] {"M.d.yyyy", "M.d.yy"};
            foreach (var dateFormat in dateFormats)
            {
                try
                {
                    var date = DateTime.ParseExact(match.Groups[1].Value, dateFormat, new CultureInfo("en-US"));
                    return date;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }

            throw new Exception("Could not parse date");
        }


        protected override string ExtractContent(IExtractor extractor)
        {
            return extractor.GetInnerHtml("div.article-content");
        }


        protected override string ExtractUrl(IExtractor extractor)
        {
            return extractor.GetAttributeValue("div.article-content p:last-child a", "href");
        }
    }
}
