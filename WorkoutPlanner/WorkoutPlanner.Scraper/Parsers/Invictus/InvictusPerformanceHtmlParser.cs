﻿using WorkoutPlanner.Core.Readers;
using WorkoutPlanner.Core.Services.FileSystems;
using WorkoutPlanner.Core.Writers;
using WorkoutPlanner.Scraper.Configs.Invictus;
using WorkoutPlanner.Scraper.Extractors;

namespace WorkoutPlanner.Scraper.Parsers.Invictus
{
    public sealed class InvictusPerformanceHtmlParser : InvictusHtmlParser
    {
        public InvictusPerformanceHtmlParser(
            InvictusPerformanceConfig config,
            IFileSystemService fileSystemService,
            IReaderFactory readerFactory,
            IExtractorFactory extractorFactory,
            IWriterFactory writerFactory
        ) : base(config, fileSystemService, readerFactory, extractorFactory, writerFactory)
        {
        }
    }
}
