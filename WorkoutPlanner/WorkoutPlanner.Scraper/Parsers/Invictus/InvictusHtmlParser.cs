﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Text.RegularExpressions;
using WorkoutPlanner.Core.Readers;
using WorkoutPlanner.Core.Services.FileSystems;
using WorkoutPlanner.Core.Writers;
using WorkoutPlanner.Scraper.Configs.Invictus;
using WorkoutPlanner.Scraper.Extractors;

namespace WorkoutPlanner.Scraper.Parsers.Invictus
{
    public abstract class InvictusHtmlParser : HtmlParser
    {
        protected InvictusHtmlParser(
            InvictusConfig config,
            IFileSystemService fileSystemService,
            IReaderFactory readerFactory,
            IExtractorFactory extractorFactory,
            IWriterFactory writerFactory
        )
            : base(config, fileSystemService, readerFactory, extractorFactory, writerFactory)
        {
        }


        protected override string ExtractName(IExtractor extractor)
        {
            return extractor.GetInnerHtml("h2.entry-title a");
        }


        [SuppressMessage("ReSharper", "StringLiteralTypo")]
        protected override DateTime ExtractDate(IExtractor extractor)
        {
            var regex = new Regex("^(\\w+)\\s+(\\d+)\\s*,\\s*(\\d+)");

            var title = ExtractName(extractor);
            var match = regex.Match(title);

            var date = $"{match.Groups[1]} {match.Groups[2]}, {match.Groups[3]}";
            return DateTime.ParseExact(date, "MMMM d, yyyy", new CultureInfo("en-US"));
        }


        protected override string ExtractContent(IExtractor extractor)
        {
            return extractor.GetInnerHtml("div.entry-content");
        }


        protected override string ExtractUrl(IExtractor extractor)
        {
            return extractor.GetAttributeValue("h2.entry-title a", "href");
        }
    }
}
