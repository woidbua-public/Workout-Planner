﻿using System;
using System.Globalization;
using WorkoutPlanner.Core.Readers;
using WorkoutPlanner.Core.Services.FileSystems;
using WorkoutPlanner.Core.Writers;
using WorkoutPlanner.Scraper.Configs.PushJerk;
using WorkoutPlanner.Scraper.Extractors;

namespace WorkoutPlanner.Scraper.Parsers.PushJerk
{
    public sealed class PushJerkStandardHtmlParser : HtmlParser
    {
        public PushJerkStandardHtmlParser(
            PushJerkStandardConfig config,
            IFileSystemService fileSystemService,
            IReaderFactory readerFactory,
            IExtractorFactory extractorFactory,
            IWriterFactory writerFactory
        ) : base(config, fileSystemService, readerFactory, extractorFactory, writerFactory)
        {
        }


        protected override string ExtractName(IExtractor extractor)
        {
            return extractor.GetInnerHtml("h1.entry-title");
        }


        protected override DateTime ExtractDate(IExtractor extractor)
        {
            var title = ExtractName(extractor);
            var dateString = title.Substring(title.IndexOf(',') + 1).Trim();
            return DateTime.ParseExact(dateString, "MMM d, yyyy", new CultureInfo("en-US"));
        }


        protected override string ExtractContent(IExtractor extractor)
        {
            return extractor.GetInnerHtml("div.entry-content");
        }


        protected override string ExtractUrl(IExtractor extractor)
        {
            return extractor.GetAttributeValue("link[rel='canonical']", "href");
        }
    }
}
