﻿namespace WorkoutPlanner.Scraper.Parsers
{
    public interface IParser
    {
        void Parse();
    }
}
