﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using WorkoutPlanner.Core.Readers;
using WorkoutPlanner.Core.Services.FileSystems;
using WorkoutPlanner.Core.Writers;
using WorkoutPlanner.Scraper.Configs.Catalyst;
using WorkoutPlanner.Scraper.Extractors;

namespace WorkoutPlanner.Scraper.Parsers.Catalyst
{
    public sealed class CatalystOlyWorkoutsHtmlParser : CatalystHtmlParser
    {
        public CatalystOlyWorkoutsHtmlParser(
            CatalystOlyWorkoutsConfig config,
            IFileSystemService fileSystemService,
            IReaderFactory readerFactory,
            IExtractorFactory extractorFactory,
            IWriterFactory writerFactory
        ) : base(config, fileSystemService, readerFactory, extractorFactory, writerFactory)
        {
        }


        protected override string ExtractName(IExtractor extractor)
        {
            return extractor.GetInnerHtml(".workout_page_date a");
        }


        [SuppressMessage("ReSharper", "StringLiteralTypo")]
        protected override DateTime ExtractDate(IExtractor extractor)
        {
            var title = ExtractName(extractor);
            var date = title.Substring(title.IndexOf(" ", StringComparison.Ordinal)).Trim();
            return DateTime.ParseExact(date, "MMMM d yyyy", new CultureInfo("en-US"));
        }


        protected override string ExtractContent(IExtractor extractor)
        {
            return extractor.GetInnerHtml(".workouts_list_text");
        }


        protected override string ExtractUrl(IExtractor extractor)
        {
            return extractor.GetAttributeValue(".workout_page_date a", "href");
        }
    }
}
