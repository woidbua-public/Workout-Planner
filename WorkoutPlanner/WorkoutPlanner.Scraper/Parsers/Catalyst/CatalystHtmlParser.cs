﻿using WorkoutPlanner.Core.Readers;
using WorkoutPlanner.Core.Services.FileSystems;
using WorkoutPlanner.Core.Writers;
using WorkoutPlanner.Scraper.Configs.Catalyst;
using WorkoutPlanner.Scraper.Extractors;

namespace WorkoutPlanner.Scraper.Parsers.Catalyst
{
    public abstract class CatalystHtmlParser : HtmlParser
    {
        protected CatalystHtmlParser(
            CatalystConfig config,
            IFileSystemService fileSystemService,
            IReaderFactory readerFactory,
            IExtractorFactory extractorFactory,
            IWriterFactory writerFactory
        ) : base(config, fileSystemService, readerFactory, extractorFactory, writerFactory)
        {
        }
    }
}
