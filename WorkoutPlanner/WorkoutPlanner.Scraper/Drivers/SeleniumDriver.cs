﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace WorkoutPlanner.Scraper.Drivers
{
    public sealed class SeleniumDriver : IDriver
    {
        private readonly ChromeDriver _driver;

        private bool _disposed;


        public SeleniumDriver(bool headless = false)
        {
            var options = new ChromeOptions();

            if (headless)
                options.AddArgument("headless");

            var path = Path.GetFullPath(
                Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                    "..",
                    "Local",
                    "Google",
                    "Chrome",
                    "User Data"
                )
            );
            options.AddArguments(
                "--disable-web-security",
                $"--user-data-dir={path}",
                "--allow-running-insecure-content"
            );
            _driver = new ChromeDriver(options);
        }


        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        public bool NavigateTo(string url)
        {
            try
            {
                _driver.Navigate().GoToUrl(url);
                return true;
            }
            catch
            {
                return false;
            }
        }


        public bool NavigateBack()
        {
            try
            {
                _driver.Navigate().Back();
                return true;
            }
            catch
            {
                return false;
            }
        }


        public string GetHtml()
        {
            return _driver.PageSource;
        }


        public string GetUrl()
        {
            return _driver.Url;
        }


        public IWebElement FindElementById(string id)
        {
            try
            {
                return _driver.FindElementById(id);
            }
            catch
            {
                return null;
            }
        }


        public IWebElement FindElementByXPath(string xPath)
        {
            try
            {
                return _driver.FindElementByXPath(xPath);
            }
            catch
            {
                return null;
            }
        }


        public IReadOnlyCollection<IWebElement> FindElementsByXPath(string xPath)
        {
            try
            {
                return _driver.FindElementsByXPath(xPath);
            }
            catch
            {
                return new Collection<IWebElement>();
            }
        }


        public IWebElement FindElementByCssSelector(string cssSelector)
        {
            try
            {
                return _driver.FindElementByCssSelector(cssSelector);
            }
            catch
            {
                return null;
            }
        }


        public IReadOnlyCollection<IWebElement> FindElementsByCssSelector(string cssSelector)
        {
            try
            {
                return _driver.FindElementsByCssSelector(cssSelector);
            }
            catch
            {
                return new Collection<IWebElement>();
            }
        }


        public void HighlightElement(IWebElement element)
        {
            var jsDriver = (IJavaScriptExecutor) _driver;
            const string highlightJavascript =
                @"arguments[0].style.cssText = ""border-width: 2px; border-style: solid; border-color: red; background: yellow;"";";
            jsDriver.ExecuteScript(highlightJavascript, element);
        }


        public void SwitchToNewTab()
        {
            //_driver.ExecuteScript("window.open('','_blank');");
            _driver.SwitchTo().Window(_driver.WindowHandles.Last());
        }


        public void SwitchToMainTab()
        {
            _driver.SwitchTo().Window(_driver.WindowHandles.First());
        }


        public void Close()
        {
            _driver.Close();
        }


        private void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
                _driver?.Quit();

            _disposed = true;
        }


        ~SeleniumDriver() => Dispose(false);
    }
}
