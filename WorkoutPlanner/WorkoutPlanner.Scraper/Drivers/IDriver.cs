﻿using System;
using System.Collections.Generic;
using OpenQA.Selenium;

namespace WorkoutPlanner.Scraper.Drivers
{
    public interface IDriver : IDisposable
    {
        bool NavigateTo(string url);


        bool NavigateBack();


        string GetHtml();


        string GetUrl();


        IWebElement FindElementById(string id);


        IWebElement FindElementByXPath(string xPath);


        IReadOnlyCollection<IWebElement> FindElementsByXPath(string xPath);


        IWebElement FindElementByCssSelector(string cssSelector);


        IReadOnlyCollection<IWebElement> FindElementsByCssSelector(string cssSelector);


        void HighlightElement(IWebElement element);


        void SwitchToNewTab();


        void SwitchToMainTab();


        void Close();
    }
}
