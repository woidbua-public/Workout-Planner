﻿namespace WorkoutPlanner.Scraper.Drivers
{
    public interface IDriverFactory
    {
        IDriver Create();
    }
}
