﻿using WorkoutPlanner.Scraper.Configs;
using WorkoutPlanner.Scraper.Crawlers;
using WorkoutPlanner.Scraper.Parsers;
using WorkoutPlanner.Scraper.Uploaders;

namespace WorkoutPlanner.Scraper.Sources
{
    public abstract class Source : ISource
    {
        private readonly IConfig _config;
        private readonly ICombinedCrawler _combinedCrawler;
        private readonly IHtmlParser _htmlParser;
        private readonly IUploader _uploader;


        protected Source(IConfig config, ICombinedCrawler combinedCrawler, IHtmlParser htmlParser, IUploader uploader)
        {
            _config = config;
            _combinedCrawler = combinedCrawler;
            _htmlParser = htmlParser;
            _uploader = uploader;
        }


        public string Name => $"{_config.ProviderName} - {_config.ProgramName}";


        public void ExecuteUrlCrawler()
        {
            _combinedCrawler.CrawlUrls();
        }


        public void ExecuteHtmlCrawler()
        {
            _combinedCrawler.CrawlHtml();
        }


        public void ExecuteCrawlers()
        {
            _combinedCrawler.Crawl();
        }


        public void ExecuteParser()
        {
            _htmlParser.Parse();
        }


        public void ExecuteUploader()
        {
            _uploader.Upload();
        }
    }
}
