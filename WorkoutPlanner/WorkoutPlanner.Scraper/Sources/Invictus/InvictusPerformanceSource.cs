﻿using WorkoutPlanner.Scraper.Configs.Invictus;
using WorkoutPlanner.Scraper.Crawlers.Invictus;
using WorkoutPlanner.Scraper.Parsers.Invictus;
using WorkoutPlanner.Scraper.Uploaders.Invictus;

namespace WorkoutPlanner.Scraper.Sources.Invictus
{
    public sealed class InvictusPerformanceSource : Source
    {
        public InvictusPerformanceSource(
            InvictusPerformanceConfig config,
            InvictusPerformanceCombinedCrawler combinedCrawler,
            InvictusPerformanceHtmlParser htmlParser,
            InvictusPerformanceUploader uploader
        ) : base(config, combinedCrawler, htmlParser, uploader)
        {
        }
    }
}
