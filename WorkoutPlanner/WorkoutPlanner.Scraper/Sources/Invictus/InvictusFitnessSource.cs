﻿using WorkoutPlanner.Scraper.Configs.Invictus;
using WorkoutPlanner.Scraper.Crawlers.Invictus;
using WorkoutPlanner.Scraper.Parsers.Invictus;
using WorkoutPlanner.Scraper.Uploaders.Invictus;

namespace WorkoutPlanner.Scraper.Sources.Invictus
{
    public sealed class InvictusFitnessSource : Source
    {
        public InvictusFitnessSource(
            InvictusFitnessConfig config,
            InvictusFitnessCombinedCrawler combinedCrawler,
            InvictusFitnessHtmlParser htmlParser,
            InvictusFitnessUploader uploader
        ) : base(config, combinedCrawler, htmlParser, uploader)
        {
        }
    }
}
