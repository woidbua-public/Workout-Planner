﻿using WorkoutPlanner.Scraper.Configs.Invictus;
using WorkoutPlanner.Scraper.Crawlers.Invictus;
using WorkoutPlanner.Scraper.Parsers.Invictus;
using WorkoutPlanner.Scraper.Uploaders.Invictus;

namespace WorkoutPlanner.Scraper.Sources.Invictus
{
    public sealed class InvictusCompetitionSource : Source
    {
        public InvictusCompetitionSource(
            InvictusCompetitionConfig config,
            InvictusCompetitionCombinedCrawler combinedCrawler,
            InvictusCompetitionHtmlParser htmlParser,
            InvictusCompetitionUploader uploader
        ) : base(config, combinedCrawler, htmlParser, uploader)
        {
        }
    }
}
