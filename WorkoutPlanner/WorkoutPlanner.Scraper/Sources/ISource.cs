﻿namespace WorkoutPlanner.Scraper.Sources
{
    public interface ISource
    {
        string Name { get; }

        void ExecuteUrlCrawler();

        void ExecuteHtmlCrawler();

        void ExecuteCrawlers();

        void ExecuteParser();

        void ExecuteUploader();
    }
}
