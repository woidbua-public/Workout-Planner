﻿using WorkoutPlanner.Scraper.Configs.PushJerk;
using WorkoutPlanner.Scraper.Crawlers.PushJerk;
using WorkoutPlanner.Scraper.Parsers.PushJerk;
using WorkoutPlanner.Scraper.Uploaders.PushJerk;

namespace WorkoutPlanner.Scraper.Sources.PushJerk
{
    public sealed class PushJerkStandardSource : Source
    {
        public PushJerkStandardSource(
            PushJerkStandardConfig config,
            PushJerkStandardCombinedCrawler combinedCrawler,
            PushJerkStandardHtmlParser htmlParser,
            PushJerkStandardUploader uploader
        ) : base(config, combinedCrawler, htmlParser, uploader)
        {
        }
    }
}
