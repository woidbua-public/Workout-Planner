﻿using WorkoutPlanner.Scraper.Configs.Catalyst;
using WorkoutPlanner.Scraper.Crawlers.Catalyst;
using WorkoutPlanner.Scraper.Parsers.Catalyst;
using WorkoutPlanner.Scraper.Uploaders.Catalyst;

namespace WorkoutPlanner.Scraper.Sources.Catalyst
{
    public sealed class CatalystOlyWorkoutsSource : Source
    {
        public CatalystOlyWorkoutsSource(
            CatalystOlyWorkoutsConfig config,
            CatalystOlyWorkoutsCombinedCrawler combinedCrawler,
            CatalystOlyWorkoutsHtmlParser htmlParser,
            CatalystOlyWorkoutsUploader uploader
        ) : base(config, combinedCrawler, htmlParser, uploader)
        {
        }
    }
}
