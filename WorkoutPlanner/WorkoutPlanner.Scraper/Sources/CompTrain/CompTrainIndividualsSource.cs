﻿using WorkoutPlanner.Scraper.Configs.CompTrain;
using WorkoutPlanner.Scraper.Crawlers.CompTrain;
using WorkoutPlanner.Scraper.Parsers.CompTrain;
using WorkoutPlanner.Scraper.Uploaders.CompTrain;

namespace WorkoutPlanner.Scraper.Sources.CompTrain
{
    public sealed class CompTrainIndividualsSource : Source
    {
        public CompTrainIndividualsSource(
            CompTrainIndividualsConfig config,
            CompTrainIndividualsCombinedCrawler combinedCrawler,
            CompTrainIndividualsHtmlParser htmlParser,
            CompTrainIndividualsUploader uploader
        ) : base(config, combinedCrawler, htmlParser, uploader)
        {
        }
    }
}
