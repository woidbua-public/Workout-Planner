﻿using WorkoutPlanner.Scraper.Configs.Burgener;
using WorkoutPlanner.Scraper.Crawlers.Burgener;
using WorkoutPlanner.Scraper.Parsers.Burgener;
using WorkoutPlanner.Scraper.Uploaders.Burgener;

namespace WorkoutPlanner.Scraper.Sources.Burgener
{
    public sealed class BurgenerWeightliftingSource : Source
    {
        public BurgenerWeightliftingSource(
            BurgenerWeightliftingConfig config,
            BurgenerWeightliftingCombinedCrawler combinedCrawler,
            BurgenerWeightliftingHtmlParser htmlParser,
            BurgenerWeightliftingUploader uploader
        ) : base(config, combinedCrawler, htmlParser, uploader)
        {
        }
    }
}
