﻿using WorkoutPlanner.Scraper.Configs.Burgener;
using WorkoutPlanner.Scraper.Crawlers.Burgener;
using WorkoutPlanner.Scraper.Parsers.Burgener;
using WorkoutPlanner.Scraper.Uploaders.Burgener;

namespace WorkoutPlanner.Scraper.Sources.Burgener
{
    public sealed class BurgenerPerformanceSource : Source
    {
        public BurgenerPerformanceSource(
            BurgenerPerformanceConfig config,
            BurgenerPerformanceCombinedCrawler combinedCrawler,
            BurgenerPerformanceHtmlParser htmlParser,
            BurgenerPerformanceUploader uploader
        ) : base(config, combinedCrawler, htmlParser, uploader)
        {
        }
    }
}
