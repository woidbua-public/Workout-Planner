﻿using WorkoutPlanner.Core.Readers;
using WorkoutPlanner.Persistence.Converters;
using WorkoutPlanner.Persistence.Repositories;
using WorkoutPlanner.Scraper.Configs.PushJerk;

namespace WorkoutPlanner.Scraper.Uploaders.PushJerk
{
    public sealed class PushJerkStandardUploader : Uploader
    {
        public PushJerkStandardUploader(
            PushJerkStandardConfig config,
            IRepository repository,
            IReaderFactory readerFactory,
            IJsonEntityConverter converter
        ) : base(config, repository, readerFactory, converter)
        {
        }
    }
}
