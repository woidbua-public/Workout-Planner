﻿using WorkoutPlanner.Core.Readers;
using WorkoutPlanner.Persistence.Converters;
using WorkoutPlanner.Persistence.Repositories;
using WorkoutPlanner.Scraper.Configs.Burgener;

namespace WorkoutPlanner.Scraper.Uploaders.Burgener
{
    public sealed class BurgenerPerformanceUploader : Uploader
    {
        public BurgenerPerformanceUploader(
            BurgenerPerformanceConfig config,
            IRepository repository,
            IReaderFactory readerFactory,
            IJsonEntityConverter converter
        ) : base(config, repository, readerFactory, converter)
        {
        }
    }
}
