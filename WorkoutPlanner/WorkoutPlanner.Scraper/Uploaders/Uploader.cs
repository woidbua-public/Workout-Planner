﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WorkoutPlanner.Core.Readers;
using WorkoutPlanner.Domain.Programs;
using WorkoutPlanner.Domain.Providers;
using WorkoutPlanner.Domain.Workouts;
using WorkoutPlanner.Persistence.Converters;
using WorkoutPlanner.Persistence.Repositories;
using WorkoutPlanner.Scraper.Configs;

namespace WorkoutPlanner.Scraper.Uploaders
{
    public abstract class Uploader : IUploader
    {
        private readonly IConfig _config;
        private readonly IRepository _repository;
        private readonly IReaderFactory _readerFactory;
        private readonly IJsonEntityConverter _converter;


        protected Uploader(
            IConfig config,
            IRepository repository,
            IReaderFactory readerFactory,
            IJsonEntityConverter converter
        )
        {
            _config = config;
            _repository = repository;
            _readerFactory = readerFactory;
            _converter = converter;
        }


        public async Task Upload()
        {
            var provider = LoadParsedFile();
            await UploadProvider(provider);
            await UploadPrograms(provider.Programs);
            await UploadWorkouts(provider.Programs.SelectMany(p => p.Workouts));
        }


        private IProvider LoadParsedFile()
        {
            using var reader = _readerFactory.Create(_config.FilePathJson);
            var providerString = reader.Read();
            return _converter.ConvertToProvider(providerString);
        }


        private async Task UploadProvider(IProvider provider)
        {
            if (!await _repository.AddProvider(provider))
                throw new InvalidOperationException($"Could not upload provider '{provider.Name}'!");
        }


        private async Task UploadPrograms(IEnumerable<IProgram> programs)
        {
            foreach (var program in programs)
            {
                if (!await _repository.AddProgram(program))
                    throw new InvalidOperationException($"Could not upload program '{program.Name}'!");
            }
        }


        private async Task UploadWorkouts(IEnumerable<IWorkout> workouts)
        {
            foreach (var workout in workouts)
            {
                if (!await _repository.AddWorkout(workout))
                    throw new InvalidOperationException($"Could not upload workout '{workout.Name}'!");
            }
        }
    }
}
