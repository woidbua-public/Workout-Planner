﻿using WorkoutPlanner.Core.Readers;
using WorkoutPlanner.Persistence.Converters;
using WorkoutPlanner.Persistence.Repositories;
using WorkoutPlanner.Scraper.Configs.Invictus;

namespace WorkoutPlanner.Scraper.Uploaders.Invictus
{
    public sealed class InvictusPerformanceUploader : Uploader
    {
        public InvictusPerformanceUploader(
            InvictusPerformanceConfig config,
            IRepository repository,
            IReaderFactory readerFactory,
            IJsonEntityConverter converter
        ) : base(config, repository, readerFactory, converter)
        {
        }
    }
}
