﻿using WorkoutPlanner.Core.Readers;
using WorkoutPlanner.Persistence.Converters;
using WorkoutPlanner.Persistence.Repositories;
using WorkoutPlanner.Scraper.Configs.CompTrain;

namespace WorkoutPlanner.Scraper.Uploaders.CompTrain
{
    public sealed class CompTrainIndividualsUploader : Uploader
    {
        public CompTrainIndividualsUploader(
            CompTrainIndividualsConfig config,
            IRepository repository,
            IReaderFactory readerFactory,
            IJsonEntityConverter converter
        ) : base(config, repository, readerFactory, converter)
        {
        }
    }
}
