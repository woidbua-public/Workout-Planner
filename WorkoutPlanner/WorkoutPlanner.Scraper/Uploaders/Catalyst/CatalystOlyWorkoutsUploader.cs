﻿using WorkoutPlanner.Core.Readers;
using WorkoutPlanner.Persistence.Converters;
using WorkoutPlanner.Persistence.Repositories;
using WorkoutPlanner.Scraper.Configs.Catalyst;

namespace WorkoutPlanner.Scraper.Uploaders.Catalyst
{
    public sealed class CatalystOlyWorkoutsUploader : Uploader
    {
        public CatalystOlyWorkoutsUploader(
            CatalystOlyWorkoutsConfig config,
            IRepository repository,
            IReaderFactory readerFactory,
            IJsonEntityConverter converter
        ) : base(config, repository, readerFactory, converter)
        {
        }
    }
}
