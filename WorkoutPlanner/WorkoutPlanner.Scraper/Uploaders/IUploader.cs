﻿using System.Threading.Tasks;

namespace WorkoutPlanner.Scraper.Uploaders
{
    public interface IUploader
    {
        Task Upload();
    }
}
