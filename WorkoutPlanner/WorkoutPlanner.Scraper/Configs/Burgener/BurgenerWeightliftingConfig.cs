﻿namespace WorkoutPlanner.Scraper.Configs.Burgener
{
    public sealed class BurgenerWeightliftingConfig : BurgenerConfig
    {
        public override string ProgramSourceUrl => StartUrl;

        public override string ProgramImageUrl => "https://wallpaperaccess.com/full/1350038.jpg";

        public override string ProgramName => "Weightlifting";

        // ReSharper disable once StringLiteralTypo
        public override string StartUrl => $"{BaseUrl}/freeweightlifting";
    }
}
