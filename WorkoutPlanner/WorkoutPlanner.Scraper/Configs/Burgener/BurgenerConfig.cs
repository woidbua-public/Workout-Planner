﻿using WorkoutPlanner.Scraper.Drivers;

namespace WorkoutPlanner.Scraper.Configs.Burgener
{
    public abstract class BurgenerConfig : Config
    {
        public override string ProviderName => "Burgener Strength";

        public override string ProviderSourceUrl => BaseUrl;

        public override string ProviderImageUrl =>
            "https://external-preview.redd.it/ja2QTF-anfw0weT9mzQgPEc9WfClGtLvOsGJg43i7_E.jpg?auto=webp&s=d068c89a794590bb46dd3111afd3f2340e88d4d1";

        public override string BaseUrl => "https://www.burgenerstrength.com";


        public override bool Login(IDriver driver)
        {
            return true;
        }


        protected override string GenerateFileName(string url)
        {
            var subUrl = url.Replace($"{BaseUrl}/", string.Empty).Replace("/", "_");
            return subUrl;
        }
    }
}
