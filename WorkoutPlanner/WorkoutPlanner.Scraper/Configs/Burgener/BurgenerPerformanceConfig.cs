﻿namespace WorkoutPlanner.Scraper.Configs.Burgener
{
    public sealed class BurgenerPerformanceConfig : BurgenerConfig
    {
        public override string ProgramSourceUrl => StartUrl;

        public override string ProgramImageUrl =>
            "https://img2.goodfon.com/wallpaper/nbig/b/7e/gym-fitness-crossfit.jpg";

        public override string ProgramName => "Performance";

        public override string StartUrl => $"{BaseUrl}/performance";
    }
}
