﻿using DotNetEnv;
using WorkoutPlanner.Scraper.Drivers;

namespace WorkoutPlanner.Scraper.Configs.CompTrain
{
    public sealed class CompTrainIndividualsConfig : Config
    {
        private const int Timeout = 5000;

        public override string ProviderName => "CompTrain";

        public override string ProviderSourceUrl => "https://comptrain.co/";

        public override string ProviderImageUrl =>
            "https://i.pinimg.com/originals/a3/d0/77/a3d0779dc0188e753408055ec0163602.png";

        public override string ProgramSourceUrl => "https://comptrain.co/wod/";

        public override string ProgramImageUrl =>
            "https://images.squarespace-cdn.com/content/v1/533efd1ae4b0e277ef5d618b/1574629780618-IUKWWI27C1ND1DYG181W/ke17ZwdGBToddI8pDm48kPRM-sRROp-_YokPgiUQr9QUqsxRUqqbr1mOJYKfIPR7LoDQ9mXPOjoJoqy81S2I8N_N4V1vUb5AoIIIbLZhVYy7Mythp_T-mtop-vrsUOmeInPi9iDjx9w8K4ZfjXt2doGBOHYmeh2UtsLyZYYixzpIWZvAmb9cSHxjsNH5DoM_CjLISwBs8eEdxAxTptZAUg/Comptrain-Feature.png?format=2500w";

        public override string ProgramName => "Individuals";

        public override string BaseUrl => "https://mail.google.com/mail/u/0";

        public override string StartUrl => $"{BaseUrl}/?pli=1#label/Sport%2FCompTrain";


        public override bool Login(IDriver driver)
        {
            Env.Load();

            try
            {
                driver.NavigateTo(StartUrl);

                //var inputUsername = driver.FindElementByCssSelector("input[type='email']");
                //inputUsername.SendKeys(Env.GetString("USERNAME_COMPTRAIN_CRAWLER"));
                //var nextButton = driver.FindElementById("identifierNext");
                //nextButton.Click();
                //Thread.Sleep(Timeout / 2);

                //var inputPassword = driver.FindElementByCssSelector("input[type='password']");
                //inputPassword.SendKeys(Environment.GetEnvironmentVariable("PASSWORD_COMPTRAIN_CRAWLER"));
                //nextButton = driver.FindElementById("passwordNext");
                //nextButton.Click();
                //Thread.Sleep(Timeout);
            }
            catch
            {
                return false;
            }

            return true;
        }


        protected override string GenerateFileName(string url)
        {
            var rawFileName = url.Substring(url.IndexOf('#') + 1);
            var fileName = rawFileName.Replace(".", "_").Replace("#", "_").ToLower();

            return fileName;
        }
    }
}
