﻿using System;
using System.Linq;
using WorkoutPlanner.Scraper.Drivers;

namespace WorkoutPlanner.Scraper.Configs.PushJerk
{
    public sealed class PushJerkStandardConfig : Config
    {
        public override string ProviderName => "Push Jerk";

        public override string ProviderSourceUrl => StartUrl;

        public override string ProviderImageUrl =>
            "https://secureservercdn.net/166.62.109.21/q0g.ead.myftpupload.com/wp-content/uploads/2020/02/PushJerk_Header1920.jpg";

        public override string ProgramSourceUrl => StartUrl;

        public override string ProgramImageUrl =>
            "https://img2.goodfon.com/wallpaper/nbig/1/e5/crossfit-gym-podkhod.jpg";

        public override string ProgramName => "Standard";

        public override string BaseUrl => "http://pushjerk.com";

        public override string StartUrl => $"{BaseUrl}/";


        public override bool Login(IDriver driver)
        {
            return true;
        }


        protected override string GenerateFileName(string url)
        {
            var urlSplits = url.Split("/", StringSplitOptions.RemoveEmptyEntries);
            return urlSplits.Last();
        }
    }
}
