﻿using WorkoutPlanner.Scraper.Drivers;

namespace WorkoutPlanner.Scraper.Configs
{
    public interface IConfig
    {
        string ProviderName { get; }

        string ProviderSourceUrl { get; }

        string ProviderImageUrl { get; }

        string ProgramSourceUrl { get; }

        string ProgramImageUrl { get; }

        string ProgramName { get; }

        string SourceName { get; }

        string BaseUrl { get; }

        string StartUrl { get; }

        string FolderPath { get; }

        string FolderPathHtml { get; }

        string FilePathUrls { get; }

        string FilePathJson { get; }

        string FilePathParseErrors { get; }

        bool Login(IDriver driver);

        string GenerateFilePath(string url);
    }
}
