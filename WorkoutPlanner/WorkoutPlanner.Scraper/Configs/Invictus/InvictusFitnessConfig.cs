﻿using WorkoutPlanner.Scraper.Drivers;

namespace WorkoutPlanner.Scraper.Configs.Invictus
{
    public sealed class InvictusFitnessConfig : InvictusConfig
    {
        public override string ProgramSourceUrl => StartUrl;

        public override string ProgramImageUrl =>
            "https://img2.goodfon.com/wallpaper/nbig/6/1f/class-crossfit-russian.jpg";

        public override string ProgramName => "Fitness";

        public override string StartUrl => $"{BaseUrl}/category/wod/fitness/";


        public override bool Login(IDriver driver)
        {
            return true;
        }
    }
}
