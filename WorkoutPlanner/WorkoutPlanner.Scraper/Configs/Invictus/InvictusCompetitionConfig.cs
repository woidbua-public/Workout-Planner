﻿namespace WorkoutPlanner.Scraper.Configs.Invictus
{
    public sealed class InvictusCompetitionConfig : InvictusConfig
    {
        public override string ProgramSourceUrl => StartUrl;

        public override string ProgramImageUrl => "https://wallpapercave.com/wp/wp2056158.jpg";

        public override string ProgramName => "Competition";

        public override string StartUrl => $"{BaseUrl}/category/invictus-competition-program/";
    }
}
