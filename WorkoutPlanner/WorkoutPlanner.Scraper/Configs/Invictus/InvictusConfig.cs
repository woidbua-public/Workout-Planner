﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using DotNetEnv;
using OpenQA.Selenium;
using WorkoutPlanner.Scraper.Drivers;

namespace WorkoutPlanner.Scraper.Configs.Invictus
{
    public abstract class InvictusConfig : Config
    {
        protected const int Timeout = 1000;

        public override string ProviderName => "Invictus";

        public override string ProviderSourceUrl => BaseUrl;

        public override string ProviderImageUrl => "https://i.ytimg.com/vi/_K9H1as9R64/maxresdefault.jpg";

        public override string BaseUrl => "https://www.crossfitinvictus.com";


        [SuppressMessage("ReSharper", "StringLiteralTypo")]
        public override bool Login(IDriver driver)
        {
            Env.Load();

            try
            {
                driver.NavigateTo(StartUrl);

                var loginButton = FindLoginButton(driver);
                loginButton.Click();
                Thread.Sleep(Timeout);

                var inputUsername = driver.FindElementById("amember-login");
                inputUsername.SendKeys(Env.GetString("USERNAME_INVICTUS_CRAWLER"));

                var inputPassword = driver.FindElementById("amember-pass");
                inputPassword.SendKeys(Env.GetString("PASSWORD_INVICTUS_CRAWLER"));

                var submitForm = driver.FindElementByCssSelector("form[name=\"login\"]");
                submitForm.Submit();
                Thread.Sleep(Timeout);
            }
            catch
            {
                return false;
            }

            return true;
        }


        private static IWebElement FindLoginButton(IDriver driver)
        {
            return driver.FindElementByXPath("//a[contains(text(), 'Log in')]");
        }


        protected override string GenerateFileName(string url)
        {
            var splits = url.Split('/', StringSplitOptions.RemoveEmptyEntries);
            return splits.Last();
        }
    }
}
