﻿using WorkoutPlanner.Scraper.Drivers;

namespace WorkoutPlanner.Scraper.Configs.Invictus
{
    public sealed class InvictusPerformanceConfig : InvictusConfig
    {
        public override string ProgramSourceUrl => StartUrl;

        public override string ProgramImageUrl =>
            "https://img4.goodfon.com/wallpaper/nbig/e/1c/workout-fitness-workout-weight-lifting-technique-look-female.jpg";

        public override string ProgramName => "Performance";

        public override string StartUrl => $"{BaseUrl}/category/wod/performance/";


        public override bool Login(IDriver driver)
        {
            return true;
        }
    }
}
