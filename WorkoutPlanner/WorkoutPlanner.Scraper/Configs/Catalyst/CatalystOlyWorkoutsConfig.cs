﻿namespace WorkoutPlanner.Scraper.Configs.Catalyst
{
    public sealed class CatalystOlyWorkoutsConfig : CatalystConfig
    {
        public override string ProgramSourceUrl => StartUrl;

        public override string ProgramImageUrl =>
            "https://simplifaster.com/wp-content/uploads/2017/09/Olympic-Lift.jpg";

        public override string ProgramName => "Olympic Weightlifting Workouts";

        public override string StartUrl => $"{BaseUrl}/olympic-weightlifting-workouts";
    }
}
