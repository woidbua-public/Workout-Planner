﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using DotNetEnv;
using OpenQA.Selenium;
using WorkoutPlanner.Scraper.Drivers;

namespace WorkoutPlanner.Scraper.Configs.Catalyst
{
    public abstract class CatalystConfig : Config
    {
        protected const int Timeout = 1000;

        public override string ProviderName => "Catalyst Athletics";

        public override string ProviderSourceUrl => BaseUrl;

        public override string ProviderImageUrl => "https://i.ytimg.com/vi/E5LiCyyrEl4/maxresdefault.jpg";

        public override string BaseUrl => "https://www.catalystathletics.com";


        [SuppressMessage("ReSharper", "StringLiteralTypo")]
        public override bool Login(IDriver driver)
        {
            Env.Load();

            try
            {
                driver.NavigateTo(StartUrl);

                var loginButton = FindLoginButton(driver);
                loginButton.Click();
                Thread.Sleep(Timeout);

                var inputEmail = driver.FindElementByXPath(
                    "//div[@class='profileRegBox regBoxLeft']/form/table/tbody/tr[1]/td/input[@class='siteReg']"
                );
                inputEmail.SendKeys(Env.GetString("USERNAME_CATALYST_CRAWLER"));

                var inputPassword = driver.FindElementByXPath(
                    "//div[@class='profileRegBox regBoxLeft']/form/table/tbody/tr[2]/td/input[@class='siteReg']"
                );
                inputPassword.SendKeys(Env.GetString("PASSWORD_CATALYST_CRAWLER"));

                var loginForm = driver.FindElementByXPath(
                    "//div[@class='profileRegBox regBoxLeft']/form/table/tbody/tr[3]/td/button[@id='Submit']"
                );
                loginForm.Submit();
                Thread.Sleep(Timeout);
            }
            catch
            {
                return false;
            }

            return true;
        }


        private static IWebElement FindLoginButton(IDriver driver)
        {
            return driver.FindElementByXPath("//a[@class='button' and text() = 'Login']");
        }


        protected override string GenerateFileName(string url)
        {
            var splits = url.Split('/', StringSplitOptions.RemoveEmptyEntries);
            return splits.Last().ToLower();
        }
    }
}
