﻿using System;
using System.IO;
using Slugify;
using WorkoutPlanner.Scraper.Drivers;

namespace WorkoutPlanner.Scraper.Configs
{
    public abstract class Config : IConfig
    {
        protected Config()
        {
            SlugHelper = new SlugHelper();
        }


        protected SlugHelper SlugHelper { get; }

        public string BaseFolderPath =>
            Environment.GetEnvironmentVariable(Resource.EnvironmentPath_WorkoutPlanner_Crawlers);

        public abstract string ProviderName { get; }

        public abstract string ProviderSourceUrl { get; }

        public abstract string ProviderImageUrl { get; }

        public abstract string ProgramSourceUrl { get; }

        public abstract string ProgramImageUrl { get; }

        public abstract string ProgramName { get; }

        public string SourceName => SlugHelper.GenerateSlug($"{ProviderName.Trim()} {ProgramName.Trim()}".Trim());

        public abstract string BaseUrl { get; }

        public abstract string StartUrl { get; }

        public string FolderPath => Path.Combine(BaseFolderPath, SourceName);

        public string FolderPathHtml => Path.Combine(FolderPath, "html");

        public string FilePathUrls => Path.Combine(FolderPath, "urls.txt");

        public string FilePathJson => Path.Combine(FolderPath, "parsed.json");

        public string FilePathParseErrors => Path.Combine(FolderPath, "parse_errors.txt");

        public abstract bool Login(IDriver driver);


        public string GenerateFilePath(string url)
        {
            return Path.Combine(FolderPathHtml, GenerateFileName(url)) + ".html";
        }


        protected abstract string GenerateFileName(string url);
    }
}
