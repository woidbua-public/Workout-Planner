﻿using System;
using HtmlAgilityPack;
using HtmlAgilityPack.CssSelectors.NetCore;

namespace WorkoutPlanner.Scraper.Extractors
{
    public sealed class HtmlAgilityExtractor : IExtractor
    {
        private readonly HtmlDocument _doc;

        private bool _disposed;


        public HtmlAgilityExtractor(string html)
        {
            _doc = new HtmlDocument();
            _doc.LoadHtml(html);
        }


        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        public string GetInnerHtml(string cssSelector)
        {
            return _doc.QuerySelector(cssSelector).InnerHtml?.Trim();
        }


        public string GetAttributeValue(string cssSelector, string attributeName)
        {
            return _doc.QuerySelector(cssSelector).GetAttributeValue(attributeName, default(string))?.Trim();
        }


        private void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
            }

            _disposed = true;
        }


        ~HtmlAgilityExtractor() => Dispose(false);
    }
}
