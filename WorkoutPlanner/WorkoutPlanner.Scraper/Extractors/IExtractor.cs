﻿using System;

namespace WorkoutPlanner.Scraper.Extractors
{
    public interface IExtractor : IDisposable
    {
        string GetInnerHtml(string cssSelector);


        string GetAttributeValue(string cssSelector, string attributeName);
    }
}
