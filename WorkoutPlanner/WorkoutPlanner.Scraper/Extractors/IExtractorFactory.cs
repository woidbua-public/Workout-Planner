﻿namespace WorkoutPlanner.Scraper.Extractors
{
    public interface IExtractorFactory
    {
        IExtractor Create(string html);
    }
}
