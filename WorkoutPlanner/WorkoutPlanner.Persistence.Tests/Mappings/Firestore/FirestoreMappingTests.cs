﻿using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using WorkoutPlanner.Domain.Builders;
using WorkoutPlanner.Persistence.Entities.Firestore;
using WorkoutPlanner.Persistence.Mappings.Firestore;

namespace WorkoutPlanner.Persistence.Tests.Mappings.Firestore
{
    [TestFixture]
    public class FirestoreMappingTests
    {
        [SetUp]
        public void SetUp()
        {
            _sut = new FirestoreMapping();
        }


        private FirestoreMapping _sut;


        [Test]
        public void ProviderShouldBeMappedToFirestore()
        {
            // Arrange
            var provider = ProviderBuilder.Typical().Build();

            // Act
            var result = _sut.ProviderToFirestore(provider);

            // Assert
            var expected = new FirestoreProvider
            {
                Id = provider.Id,
                Name = provider.Name,
                SourceUrl = provider.SourceUrl,
                ImageUrl = provider.ImageUrl
            };
            result.Should().BeEquivalentTo(expected);
        }


        [Test]
        public void FirestoreShouldBeMappedToProvider()
        {
            // Arrange
            var provider = ProviderBuilder.Typical().Build();

            // Act
            var firestore = _sut.ProviderToFirestore(provider);
            var result = _sut.FirestoreToProvider(firestore);

            // Assert
            var expected = ProviderBuilder.Default()
                                          .WithName(provider.Name)
                                          .WithSourceUrl(provider.SourceUrl)
                                          .WithImageUrl(provider.ImageUrl)
                                          .Build();
            result.Should().BeEquivalentTo(expected);
        }


        [Test]
        public void ProgramShouldBeMappedToFirestore()
        {
            // Arrange
            var provider = ProviderBuilder.Typical().Build();
            var program = provider.Programs.First();

            // Act
            var result = _sut.ProgramToFirestore(program);

            // Assert
            var expected = new FirestoreProgram
            {
                Id = program.Id,
                Name = program.Name,
                SourceUrl = program.SourceUrl,
                ImageUrl = program.ImageUrl,
                Provider = new FirestoreProviderInfo {Id = program.Provider.Id, Name = program.Provider.Name}
            };
            result.Should().BeEquivalentTo(expected);
        }


        [Test]
        public void FirestoreShouldBeMappedToProgram()
        {
            // Arrange
            var program = ProgramBuilder.Typical().Build();

            // Act
            var firestore = _sut.ProgramToFirestore(program);
            var result = _sut.FirestoreToProgram(firestore);

            // Assert
            var expected = ProgramBuilder.Default()
                                         .WithName(program.Name)
                                         .WithSourceUrl(program.SourceUrl)
                                         .WithImageUrl(program.ImageUrl)
                                         .Build();
            result.Should().BeEquivalentTo(expected);
        }


        [Test]
        public void WorkoutShouldBeMappedToFirestore()
        {
            // Arrange
            var provider = ProviderBuilder.Typical().Build();
            var workout = provider.Programs.First().Workouts.First();

            // Act
            var result = _sut.WorkoutToFirestore(workout);

            // Assert
            var expected = new FirestoreWorkout
            {
                Id = workout.Id,
                Name = workout.Name,
                Date = workout.Date,
                Content = workout.Content,
                Url = workout.Url,
                SourceUrl = workout.SourceUrl,
                ImageUrl = workout.ImageUrl,
                Provider = new FirestoreProviderInfo
                {
                    Id = workout.Program.Provider.Id, Name = workout.Program.Provider.Name
                },
                Program = new FirestoreProgramInfo {Id = workout.Program.Id, Name = workout.Program.Name}
            };
            result.Should().BeEquivalentTo(expected);
        }


        [Test]
        public void FirestoreShouldBeMappedToWorkout()
        {
            // Arrange
            var workout = WorkoutBuilder.Simple().Build();

            // Act
            var firestore = _sut.WorkoutToFirestore(workout);
            var result = _sut.FirestoreToWorkout(firestore);

            // Assert
            var expected = WorkoutBuilder.Default()
                                         .WithName(workout.Name)
                                         .WithDate(workout.Date)
                                         .WithContent(workout.Content)
                                         .WithUrl(workout.Url)
                                         .WithSourceUrl(workout.SourceUrl)
                                         .WithImageUrl(workout.ImageUrl)
                                         .Build();

            result.Should().BeEquivalentTo(expected);
        }
    }
}
