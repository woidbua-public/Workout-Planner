﻿using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;
using WorkoutPlanner.Domain.Builders;
using WorkoutPlanner.Persistence.Mappings.Firestore;
using WorkoutPlanner.Persistence.Repositories;
using WorkoutPlanner.Persistence.Repositories.Firestore;

namespace WorkoutPlanner.Persistence.Tests.Repositories.Firestore
{
    [TestFixture]
    public class FirestoreRepositoryTests
    {
        [SetUp]
        public void SetUp()
        {
            _mapping = new FirestoreMapping();
            _sut = new DevFirestoreRepository(_mapping);
        }


        private IFirestoreMapping _mapping;
        private IRepository _sut;


        [Test]
        public async Task ProviderShouldBeAdded()
        {
            // Arrange
            var provider = ProviderBuilder.Simple().Build();

            // Act
            await _sut.AddProvider(provider);
            var actualProvider = await _sut.GetProvider(provider.Id);

            // Assert
            var expectedProvider = provider.Clone();
            expectedProvider.ClearPrograms();
            actualProvider.Should().BeEquivalentTo(expectedProvider);
        }


        [Test]
        public async Task ProgramShouldBeAdded()
        {
            // Arrange
            var provider = ProviderBuilder.Simple().Build();
            var program = ProgramBuilder.Simple().WithProvider(provider).Build();

            // Act
            await _sut.AddProgram(program);
            var actualProgram = await _sut.GetProgram(program.Id);

            // Assert
            var expectedProgram = program.Clone();
            expectedProgram.Provider = null;
            expectedProgram.ClearWorkouts();
            actualProgram.Should().BeEquivalentTo(expectedProgram);
        }


        [Test]
        public async Task WorkoutShouldBeAdded()
        {
            // Arrange
            var provider = ProviderBuilder.Simple().Build();
            var program = ProgramBuilder.Simple().WithProvider(provider).Build();
            var workout = WorkoutBuilder.Simple().WithProgram(program).Build();

            // Act
            await _sut.AddWorkout(workout);
            var actualWorkout = await _sut.GetWorkout(workout.Id);

            // Assert
            var expectedWorkout = workout.Clone();
            expectedWorkout.Program = null;
            actualWorkout.Should().BeEquivalentTo(expectedWorkout);
        }
    }
}
