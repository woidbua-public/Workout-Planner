﻿using System;
using FluentAssertions;
using NUnit.Framework;
using Snapper;
using WorkoutPlanner.Core.Converters;
using WorkoutPlanner.Domain.Builders;
using WorkoutPlanner.Persistence.Converters;

namespace WorkoutPlanner.Persistence.Tests.Converters
{
    [TestFixture]
    public class JsonEntityConverterTests
    {
        [SetUp]
        public void SetUp()
        {
            _sut = new JsonEntityConverter();
        }


        private IJsonEntityConverter _sut;


        [Test]
        public void SerializerShouldConvertWorkoutWithoutProgramCorrectly()
        {
            // Arrange
            var workout = WorkoutBuilder.Default()
                                        .WithName("workout")
                                        .WithDate(new DateTime(2000, 11, 12))
                                        .WithContent("content")
                                        .WithUrl("url")
                                        .WithSourceUrl("sourceUrl")
                                        .WithImageUrl("imageUrl")
                                        .Build();

            // Act

            // Assert
            workout.ShouldMatchSnapshot();
        }


        [Test]
        public void SerializerShouldConvertWorkoutWithProgramCorrectly()
        {
            // Arrange
            var program = ProgramBuilder.Default().WithName("program").Build();
            var workout = WorkoutBuilder.Default()
                                        .WithName("workout")
                                        .WithDate(new DateTime(2000, 11, 12))
                                        .WithContent("content")
                                        .WithUrl("url")
                                        .WithSourceUrl("sourceUrl")
                                        .WithImageUrl("imageUrl")
                                        .Build();
            program.AddWorkout(workout);

            // Act

            // Assert
            workout.ShouldMatchSnapshot();
        }


        [Test]
        public void EntityConverterShouldConvertWorkoutCorrectly()
        {
            // Arrange
            var workout = WorkoutBuilder.Typical().Build();
            var workoutString = JsonConverter.SerializeObject(workout);

            // Act
            var actual = _sut.ConvertToWorkout(workoutString);

            // Assert
            actual.Should().BeEquivalentTo(workout);
        }


        [Test]
        public void SerializerShouldConvertProgramWithProviderAndWorkoutsCorrectly()
        {
            // Arrange
            var provider = ProviderBuilder.Default()
                                          .WithName("provider")
                                          .WithSourceUrl("sourceUrl")
                                          .WithImageUrl("imageUrl")
                                          .Build();
            var program = ProgramBuilder.Default()
                                        .WithName("program")
                                        .WithSourceUrl("sourceUrl")
                                        .WithImageUrl("imageUrl")
                                        .Build();
            var workout = WorkoutBuilder.Default()
                                        .WithName("workout")
                                        .WithDate(new DateTime(2000, 11, 12))
                                        .WithContent("content")
                                        .WithUrl("url")
                                        .WithSourceUrl("sourceUrl")
                                        .WithImageUrl("imageUrl")
                                        .Build();
            provider.AddProgram(program);
            program.AddWorkout(workout);

            // Act

            // Assert
            program.ShouldMatchSnapshot();
        }


        [Test]
        public void SerializerShouldConvertProgramWithoutProviderButWorkoutsCorrectly()
        {
            // Arrange
            var program = ProgramBuilder.Default()
                                        .WithName("program")
                                        .WithSourceUrl("sourceUrl")
                                        .WithImageUrl("imageUrl")
                                        .Build();
            var workout = WorkoutBuilder.Default()
                                        .WithName("workout")
                                        .WithDate(new DateTime(2000, 11, 12))
                                        .WithContent("content")
                                        .WithUrl("url")
                                        .WithSourceUrl("sourceUrl")
                                        .WithImageUrl("imageUrl")
                                        .Build();
            program.AddWorkout(workout);

            // Act

            // Assert
            program.ShouldMatchSnapshot();
        }


        [Test]
        public void SerializerShouldConvertProgramWithProviderButWithoutWorkoutsCorrectly()
        {
            // Arrange
            var provider = ProviderBuilder.Default()
                                          .WithName("provider")
                                          .WithSourceUrl("sourceUrl")
                                          .WithImageUrl("imageUrl")
                                          .Build();
            var program = ProgramBuilder.Default()
                                        .WithName("program")
                                        .WithSourceUrl("sourceUrl")
                                        .WithImageUrl("imageUrl")
                                        .Build();
            provider.AddProgram(program);

            // Act

            // Assert
            program.ShouldMatchSnapshot();
        }


        [Test]
        public void SerializerShouldConvertProgramWithoutProviderAndWorkoutsCorrectly()
        {
            // Arrange
            var program = ProgramBuilder.Default()
                                        .WithName("program")
                                        .WithSourceUrl("sourceUrl")
                                        .WithImageUrl("imageUrl")
                                        .Build();

            // Act

            // Assert
            program.ShouldMatchSnapshot();
        }


        [Test]
        public void EntityConverterShouldConvertProgramCorrectly()
        {
            // Arrange
            var program = ProgramBuilder.Typical().Build();
            var programString = JsonConverter.SerializeObject(program);

            // Act
            var actual = _sut.ConvertToProgram(programString);

            // Assert
            actual.Should().BeEquivalentTo(program, config => config.IgnoringCyclicReferences());
        }


        [Test]
        public void SerializerShouldConvertProviderWithProgramAndWorkoutsCorrectly()
        {
            // Arrange
            var provider = ProviderBuilder.Default()
                                          .WithName("provider")
                                          .WithSourceUrl("sourceUrl")
                                          .WithImageUrl("imageUrl")
                                          .Build();
            var program = ProgramBuilder.Default()
                                        .WithName("program")
                                        .WithSourceUrl("sourceUrl")
                                        .WithImageUrl("imageUrl")
                                        .Build();
            var workout = WorkoutBuilder.Default()
                                        .WithName("workout")
                                        .WithDate(new DateTime(2000, 11, 12))
                                        .WithContent("content")
                                        .WithUrl("url")
                                        .WithSourceUrl("sourceUrl")
                                        .WithImageUrl("imageUrl")
                                        .Build();
            provider.AddProgram(program);
            program.AddWorkout(workout);

            // Act

            // Assert
            provider.ShouldMatchSnapshot();
        }


        [Test]
        public void SerializerShouldConvertProviderWithoutProgramCorrectly()
        {
            // Arrange
            var provider = ProviderBuilder.Default()
                                          .WithName("provider")
                                          .WithSourceUrl("sourceUrl")
                                          .WithImageUrl("imageUrl")
                                          .Build();

            // Act

            // Assert
            provider.ShouldMatchSnapshot();
        }


        [Test]
        public void SerializerShouldConvertProviderWithProgramButWithoutWorkoutsCorrectly()
        {
            // Arrange
            var provider = ProviderBuilder.Default()
                                          .WithName("provider")
                                          .WithSourceUrl("sourceUrl")
                                          .WithImageUrl("imageUrl")
                                          .Build();
            var program = ProgramBuilder.Default()
                                        .WithName("program")
                                        .WithSourceUrl("sourceUrl")
                                        .WithImageUrl("imageUrl")
                                        .Build();

            provider.AddProgram(program);

            // Act

            // Assert
            provider.ShouldMatchSnapshot();
        }


        [Test]
        public void EntityConverterShouldConvertProviderCorrectly()
        {
            // Arrange
            var provider = ProviderBuilder.Typical().Build();
            var providerString = JsonConverter.SerializeObject(provider);

            // Act
            var actual = _sut.ConvertToProvider(providerString);

            // Assert
            actual.Should().BeEquivalentTo(provider, config => config.IgnoringCyclicReferences());
        }
    }
}
