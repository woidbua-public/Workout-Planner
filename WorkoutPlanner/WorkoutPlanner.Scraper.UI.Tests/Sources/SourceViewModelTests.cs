﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;
using NSubstitute;
using NUnit.Framework;
using WorkoutPlanner.Scraper.Sources;

namespace WorkoutPlanner.Scraper.UI.Sources
{
    [TestFixture]
    public class SourceViewModelTests
    {
        [SetUp]
        public void SetUp()
        {
            _isBusyEvents = new List<bool>();
            _source = Substitute.For<ISource>();
            _sut = new SourceViewModel(_source, b => { _isBusyEvents.Add(b); });
        }


        private ISource _source;
        private List<bool> _isBusyEvents;
        private SourceViewModel _sut;


        [Test]
        public async Task CrawlUrlsShouldExecuteCorrectSourceMethod()
        {
            // Arrange

            // Act
            await _sut.CrawlUrlsAsyncCommand.Execute();

            // Assert
            _source.Received(1).ExecuteUrlCrawler();
        }


        [Test]
        public async Task CrawlHtmlShouldExecuteCorrectSourceMethod()
        {
            // Arrange

            // Act
            await _sut.CrawlHtmlAsyncCommand.Execute();

            // Assert
            _source.Received(1).ExecuteHtmlCrawler();
        }


        [Test]
        public async Task CrawlAllShouldExecuteCorrectSourceMethod()
        {
            // Arrange

            // Act
            await _sut.CrawlAllAsyncCommand.Execute();

            // Assert
            _source.Received(1).ExecuteCrawlers();
        }


        [Test]
        public async Task CrawlShouldSetAndResetIsBusyAction()
        {
            // Arrange

            // Act
            await _sut.CrawlAllAsyncCommand.Execute();

            // Assert
            AssertHasCorrectBusyEvents();
        }


        [Test]
        public async Task ParseHtmlShouldExecuteCorrectSourceMethod()
        {
            // Arrange

            // Act
            await _sut.ParseAsyncCommand.Execute();

            // Assert
            _source.Received(1).ExecuteParser();
        }


        [Test]
        public async Task ParseHtmlShouldSetAndResetIsBusyAction()
        {
            // Arrange

            // Act
            await _sut.ParseAsyncCommand.Execute();

            // Assert
            AssertHasCorrectBusyEvents();
        }


        [Test]
        public async Task CrawlAndParseShouldExecuteCorrectSourceMethod()
        {
            // Arrange

            // Act
            await _sut.CrawlAndParseAsyncCommand.Execute();

            // Assert
            _source.Received(1).ExecuteCrawlers();
            _source.Received(1).ExecuteParser();
        }


        [Test]
        public async Task CrawlAndParseShouldSetAndResetIsBusyAction()
        {
            // Arrange

            // Act
            await _sut.CrawlAndParseAsyncCommand.Execute();

            // Assert
            AssertHasCorrectBusyEvents();
        }


        [Test]
        public async Task UploadShouldExecuteCorrectSourceMethod()
        {
            // Arrange

            // Act
            await _sut.UploadAsyncCommand.Execute();

            // Assert
            _source.Received(1).ExecuteUploader();
        }


        [Test]
        public async Task UploadShouldSetAndResetIsBusyAction()
        {
            // Arrange

            // Act
            await _sut.UploadAsyncCommand.Execute();

            // Assert
            AssertHasCorrectBusyEvents();
        }


        [Test]
        public async Task ExecuteAllShouldExecuteCorrectSourceMethod()
        {
            // Arrange

            // Act
            await _sut.ExecuteAllAsyncCommand.Execute();

            // Assert
            _source.Received(1).ExecuteCrawlers();
            _source.Received(1).ExecuteParser();
            _source.Received(1).ExecuteUploader();
        }


        [Test]
        public async Task ExecuteAllShouldSetAndResetIsBusyAction()
        {
            // Arrange

            // Act
            await _sut.ExecuteAllAsyncCommand.Execute();

            // Assert
            AssertHasCorrectBusyEvents();
        }


        private void AssertHasCorrectBusyEvents()
        {
            _isBusyEvents.Should().HaveCount(2);
            _isBusyEvents[0].Should().BeTrue();
            _isBusyEvents[1].Should().BeFalse();
        }
    }
}
