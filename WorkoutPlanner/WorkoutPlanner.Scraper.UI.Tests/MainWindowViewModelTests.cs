﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using NSubstitute;
using NUnit.Framework;
using WorkoutPlanner.Scraper.Sources;

namespace WorkoutPlanner.Scraper.UI
{
    [TestFixture]
    public class MainWindowViewModelTests
    {
        [SetUp]
        public void SetUp()
        {
            _sources = SetupSources().ToList();
            _sut = new MainWindowViewModel(_sources);
        }


        private const int SourceCount = 5;
        private IEnumerable<ISource> _sources;
        private MainWindowViewModel _sut;


        [Test]
        public void ConstructorCallShouldInitializeSources()
        {
            // Arrange

            // Act
            var actual = _sut.Sources.ToList();

            // Assert
            actual.Should().HaveCount(SourceCount);
            actual.ForEach(svm => svm.Name.Should().BeOneOf(_sources.Select(s => s.Name)));
        }


        [Test]
        public void IsBusyShouldBeFalseWhenInitialized()
        {
            // Arrange

            // Act

            // Assert
            _sut.IsBusy.Should().BeFalse();
        }


        [Test]
        public async Task ExecuteEverythingShouldInvokeAllExecuteTasksFromSources()
        {
            // Arrange

            // Act
            await _sut.ExecuteEverythingAsyncCommand.Execute();

            // Assert
            foreach (var source in _sources)
            {
                source.Received(1).ExecuteCrawlers();
                source.Received(1).ExecuteParser();
                source.Received(1).ExecuteUploader();
            }
        }


        private static IEnumerable<ISource> SetupSources()
        {
            for (var i = 0; i < SourceCount; i++)
            {
                var source = Substitute.For<ISource>();
                source.Name.Returns($"source_{i}");
                yield return source;
            }
        }
    }
}
