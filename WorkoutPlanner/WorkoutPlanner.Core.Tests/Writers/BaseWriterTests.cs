﻿using NUnit.Framework;

namespace WorkoutPlanner.Core.Writers
{
    [TestFixture]
    public abstract class BaseWriterTests
    {
        [Test]
        public abstract void ShouldWriteContentToTarget();


        [Test]
        public abstract void ShouldWriteLineToTarget();


        [Test]
        public abstract void ShouldWriteLinesToTarget();
    }
}
