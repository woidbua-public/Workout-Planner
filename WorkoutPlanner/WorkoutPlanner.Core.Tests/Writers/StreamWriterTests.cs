﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using AutoFixture;
using FluentAssertions;
using NUnit.Framework;

namespace WorkoutPlanner.Core.Writers
{
    [TestFixture]
    public class StreamWriterTests : BaseWriterTests
    {
        [SetUp]
        public void SetUp()
        {
            _memoryStream = new MemoryStream();
            _sut = new StreamWriter(_memoryStream);
        }


        private static readonly Fixture Fixture = new Fixture();
        private static readonly IEnumerable<string> SampleLines = Fixture.CreateMany<string>();
        private static readonly string Content = string.Join(Environment.NewLine, SampleLines);

        private MemoryStream _memoryStream;
        private StreamWriter _sut;


        [Test]
        public override void ShouldWriteContentToTarget()
        {
            // Arrange

            // Act
            _sut.Write(Content);

            // Assert
            GetMemoryStreamContent().Should().Be(Content);
        }


        [Test]
        public override void ShouldWriteLineToTarget()
        {
            // Arrange
            var line = SampleLines.First();

            // Act
            _sut.WriteLine(line);

            // Assert
            var expected = line + Environment.NewLine;
            GetMemoryStreamContent().Should().Be(expected);
        }


        [Test]
        public override void ShouldWriteLinesToTarget()
        {
            // Arrange

            // Act
            _sut.WriteLines(SampleLines);

            // Assert
            var expected = Content + Environment.NewLine;
            GetMemoryStreamContent().Should().Be(expected);
        }


        private string GetMemoryStreamContent()
        {
            return Encoding.ASCII.GetString(_memoryStream.ToArray());
        }
    }
}
