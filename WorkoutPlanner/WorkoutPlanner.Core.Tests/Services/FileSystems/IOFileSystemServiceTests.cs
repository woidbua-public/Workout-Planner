﻿using System;
using System.Collections.Generic;
using System.IO;
using FluentAssertions;
using NUnit.Framework;

namespace WorkoutPlanner.Core.Services.FileSystems
{
    // ReSharper disable once InconsistentNaming
    [TestFixture]
    public class IOFileSystemServiceTests : BaseFileSystemServiceTests
    {
        [SetUp]
        public void SetUp()
        {
            DeleteTempFolder();

            _sut = new IOFileSystemService();
        }


        [TearDown]
        public void TearDown()
        {
            DeleteTempFolder();
        }


        private static void DeleteTempFolder()
        {
            if (Directory.Exists(TempDirectory))
                Directory.Delete(TempDirectory, true);
        }


        private static readonly string TempDirectory = Path.Combine(
            AppDomain.CurrentDomain.BaseDirectory!,
            "IOFileSystemServiceTestFolder"
        );

        private IOFileSystemService _sut;


        [Test]
        public override void FileExistsShouldReturnTrueWhenFileExists()
        {
            // Arrange
            var filePath = CreateFilePath("test.txt");
            _sut.CreateFile(filePath);

            // Act
            var actual = _sut.FileExists(filePath);

            // Assert
            actual.Should().BeTrue();
        }


        [Test]
        public override void FileExistsShouldReturnFalseWhenFileDoesNotExist()
        {
            // Arrange
            var filePath = CreateFilePath("test.txt");

            // Act
            var actual = _sut.FileExists(filePath);

            // Assert
            actual.Should().BeFalse();
        }


        [Test]
        public override void DirectoryExistsShouldReturnTrueWhenDirectoryExists()
        {
            // Arrange
            _sut.CreateDirectory(TempDirectory);

            // Act
            var actual = _sut.DirectoryExists(TempDirectory);

            // Assert
            actual.Should().BeTrue();
        }


        [Test]
        public override void DirectoryExistsShouldReturnFalseWhenDirectoryDoesNotExist()
        {
            // Arrange

            // Act
            var actual = _sut.DirectoryExists(TempDirectory);

            // Assert
            actual.Should().BeFalse();
        }


        [Test]
        public override void DirectoryPathShouldBeCreatedForGivenDirectoryPathWhenNotExists()
        {
            // Arrange
            var directoryPath = Path.Combine(TempDirectory, "folder1", "folder2", "folder3");

            // Act
            _sut.CreateDirectory(directoryPath);

            // Assert
            _sut.DirectoryExists(directoryPath).Should().BeTrue();
        }


        [Test]
        public override void DirectoryPathShouldBeCreatedForGivenFilePathWhenNotExists()
        {
            // Arrange
            var directoryPath = Path.Combine(TempDirectory, "folder1", "folder2", "folder3");
            var filePath = Path.Combine(directoryPath, "test.txt");

            // Act
            _sut.CreateFile(filePath);

            // Assert
            _sut.DirectoryExists(directoryPath).Should().BeTrue();
        }


        [Test]
        public override void GetFilesShouldReturnEmptyListWhenDirectoryDoesNotExist()
        {
            // Arrange

            // Act
            var actual = _sut.GetFilePaths("testFolder");

            // Assert
            actual.Should().BeEmpty();
        }


        [Test]
        public override void GetFilesShouldReturnAllFilesForSpecifiedDirectoryPath()
        {
            // Arrange
            static string GenerateFilePath() => Path.Combine(TempDirectory, Path.GetRandomFileName());

            var filePaths = new List<string>
            {
                GenerateFilePath(),
                GenerateFilePath(),
                GenerateFilePath()
            };
            filePaths.ForEach(fp => _sut.CreateFile(fp));

            // Act
            var actual = _sut.GetFilePaths(TempDirectory);

            // Assert
            actual.Should().BeEquivalentTo(filePaths);
        }


        private static string CreateFilePath(string fileName)
        {
            return Path.Combine(TempDirectory, fileName);
        }
    }
}
