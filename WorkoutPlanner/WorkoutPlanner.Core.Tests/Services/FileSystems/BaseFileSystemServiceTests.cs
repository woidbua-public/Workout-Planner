﻿using NUnit.Framework;

namespace WorkoutPlanner.Core.Services.FileSystems
{
    [TestFixture]
    public abstract class BaseFileSystemServiceTests
    {
        [Test]
        public abstract void FileExistsShouldReturnTrueWhenFileExists();


        [Test]
        public abstract void FileExistsShouldReturnFalseWhenFileDoesNotExist();


        [Test]
        public abstract void DirectoryExistsShouldReturnTrueWhenDirectoryExists();


        [Test]
        public abstract void DirectoryExistsShouldReturnFalseWhenDirectoryDoesNotExist();


        [Test]
        public abstract void DirectoryPathShouldBeCreatedForGivenDirectoryPathWhenNotExists();


        [Test]
        public abstract void DirectoryPathShouldBeCreatedForGivenFilePathWhenNotExists();


        [Test]
        public abstract void GetFilesShouldReturnEmptyListWhenDirectoryDoesNotExist();


        [Test]
        public abstract void GetFilesShouldReturnAllFilesForSpecifiedDirectoryPath();
    }
}
