﻿using System;
using AutoFixture;

namespace WorkoutPlanner.Core.Fixtures.Builders
{
    public abstract class Builder<T>
    {
        protected Lazy<T> Instance;

        // ReSharper disable once StaticMemberInGenericType
        protected static Fixture Fixture { get; } = new Fixture();


        public T Build()
        {
            if (Instance?.IsValueCreated != true)
                Instance = new Lazy<T>(GenerateInstance());

            PostBuild(Instance.Value);
            return Instance.Value;
        }


        protected abstract T GenerateInstance();


        protected virtual void PostBuild(T instance)
        {
        }
    }
}
