﻿using System;
using System.IO;
using System.Text;
using FluentAssertions;
using NUnit.Framework;

namespace WorkoutPlanner.Core.Readers
{
    [TestFixture]
    public class StreamReaderTests : BaseReaderTests
    {
        [SetUp]
        public void SetUp()
        {
            var byteStream = Encoding.ASCII.GetBytes(Content);
            _sut = new StreamReader(new MemoryStream(byteStream));
        }


        public static readonly string[] SampleLines = {"Text number 1", "Text number 2", "Text number 3"};
        public static readonly string Content = string.Join(Environment.NewLine, SampleLines);

        private StreamReader _sut;


        [Test]
        public override void ShouldReadWholeContentFromSource()
        {
            // Arrange

            // Act
            var actual = _sut.Read();

            // Assert
            actual.Should().Be(Content);
        }


        [Test]
        public override void ShouldReadAllSingleLinesFromSource()
        {
            // Arrange

            // Act
            var actual = _sut.ReadLines();

            // Assert
            actual.Should().BeEquivalentTo(SampleLines);
        }
    }
}
