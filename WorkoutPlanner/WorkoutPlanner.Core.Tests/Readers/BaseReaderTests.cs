﻿using NUnit.Framework;

namespace WorkoutPlanner.Core.Readers
{
    [TestFixture]
    public abstract class BaseReaderTests
    {
        [Test]
        public abstract void ShouldReadWholeContentFromSource();


        [Test]
        public abstract void ShouldReadAllSingleLinesFromSource();
    }
}
