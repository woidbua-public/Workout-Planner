import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { SharedComponentsModule } from '../shared/components';
import {
  ProviderBreadcrumbComponent,
  ProviderListComponent,
  ProviderListItemComponent
} from './components';
import { ProviderCollectionPageComponent } from './containers';
import { ProvidersEffects } from './effects';
import { ProvidersRoutingModule } from './providers-routing.module';
import * as fromProviders from './reducers';

export const COMPONENTS = [
  ProviderBreadcrumbComponent,
  ProviderListComponent,
  ProviderListItemComponent,
];

export const CONTAINERS = [ProviderCollectionPageComponent];

@NgModule({
  imports: [
    CommonModule,
    ProvidersRoutingModule,
    StoreModule.forFeature(
      fromProviders.providersFeatureKey,
      fromProviders.reducers
    ),
    EffectsModule.forFeature([ProvidersEffects]),
    FontAwesomeModule,
    SharedComponentsModule,
  ],
  declarations: [COMPONENTS, CONTAINERS],
})
export class ProvidersModule {}
