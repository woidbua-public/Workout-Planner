import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { ProvidersActions } from '../actions';
import { Provider } from '../models';

export const providersFeatureKey = 'providers';

export interface State extends EntityState<Provider> {
  selectedProviderId: string | null;
  loaded: boolean;
  loading: boolean;
  error: any;
}

export const adapter: EntityAdapter<Provider> = createEntityAdapter<Provider>({
  selectId: (provider: Provider) => provider.id,
  sortComparer: (p1: Provider, p2: Provider) => p1.name.localeCompare(p2.name),
});

export const initialState: State = adapter.getInitialState({
  selectedProviderId: null,
  loaded: false,
  loading: false,
  error: null,
});

export const reducer = createReducer(
  initialState,
  on(ProvidersActions.loadProviders, (state) => ({
    ...state,
    loading: true,
    error: null,
  })),
  on(ProvidersActions.loadProvidersSuccess, (state, { providers }) =>
    adapter.setAll(providers, {
      ...state,
      loaded: true,
      loading: false,
      error: null,
    })
  ),
  on(ProvidersActions.loadProvidersSkipped, (state) => ({
    ...state,
    loading: false,
  })),
  // tslint:disable-next-line: no-shadowed-variable
  on(ProvidersActions.loadProvidersFailure, (state, { error }) => ({
    ...state,
    loaded: false,
    loading: false,
    error,
  })),
  on(ProvidersActions.selectProvider, (state) => ({
    ...state,
    error: null,
  })),
  on(ProvidersActions.selectProviderSuccess, (state, { provider }) =>
    adapter.addOne(provider, {
      ...state,
      selectedProviderId: provider.id,
      error: null,
    })
  ),
  on(ProvidersActions.selectProviderSkipped, (state) => state),
  // tslint:disable-next-line: no-shadowed-variable
  on(ProvidersActions.selectProviderFailure, (state, { error }) => ({
    ...state,
    loaded: false,
    error,
  }))
);

export const selectedProviderId = (state: State) => state.selectedProviderId;

export const loaded = (state: State) => state.loaded;

export const loading = (state: State) => state.loading;

export const error = (state: State) => state.error;
