import {
  Action,
  combineReducers,
  createFeatureSelector,
  createSelector
} from '@ngrx/store';
import * as fromRoot from '../../reducers';
import * as fromProviders from './providers.reducer';

export const providersFeatureKey = 'providers';

export interface ProvidersState {
  [fromProviders.providersFeatureKey]: fromProviders.State;
}

export interface State extends fromRoot.State {
  [providersFeatureKey]: ProvidersState;
}

export function reducers(
  state: ProvidersState,
  action: Action
): ProvidersState {
  return combineReducers({
    [fromProviders.providersFeatureKey]: fromProviders.reducer,
  })(state, action);
}

/*
 * Providers Selectors
 */
export const selectProvidersState = createFeatureSelector<
  State,
  ProvidersState
>(providersFeatureKey);

export const selectProviderEntitiesState = createSelector(
  selectProvidersState,
  (state) => state.providers
);

export const {
  selectIds: selectProviderIds,
  selectEntities: selectProviderEntities,
  selectAll: selectAllProviders,
  selectTotal: selectTotalProviders,
} = fromProviders.adapter.getSelectors(selectProviderEntitiesState);

export const selectSelectedProviderId = createSelector(
  selectProviderEntitiesState,
  fromProviders.selectedProviderId
);

export const selectSelectedProvider = createSelector(
  selectProviderEntities,
  selectSelectedProviderId,
  (entities, selectedId) => {
    return selectedId && entities[selectedId];
  }
);

export const isSelectedProviderInEntities = createSelector(
  selectProviderIds,
  selectSelectedProviderId,
  (ids: string[], selected) => {
    return !!selected && ids.indexOf(selected) > -1;
  }
);

export const selectProvidersLoaded = createSelector(
  selectProviderEntitiesState,
  fromProviders.loaded
);

export const selectProvidersLoading = createSelector(
  selectProviderEntitiesState,
  fromProviders.loading
);

export const selectProvidersError = createSelector(
  selectProviderEntitiesState,
  fromProviders.error
);
