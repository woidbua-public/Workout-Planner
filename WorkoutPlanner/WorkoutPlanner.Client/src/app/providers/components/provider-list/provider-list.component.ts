import { Component, Input } from '@angular/core';
import { Provider } from '../../models';

@Component({
  selector: 'app-provider-list',
  templateUrl: './provider-list.component.html',
})
export class ProviderListComponent {
  @Input() providers: Provider[];
}
