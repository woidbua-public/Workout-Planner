import { Component, Input } from '@angular/core';
import {
  faClipboardList,
  faDirections
} from '@fortawesome/free-solid-svg-icons';
import { Provider } from '../../models';

@Component({
  selector: 'app-provider-list-item',
  templateUrl: './provider-list-item.component.html',
  styleUrls: ['./provider-list-item.component.scss'],
})
export class ProviderListItemComponent {
  faRedirectToProvider = faDirections;
  faGoToPrograms = faClipboardList;
  @Input() provider: Provider;
}
