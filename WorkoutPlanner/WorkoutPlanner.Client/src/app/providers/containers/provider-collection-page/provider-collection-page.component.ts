import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { ProvidersActions } from '../../actions';
import { Provider } from '../../models';
import * as fromProviders from '../../reducers';

@Component({
  selector: 'app-provider-collection-page',
  templateUrl: './provider-collection-page.component.html',
})
export class ProviderCollectionPageComponent implements OnInit, OnDestroy {
  providers$: Observable<Provider[]>;
  isLoadingSub: Subscription;
  isLoading: boolean;

  constructor(private store: Store<fromProviders.State>) {
    this.providers$ = this.store.select(fromProviders.selectAllProviders);
  }

  ngOnInit(): void {
    this.isLoadingSub = this.store
      .select(fromProviders.selectProvidersLoading)
      .subscribe((isLoading) => {
        this.isLoading = isLoading;
      });
    this.store.dispatch(ProvidersActions.loadProviders());
  }

  ngOnDestroy(): void {
    this.isLoadingSub.unsubscribe();
  }
}
