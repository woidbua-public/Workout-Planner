import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../shared/guards';
import { ProviderCollectionPageComponent } from './containers';

export const routes: Routes = [
  {
    path: '',
    component: ProviderCollectionPageComponent,
    canActivate: [AuthGuard],
  },
  {
    path: ':providerId',
    canActivateChild: [AuthGuard],
    loadChildren: () => import('../programs').then((m) => m.ProgramsModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProvidersRoutingModule {}
