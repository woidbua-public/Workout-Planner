import { createAction, props } from '@ngrx/store';
import { Provider } from '../models';

export const loadProviders = createAction('[Providers] Load Providers');

export const loadProvidersSuccess = createAction(
  '[Providers] Load Providers Success',
  props<{ providers: Provider[] }>()
);

export const loadProvidersSkipped = createAction(
  '[Providers] Load Providers Skipped'
);

export const loadProvidersFailure = createAction(
  '[Providers] Load Providers Failure',
  props<{ error: any }>()
);

export const selectProvider = createAction(
  '[Providers] Select Provider',
  props<{ providerId: string }>()
);

export const selectProviderSuccess = createAction(
  '[Providers] Select Provider Success',
  props<{ provider: Provider }>()
);

export const selectProviderSkipped = createAction(
  '[Providers] Select Provider Skipped'
);

export const selectProviderFailure = createAction(
  '[Providers] Select Provider Failure',
  props<{ error: any }>()
);
