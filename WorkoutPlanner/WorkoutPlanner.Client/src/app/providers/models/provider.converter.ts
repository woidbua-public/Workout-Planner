import { FirestoreProvider, Provider } from './provider';

export const providerConverter = {
  fromFirestore: (fp: FirestoreProvider): Provider => ({
    id: fp.id,
    name: fp.name,
    sourceUrl: fp.sourceUrl,
    imageUrl: fp.imageUrl,
  }),
};
