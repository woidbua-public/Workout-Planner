import { EntityInfo } from 'src/app/shared/models';

// tslint:disable-next-line: no-empty-interface
export interface ProviderInfo extends EntityInfo {}

export interface Provider extends ProviderInfo {
  sourceUrl: string;
  imageUrl: string;
}

export interface FirestoreProvider extends ProviderInfo {
  sourceUrl: string;
  imageUrl: string;
}
