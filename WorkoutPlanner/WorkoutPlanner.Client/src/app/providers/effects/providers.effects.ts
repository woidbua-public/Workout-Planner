import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { catchError, map, switchMap, withLatestFrom } from 'rxjs/operators';
import { DataService } from 'src/app/shared/services';
import { ProvidersActions } from '../actions';
import { Provider } from '../models';
import * as fromProviders from '../reducers';

@Injectable()
export class ProvidersEffects {
  constructor(
    private actions$: Actions,
    private store: Store<fromProviders.State>,
    private dataService: DataService
  ) {}

  loadProviders$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ProvidersActions.loadProviders),
      withLatestFrom(this.store.select(fromProviders.selectProvidersLoaded)),
      switchMap(([_, providersLoaded]) => {
        return providersLoaded
          ? of(ProvidersActions.loadProvidersSkipped())
          : this.dataService.getProviders().pipe(
              map((providers: Provider[]) =>
                ProvidersActions.loadProvidersSuccess({ providers })
              ),
              catchError((error) =>
                of(ProvidersActions.loadProvidersFailure({ error }))
              )
            );
      })
    )
  );

  selectProvider$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ProvidersActions.selectProvider),
      withLatestFrom(
        this.store.select(fromProviders.selectProviderEntitiesState)
      ),
      switchMap(([{ providerId }, { selectedProviderId, entities }]) => {
        if (selectedProviderId === providerId) {
          return of(ProvidersActions.selectProviderSkipped());
        }

        const storeProvider = Object.values(entities).find(
          (p) => p.id === providerId
        );
        if (storeProvider) {
          return of(
            ProvidersActions.selectProviderSuccess({ provider: storeProvider })
          );
        }

        return this.dataService.getProvider(providerId).pipe(
          map((provider) =>
            ProvidersActions.selectProviderSuccess({ provider })
          ),
          catchError((error) =>
            of(ProvidersActions.selectProviderFailure({ error }))
          )
        );
      })
    )
  );
}
