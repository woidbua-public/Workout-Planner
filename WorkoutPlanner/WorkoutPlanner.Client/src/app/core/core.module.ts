import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { HeaderComponent } from './components';
import { AppPageComponent, NotFoundPageComponent } from './containers';

export const COMPONENTS = [HeaderComponent];

export const CONTAINERS = [AppPageComponent, NotFoundPageComponent];

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FontAwesomeModule,
    CollapseModule.forRoot(),
  ],
  declarations: [COMPONENTS, CONTAINERS],
})
export class CoreModule {}
