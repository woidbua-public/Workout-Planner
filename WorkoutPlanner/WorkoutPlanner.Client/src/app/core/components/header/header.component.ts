import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { faDumbbell, faSignOutAlt } from '@fortawesome/free-solid-svg-icons';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { AuthActions } from 'src/app/auth/actions';
import * as fromAuth from 'src/app/auth/reducers';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit, OnDestroy {
  favicon = faDumbbell;
  faLogout = faSignOutAlt;
  userSub: Subscription;
  loggedIn: boolean;
  isCollapsed = true;

  constructor(
    private authStore: Store<fromAuth.State>,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.userSub = this.authStore
      .select(fromAuth.selectAuthUser)
      .subscribe((user) => {
        this.loggedIn = !!user;
      });
  }

  onLogout(): void {
    this.authStore.dispatch(AuthActions.logout());
    this.router.navigate(['/login']);
  }

  ngOnDestroy(): void {
    this.userSub.unsubscribe();
  }
}
