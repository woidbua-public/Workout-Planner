import { Action, ActionReducerMap } from '@ngrx/store';

// tslint:disable-next-line: no-empty-interface
export interface State {}

export const rootReducers: ActionReducerMap<State, Action> = {};
