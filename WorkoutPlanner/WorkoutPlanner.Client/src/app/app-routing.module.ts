import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthPageComponent } from './auth/containers';
import { NotFoundPageComponent } from './core/containers';
import { AuthGuard, LoggedInGuard } from './shared/guards';

const routes: Routes = [
  { path: '', redirectTo: '/providers', pathMatch: 'full' },
  {
    path: 'login',
    component: AuthPageComponent,
    canActivate: [LoggedInGuard],
  },
  {
    path: 'providers',
    loadChildren: () => import('./providers').then((m) => m.ProvidersModule),
  },
  {
    path: '**',
    component: NotFoundPageComponent,
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
