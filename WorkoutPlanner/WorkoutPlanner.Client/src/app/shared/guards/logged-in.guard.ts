import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree
} from '@angular/router';
import { Store } from '@ngrx/store';
import { first } from 'rxjs/operators';
import { AuthActions } from 'src/app/auth/actions';
import * as fromAuth from '../../auth/reducers';
import { AuthService } from '../services';

@Injectable({
  providedIn: 'root',
})
export class LoggedInGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private store: Store<fromAuth.State>,
    private router: Router
  ) {}

  async canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<boolean | UrlTree> {
    const { user, triedAutoLogin } = await this.store
      .select(fromAuth.selectAuthEntitiesState)
      .pipe(first())
      .toPromise();

    if (user) {
      this.store.dispatch(AuthActions.autoLoginSuccess({ user }));
      return this.router.createUrlTree(['/providers']);
    }

    if (!triedAutoLogin) {
      const autoLoginUser = await this.authService.autoLogin();
      if (autoLoginUser) {
        this.store.dispatch(
          AuthActions.autoLoginSuccess({ user: autoLoginUser })
        );
        return this.router.createUrlTree(['/providers']);
      }
    }

    return true;
  }
}
