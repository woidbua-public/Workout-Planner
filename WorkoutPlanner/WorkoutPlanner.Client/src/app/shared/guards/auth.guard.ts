import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  CanActivateChild,
  Router,
  RouterStateSnapshot,
  UrlTree
} from '@angular/router';
import { Store } from '@ngrx/store';
import { first } from 'rxjs/operators';
import { AuthActions } from 'src/app/auth/actions';
import * as fromAuth from '../../auth/reducers';
import { AuthService } from '../services';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate, CanActivateChild {
  constructor(
    private authService: AuthService,
    private store: Store<fromAuth.State>,
    private router: Router
  ) {}

  async canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<boolean | UrlTree> {
    const { user, triedAutoLogin } = await this.store
      .select(fromAuth.selectAuthEntitiesState)
      .pipe(first())
      .toPromise();

    if (user) {
      return true;
    }

    if (!triedAutoLogin) {
      const autoLoginUser = await this.authService.autoLogin();
      if (autoLoginUser) {
        this.store.dispatch(
          AuthActions.autoLoginSuccess({ user: autoLoginUser })
        );
        return true;
      } else {
        this.store.dispatch(AuthActions.autoLoginFailure());
      }
    }

    return this.router.createUrlTree(['/login']);
  }

  canActivateChild(
    childRoute: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<boolean | UrlTree> {
    return this.canActivate(childRoute, state);
  }
}
