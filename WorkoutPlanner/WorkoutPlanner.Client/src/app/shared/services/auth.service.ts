import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { first } from 'rxjs/operators';
import { User, userConverter } from '../models';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private auth: AngularFireAuth) {}

  async login(email: string, password: string): Promise<User> {
    const userCredential = await this.auth.signInWithEmailAndPassword(
      email,
      password
    );
    return userCredential
      ? userConverter.fromFirestore(userCredential.user)
      : null;
  }

  async autoLogin(): Promise<User> {
    const firebaseUser = await this.auth.authState.pipe(first()).toPromise();
    return firebaseUser ? userConverter.fromFirestore(firebaseUser) : null;
  }

  async logout(): Promise<void> {
    return this.auth.signOut();
  }
}
