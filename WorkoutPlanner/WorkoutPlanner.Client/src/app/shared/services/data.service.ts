import { Injectable } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument,
  QueryFn
} from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {
  FirestoreProgram,
  Program,
  programConverter
} from 'src/app/programs/models';
import {
  FirestoreProvider,
  Provider,
  providerConverter
} from 'src/app/providers/models';
import {
  FirestoreWorkout,
  Workout,
  workoutConverter,
  WorkoutQuery
} from 'src/app/workouts/models';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  private readonly providersCollectionKey = 'providers';
  private readonly programsCollectionKey = 'programs';
  private readonly workoutsCollectionKey = 'workouts';

  constructor(private db: AngularFirestore) {}

  getProvider(providerId: string): Observable<Provider> {
    return this.getProviderDoc(providerId)
      .snapshotChanges()
      .pipe(map((fp) => providerConverter.fromFirestore(fp.payload.data())));
  }

  getProviders(): Observable<Provider[]> {
    return this.getProviderCollection()
      .snapshotChanges()
      .pipe(
        map((fps) =>
          fps.map<Provider>((fp) =>
            providerConverter.fromFirestore(fp.payload.doc.data())
          )
        )
      );
  }

  getProgram(providerId: string, programId: string): Observable<Program> {
    return this.getProgramDoc(providerId, programId)
      .snapshotChanges()
      .pipe(map((fp) => programConverter.fromFirestore(fp.payload.data())));
  }

  getPrograms(providerId: string): Observable<Program[]> {
    return this.getProgramCollection(providerId)
      .snapshotChanges()
      .pipe(
        map((fps) =>
          fps.map<Program>((fp) =>
            programConverter.fromFirestore(fp.payload.doc.data())
          )
        )
      );
  }

  getWorkouts(query: WorkoutQuery): Observable<Workout[]> {
    return this.getWorkoutCollection(
      query.providerId,
      query.programId,
      this.generateWorkoutFilter(query)
    )
      .snapshotChanges()
      .pipe(
        map((fws) =>
          fws.map<Workout>((fw) =>
            workoutConverter.fromFirestore(fw.payload.doc.data())
          )
        )
      );
  }

  private generateWorkoutFilter(
    query: WorkoutQuery
  ): QueryFn<FirestoreWorkout> {
    if (query.startDate && query.endDate) {
      return (ref) =>
        ref
          .orderBy('date', query.sortOrder === 'asc' ? 'asc' : 'desc')
          .where('date', '>=', query.startDate)
          .where('date', '<=', query.endDate);
    }

    return (ref) =>
      ref.orderBy('date', query.sortOrder === 'asc' ? 'asc' : 'desc').limit(9);
  }

  private getProviderDoc(
    provider: Provider | string,
    queryFnProviders?: QueryFn<FirestoreProvider>
  ): AngularFirestoreDocument<FirestoreProvider> {
    return this.getProviderCollection(queryFnProviders).doc<FirestoreProvider>(
      this.getProviderId(provider)
    );
  }

  private getProviderId(provider: Provider | string): string {
    return typeof provider === 'string' ? provider : provider.id;
  }

  private getProviderCollection(
    queryFnProviders?: QueryFn<FirestoreProvider>
  ): AngularFirestoreCollection<FirestoreProvider> {
    return this.db
      .collection('environment')
      .doc('prod')
      .collection<FirestoreProvider>(
        this.providersCollectionKey,
        queryFnProviders
      );
  }

  private getProgramDoc(
    provider: Provider | string,
    program: Program | string,
    queryFnPrograms?: QueryFn<FirestoreProgram>
  ): AngularFirestoreDocument<FirestoreProgram> {
    return this.getProgramCollection(
      provider,
      queryFnPrograms
    ).doc<FirestoreProgram>(this.getProgramId(program));
  }

  private getProgramId(program: Program | string): string {
    return typeof program === 'string' ? program : program.id;
  }

  private getProgramCollection(
    provider: Provider | string,
    queryFnPrograms?: QueryFn<FirestoreProgram>
  ): AngularFirestoreCollection<FirestoreProgram> {
    return this.getProviderDoc(provider).collection(
      this.programsCollectionKey,
      queryFnPrograms
    );
  }

  private getWorkoutDoc(
    provider: Provider | string,
    program: Program | string,
    workout: Workout | string,
    queryFnWorkouts?: QueryFn<FirestoreWorkout>
  ): AngularFirestoreDocument<FirestoreWorkout> {
    return this.getWorkoutCollection(
      provider,
      program,
      queryFnWorkouts
    ).doc<FirestoreWorkout>(this.getWorkoutId(workout));
  }

  private getWorkoutId(workout: Workout | string): string {
    return typeof workout === 'string' ? workout : workout.id;
  }

  private getWorkoutCollection(
    provider: Provider | string,
    program: Program | string,
    queryFnWorkouts?: QueryFn<FirestoreWorkout>
  ): AngularFirestoreCollection<FirestoreWorkout> {
    return this.getProgramDoc(provider, program).collection(
      this.workoutsCollectionKey,
      queryFnWorkouts
    );
  }
}
