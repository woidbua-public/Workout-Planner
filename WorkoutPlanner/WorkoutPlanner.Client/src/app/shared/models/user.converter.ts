import firebase from 'firebase/app';
import { User } from './user';

export const userConverter = {
  fromFirestore: (fu: firebase.User): User => {
    return new User(fu.uid, fu.email);
  },
};
