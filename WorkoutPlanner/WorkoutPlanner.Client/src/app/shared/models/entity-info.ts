export interface EntityInfo {
  id: string;
  name: string;
}
