export class User {
  constructor(private uid: string, private email: string) {}

  get Uid(): string {
    return this.uid;
  }

  get Email(): string {
    return this.email;
  }
}
