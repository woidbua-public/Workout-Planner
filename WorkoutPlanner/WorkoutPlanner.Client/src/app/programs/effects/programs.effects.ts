import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { catchError, map, switchMap, withLatestFrom } from 'rxjs/operators';
import { DataService } from 'src/app/shared/services';
import * as fromPrograms from '..//reducers';
import { ProgramsActions } from '../actions';

@Injectable()
export class ProgramsEffects {
  constructor(
    private actions$: Actions,
    private store: Store<fromPrograms.State>,
    private dataService: DataService
  ) {}

  loadPrograms$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ProgramsActions.loadPrograms),
      withLatestFrom(
        this.store.select(fromPrograms.selectProgramEntitiesState)
      ),
      switchMap(([{ providerId }, { loaded, entities }]) => {
        return loaded && Object.values(entities)[0].provider.id === providerId
          ? of(ProgramsActions.loadProgramsSkipped())
          : this.dataService.getPrograms(providerId).pipe(
              map((programs) =>
                ProgramsActions.loadProgramsSuccess({ programs })
              ),
              catchError((error) =>
                of(ProgramsActions.loadProgramsFailure({ error }))
              )
            );
      })
    )
  );

  selectProgram$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ProgramsActions.selectProgram),
      withLatestFrom(
        this.store.select(fromPrograms.selectProgramEntitiesState)
      ),
      switchMap(
        ([{ providerId, programId }, { selectedProgramId, entities }]) => {
          if (selectedProgramId === programId) {
            return of(ProgramsActions.selectProgramSkipped());
          }

          const storeProgram = Object.values(entities).find(
            (p) => p.id === programId
          );
          if (storeProgram) {
            return of(
              ProgramsActions.selectProgramSuccess({ program: storeProgram })
            );
          }

          return this.dataService.getProgram(providerId, programId).pipe(
            map((program) => ProgramsActions.selectProgramSuccess({ program })),
            catchError((error) =>
              of(ProgramsActions.selectProgramFailure({ error }))
            )
          );
        }
      )
    )
  );
}
