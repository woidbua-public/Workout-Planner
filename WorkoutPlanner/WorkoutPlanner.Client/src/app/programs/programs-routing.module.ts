import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProgramCollectionPageComponent } from './containers';

export const routes: Routes = [
  {
    path: '',
    component: ProgramCollectionPageComponent,
  },
  {
    path: ':programId',
    loadChildren: () => import('../workouts').then((m) => m.WorkoutsModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProgramsRoutingModule {}
