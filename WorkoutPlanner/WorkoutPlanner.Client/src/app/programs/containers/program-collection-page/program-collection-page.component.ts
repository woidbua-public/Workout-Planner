import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { ProvidersActions } from 'src/app/providers/actions';
import { Provider } from 'src/app/providers/models';
import * as fromProviders from '../../../providers/reducers';
import { ProgramsActions } from '../../actions';
import { Program } from '../../models';
import * as fromPrograms from '../../reducers';

@Component({
  selector: 'app-program-collection-page',
  templateUrl: './program-collection-page.component.html',
})
export class ProgramCollectionPageComponent implements OnInit, OnDestroy {
  programs$: Observable<Program[]>;
  paramsSub: Subscription;
  providerSub: Subscription;
  provider: Provider;
  isLoadingSub: Subscription;
  isLoading: boolean;

  constructor(
    private providersStore: Store<fromProviders.State>,
    private programsStore: Store<fromPrograms.State>,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.programs$ = this.programsStore.select(fromPrograms.selectAllPrograms);
  }

  ngOnInit(): void {
    this.paramsSub = this.route.params.subscribe(({ providerId }) => {
      if (providerId) {
        this.providersStore.dispatch(
          ProvidersActions.selectProvider({ providerId })
        );
        this.programsStore.dispatch(
          ProgramsActions.loadPrograms({ providerId })
        );
      } else {
        this.router.navigate(['/']);
      }
    });

    this.providerSub = this.providersStore
      .select(fromProviders.selectSelectedProvider)
      .subscribe((provider) => {
        this.provider = provider;
      });

    this.isLoadingSub = this.programsStore
      .select(fromPrograms.selectProgramsLoading)
      .subscribe((isLoading) => {
        this.isLoading = isLoading;
      });
  }

  ngOnDestroy(): void {
    this.paramsSub.unsubscribe();
    this.providerSub.unsubscribe();
    this.isLoadingSub.unsubscribe();
  }
}
