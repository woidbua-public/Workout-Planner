import { createAction, props } from '@ngrx/store';
import { Program } from '../models';

export const loadPrograms = createAction(
  '[Programs] Load Programs',
  props<{ providerId: string }>()
);

export const loadProgramsSuccess = createAction(
  '[Programs] Load Programs Success',
  props<{ programs: Program[] }>()
);

export const loadProgramsSkipped = createAction(
  '[Programs] Load Programs Skipped'
);

export const loadProgramsFailure = createAction(
  '[Programs] Load Programs Failure',
  props<{ error: any }>()
);

export const selectProgram = createAction(
  '[Programs] Select Program',
  props<{ providerId: string; programId: string }>()
);

export const selectProgramSuccess = createAction(
  '[Programs] Select Program Success',
  props<{ program: Program }>()
);

export const selectProgramSkipped = createAction(
  '[Programs] Select Program Skipped'
);

export const selectProgramFailure = createAction(
  '[Programs] Select Program Failure',
  props<{ error: any }>()
);
