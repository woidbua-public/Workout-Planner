import { FirestoreProgram, Program } from './program';

export const programConverter = {
  fromFirestore: (fp: FirestoreProgram): Program => ({
    id: fp.id,
    name: fp.name,
    sourceUrl: fp.sourceUrl,
    imageUrl: fp.imageUrl,
    provider: {
      id: fp.provider.id,
      name: fp.provider.name,
    },
  }),
};
