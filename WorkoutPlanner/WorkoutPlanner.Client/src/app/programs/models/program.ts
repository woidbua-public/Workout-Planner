import { ProviderInfo } from 'src/app/providers/models';
import { EntityInfo } from 'src/app/shared/models';

// tslint:disable-next-line: no-empty-interface
export interface ProgramInfo extends EntityInfo {}

export interface Program extends ProgramInfo {
  sourceUrl: string;
  imageUrl: string;
  provider: ProviderInfo;
}

export interface FirestoreProgram extends ProgramInfo {
  sourceUrl: string;
  imageUrl: string;
  provider: ProviderInfo;
}
