import { Component, Input } from '@angular/core';
import { Program } from '../../models';

@Component({
  selector: 'app-program-list',
  templateUrl: './program-list.component.html',
})
export class ProgramListComponent {
  @Input() programs: Program[];
}
