import { Component, Input } from '@angular/core';
import {
  faClipboardList,
  faDirections
} from '@fortawesome/free-solid-svg-icons';
import { Program } from '../../models';

@Component({
  selector: 'app-program-list-item',
  templateUrl: './program-list-item.component.html',
  styleUrls: ['./program-list-item.component.scss'],
})
export class ProgramListItemComponent {
  faRedirectToProgram = faDirections;
  faGoToWorkouts = faClipboardList;
  @Input() program: Program;
}
