import { Component, Input } from '@angular/core';
import { Provider } from 'src/app/providers/models';

@Component({
  selector: 'app-program-breadcrumb',
  templateUrl: './program-breadcrumb.component.html',
})
export class ProgramBreadcrumbComponent {
  @Input() provider: Provider;
}
