import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { SharedComponentsModule } from '../shared/components';
import {
  ProgramBreadcrumbComponent,
  ProgramListComponent,
  ProgramListItemComponent
} from './components';
import { ProgramCollectionPageComponent } from './containers';
import { ProgramsEffects } from './effects';
import { ProgramsRoutingModule } from './programs-routing.module';
import * as fromPrograms from './reducers';

export const COMPONENTS = [
  ProgramBreadcrumbComponent,
  ProgramListComponent,
  ProgramListItemComponent,
];

export const CONTAINERS = [ProgramCollectionPageComponent];

@NgModule({
  imports: [
    CommonModule,
    ProgramsRoutingModule,
    StoreModule.forFeature(
      fromPrograms.programsFeatureKey,
      fromPrograms.reducers
    ),
    EffectsModule.forFeature([ProgramsEffects]),
    FontAwesomeModule,
    SharedComponentsModule,
  ],
  declarations: [COMPONENTS, CONTAINERS],
})
export class ProgramsModule {}
