import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { ProgramsActions } from '../actions';
import { Program } from '../models';

export const programsFeatureKey = 'programs';

export interface State extends EntityState<Program> {
  selectedProgramId: string | null;
  loaded: boolean;
  loading: boolean;
  error: any;
}

export const adapter: EntityAdapter<Program> = createEntityAdapter<Program>({
  selectId: (program: Program) => program.id,
  sortComparer: (p1: Program, p2: Program) => p1.name.localeCompare(p2.name),
});

export const initialState: State = adapter.getInitialState({
  selectedProgramId: null,
  loaded: false,
  loading: false,
  error: null,
});

export const reducer = createReducer(
  initialState,
  on(ProgramsActions.loadPrograms, (state) => ({
    ...state,
    loading: true,
    error: null,
  })),
  on(ProgramsActions.loadProgramsSuccess, (state, { programs }) =>
    adapter.setAll(programs, {
      ...state,
      loaded: true,
      loading: false,
      error: null,
    })
  ),
  on(ProgramsActions.loadProgramsSkipped, (state) => ({
    ...state,
    loading: false,
  })),
  // tslint:disable-next-line: no-shadowed-variable
  on(ProgramsActions.loadProgramsFailure, (state, { error }) => ({
    ...state,
    loaded: false,
    loading: false,
    error,
  })),
  on(ProgramsActions.selectProgram, (state) => ({
    ...state,
    error: null,
  })),
  on(ProgramsActions.selectProgramSuccess, (state, { program }) =>
    adapter.addOne(program, {
      ...state,
      selectedProgramId: program.id,
      error: null,
    })
  ),
  on(ProgramsActions.selectProgramSkipped, (state) => state),
  // tslint:disable-next-line: no-shadowed-variable
  on(ProgramsActions.selectProgramFailure, (state, { error }) => ({
    ...state,
    loaded: false,
    loading: false,
    error,
  }))
);

export const selectedProgramId = (state: State) => state.selectedProgramId;

export const loaded = (state: State) => state.loaded;

export const loading = (state: State) => state.loading;

export const error = (state: State) => state.error;
