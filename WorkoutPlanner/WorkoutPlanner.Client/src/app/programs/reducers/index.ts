import {
  Action,
  combineReducers,
  createFeatureSelector,
  createSelector
} from '@ngrx/store';
import * as fromRoot from '../../reducers';
import * as fromPrograms from './programs.reducer';

export const programsFeatureKey = 'programs';

export interface ProgramsState {
  [fromPrograms.programsFeatureKey]: fromPrograms.State;
}

export interface State extends fromRoot.State {
  [programsFeatureKey]: ProgramsState;
}

export function reducers(state: ProgramsState, action: Action): ProgramsState {
  return combineReducers({
    [fromPrograms.programsFeatureKey]: fromPrograms.reducer,
  })(state, action);
}

/*
 * Programs Selectors
 */
export const selectProgramsState = createFeatureSelector<State, ProgramsState>(
  programsFeatureKey
);

export const selectProgramEntitiesState = createSelector(
  selectProgramsState,
  (state) => state.programs
);

export const {
  selectIds: selectProgramIds,
  selectEntities: selectProgramEntities,
  selectAll: selectAllPrograms,
  selectTotal: selectTotalPrograms,
} = fromPrograms.adapter.getSelectors(selectProgramEntitiesState);

export const selectSelectedProgramId = createSelector(
  selectProgramEntitiesState,
  fromPrograms.selectedProgramId
);

export const selectSelectedProgram = createSelector(
  selectProgramEntities,
  selectSelectedProgramId,
  (entities, selectedId) => {
    return selectedId && entities[selectedId];
  }
);

export const selectProgramsLoaded = createSelector(
  selectProgramEntitiesState,
  fromPrograms.loaded
);

export const selectProgramsLoading = createSelector(
  selectProgramEntitiesState,
  fromPrograms.loading
);

export const selectProgramsError = createSelector(
  selectProgramEntitiesState,
  fromPrograms.error
);
