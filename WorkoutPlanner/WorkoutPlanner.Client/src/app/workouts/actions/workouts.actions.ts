import { createAction, props } from '@ngrx/store';
import { Workout, WorkoutQuery } from '../models';

export const loadWorkouts = createAction(
  '[Workouts] Load Workouts',
  props<{ query: WorkoutQuery }>()
);

export const loadWorkoutsSuccess = createAction(
  '[Workouts] Load Workouts Success',
  props<{ query: WorkoutQuery; workouts: Workout[] }>()
);

export const loadWorkoutsSkipped = createAction(
  '[Workouts] Load Workouts Skipped'
);

export const loadWorkoutsFailure = createAction(
  '[Workouts] Load Workouts Failure',
  props<{ error: any }>()
);

export const clearWorkouts = createAction('[Workouts] Clear Workouts');

export const resetWorkoutQueryParams = createAction(
  '[Workouts] Reset Workout Query Params'
);
