export interface WorkoutQuery {
  providerId: string;
  programId: string;
  sortOrder: string;
  startDate: Date | null;
  endDate: Date | null;
}
