import firebase from 'firebase/app';
import { ProgramInfo } from 'src/app/programs/models';
import { ProviderInfo } from 'src/app/providers/models';
import { EntityInfo } from 'src/app/shared/models';

import Timestamp = firebase.firestore.Timestamp;

// tslint:disable-next-line: no-empty-interface
export interface WorkoutInfo extends EntityInfo {}

export interface Workout extends WorkoutInfo {
  content: string;
  date: Date;
  url: string;
  sourceUrl: string;
  imageUrl: string;
  provider: ProviderInfo;
  program: ProgramInfo;
}

export interface FirestoreWorkout extends WorkoutInfo {
  content: string;
  date: Timestamp;
  url: string;
  sourceUrl: string;
  imageUrl: string;
  provider: ProviderInfo;
  program: ProgramInfo;
}
