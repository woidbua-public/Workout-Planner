import { FirestoreWorkout, Workout } from './workout';

export const workoutConverter = {
  fromFirestore: (fp: FirestoreWorkout): Workout => ({
    id: fp.id,
    name: fp.name,
    content: fp.content,
    date: fp.date.toDate(),
    url: fp.url,
    sourceUrl: fp.sourceUrl,
    imageUrl: fp.imageUrl,
    provider: {
      id: fp.provider.id,
      name: fp.provider.name,
    },
    program: {
      id: fp.program.id,
      name: fp.program.name,
    },
  }),
};
