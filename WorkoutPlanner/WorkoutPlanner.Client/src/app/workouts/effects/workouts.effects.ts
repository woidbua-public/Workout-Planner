import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { catchError, map, switchMap, withLatestFrom } from 'rxjs/operators';
import { DataService } from 'src/app/shared/services';
import { WorkoutsActions } from '../actions';
import * as fromWorkouts from '../reducers';

@Injectable()
export class WorkoutsEffects {
  constructor(
    private actions$: Actions,
    private workoutsStore: Store<fromWorkouts.State>,
    private dataService: DataService
  ) {}

  loadWorkouts$ = createEffect(() =>
    this.actions$.pipe(
      ofType(WorkoutsActions.loadWorkouts),
      withLatestFrom(
        this.workoutsStore.select(fromWorkouts.selectWorkoutEntitiesState)
      ),
      switchMap(([{ query }, state]) => {
        const {
          programId: queryProgramId,
          startDate: queryStartDate,
          endDate: queryEndDate,
        } = query;

        const { ids: entityIds, entities: entities, query: stateQuery } = state;
        const { startDate: stateStartDate, endDate: stateEndDate } = stateQuery;

        const differentStartDate = queryStartDate !== stateStartDate;
        const differentEndDate = queryEndDate !== stateEndDate;
        const changedEntities =
          entityIds.length === 0 ||
          Object.values(entities)[0].program.id !== queryProgramId;

        if (differentStartDate || differentEndDate || changedEntities) {
          return this.dataService.getWorkouts(query).pipe(
            map((workouts) =>
              WorkoutsActions.loadWorkoutsSuccess({ query, workouts })
            ),
            catchError((error) =>
              of(WorkoutsActions.loadWorkoutsFailure({ error }))
            )
          );
        }

        return of(WorkoutsActions.loadWorkoutsSkipped());
      })
    )
  );
}
