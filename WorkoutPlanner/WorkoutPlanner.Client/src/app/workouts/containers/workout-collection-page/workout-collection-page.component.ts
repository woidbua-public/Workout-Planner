import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
import { ProgramsActions } from 'src/app/programs/actions';
import { Program } from 'src/app/programs/models';
import { ProvidersActions } from 'src/app/providers/actions';
import { Provider } from 'src/app/providers/models';
import * as fromPrograms from '../../../programs/reducers';
import * as fromProviders from '../../../providers/reducers';
import { WorkoutsActions } from '../../actions';
import { Workout } from '../../models';
import * as fromWorkouts from '../../reducers';

@Component({
  selector: 'app-workout-collection-page',
  templateUrl: './workout-collection-page.component.html',
})
export class WorkoutCollectionPageComponent implements OnInit, OnDestroy {
  workouts$: Observable<Workout[]>;
  paramsSub: Subscription;
  providerSub: Subscription;
  provider: Provider;
  programSub: Subscription;
  program: Program;
  isLoadingSub: Subscription;
  isLoading: boolean;

  constructor(
    private providersStore: Store<fromProviders.State>,
    private programsStore: Store<fromPrograms.State>,
    private workoutsStore: Store<fromWorkouts.State>,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.workouts$ = this.workoutsStore.select(fromWorkouts.selectAllWorkouts);
  }

  ngOnInit(): void {
    this.paramsSub = this.route.params.subscribe(
      ({ providerId, programId }) => {
        if (providerId && programId) {
          this.workoutsStore
            .select(fromWorkouts.selectAllWorkouts)
            .pipe(first())
            .toPromise()
            .then((workouts) => {
              if (workouts.length > 0 && workouts[0].program.id !== programId) {
                this.workoutsStore.dispatch(WorkoutsActions.clearWorkouts());
                this.workoutsStore.dispatch(
                  WorkoutsActions.resetWorkoutQueryParams()
                );
              }

              this.providersStore.dispatch(
                ProvidersActions.selectProvider({ providerId })
              );
              this.programsStore.dispatch(
                ProgramsActions.selectProgram({ providerId, programId })
              );
            });
        } else {
          this.router.navigate(['/']);
        }
      }
    );

    this.providerSub = this.providersStore
      .select(fromProviders.selectSelectedProvider)
      .subscribe((provider) => {
        this.provider = provider;
      });

    this.programSub = this.programsStore
      .select(fromPrograms.selectSelectedProgram)
      .subscribe((program) => {
        this.program = program;
      });

    this.isLoadingSub = this.workoutsStore
      .select(fromWorkouts.selectWorkoutsLoading)
      .subscribe((isLoading) => {
        this.isLoading = isLoading;
      });
  }

  ngOnDestroy(): void {
    this.paramsSub.unsubscribe();
    this.providerSub.unsubscribe();
    this.programSub.unsubscribe();
    this.isLoadingSub.unsubscribe();
  }
}
