import { Component, Input } from '@angular/core';
import { Workout } from '../../models';

@Component({
  selector: 'app-workout-list-item',
  templateUrl: './workout-list-item.component.html',
  styleUrls: ['./workout-list-item.component.scss'],
})
export class WorkoutListItemComponent {
  @Input() workout: Workout;
}
