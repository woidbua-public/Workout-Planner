import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { Program } from 'src/app/programs/models';
import { Provider } from 'src/app/providers/models';
import { WorkoutsActions } from '../../actions';
import * as fromWorkouts from '../../reducers';

@Component({
  selector: 'app-workout-filter',
  templateUrl: './workout-filter.component.html',
})
export class WorkoutFilterComponent implements OnInit, OnDestroy {
  @Input() provider: Provider;
  @Input() program: Program;
  querySub: Subscription;
  sortOrder = 'desc';
  bsRangeValue: Date[];

  constructor(private store: Store<fromWorkouts.State>) {}

  ngOnInit(): void {
    this.querySub = this.store
      .select(fromWorkouts.selectWorkoutsQuery)
      .subscribe((query) => {
        this.sortOrder = query.sortOrder;
        this.bsRangeValue = [query.startDate, query.endDate];
      });
  }

  onSearch(): void {
    let startDate = null;
    let endDate = null;
    if (this.bsRangeValue && this.bsRangeValue.length === 2) {
      startDate = this.createUtcDate(this.bsRangeValue[0]);
      endDate = this.createUtcDate(this.bsRangeValue[1]);
    }

    this.store.dispatch(
      WorkoutsActions.loadWorkouts({
        query: {
          providerId: this.provider.id,
          programId: this.program.id,
          sortOrder: this.sortOrder,
          startDate,
          endDate,
        },
      })
    );
  }

  onClearDate(): void {
    this.bsRangeValue = [null, null];
  }

  private createUtcDate(date: Date | null): Date {
    if (date) {
      return new Date(
        Date.UTC(date.getFullYear(), date.getMonth(), date.getDate())
      );
    }

    return null;
  }

  ngOnDestroy(): void {
    this.querySub.unsubscribe();
  }
}
