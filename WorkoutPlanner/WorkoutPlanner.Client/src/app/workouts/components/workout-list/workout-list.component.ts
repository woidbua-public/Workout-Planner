import { Component, Input } from '@angular/core';
import { Workout } from '../../models';

@Component({
  selector: 'app-workout-list',
  templateUrl: './workout-list.component.html',
  styleUrls: ['./workout-list.component.scss'],
})
export class WorkoutListComponent {
  @Input() workouts: Workout[];
}
