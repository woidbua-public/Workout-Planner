import { Component, Input } from '@angular/core';
import { Program } from 'src/app/programs/models';
import { Provider } from 'src/app/providers/models';

@Component({
  selector: 'app-workout-breadcrumb',
  templateUrl: './workout-breadcrumb.component.html',
})
export class WorkoutBreadcrumbComponent {
  @Input() provider: Provider;
  @Input() program: Program;
}
