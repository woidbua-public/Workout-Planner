import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { SharedComponentsModule } from '../shared/components';
import { SafeHtmlPipe } from '../shared/pipes';
import {
  WorkoutBreadcrumbComponent,
  WorkoutFilterComponent,
  WorkoutListComponent,
  WorkoutListItemComponent
} from './components';
import { WorkoutCollectionPageComponent } from './containers';
import { WorkoutsEffects } from './effects';
import * as fromWorkouts from './reducers';
import { WorkoutsRoutingModule } from './workouts-routing.module';


export const PIPES = [SafeHtmlPipe];

export const COMPONENTS = [
  WorkoutBreadcrumbComponent,
  WorkoutFilterComponent,
  WorkoutListComponent,
  WorkoutListItemComponent,
];

export const CONTAINERS = [WorkoutCollectionPageComponent];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    WorkoutsRoutingModule,
    StoreModule.forFeature(
      fromWorkouts.workoutsFeatureKey,
      fromWorkouts.reducers
    ),
    EffectsModule.forFeature([WorkoutsEffects]),
    SharedComponentsModule,
    ButtonsModule.forRoot(),
    BsDatepickerModule.forRoot(),
  ],
  declarations: [PIPES, COMPONENTS, CONTAINERS],
})
export class WorkoutsModule {}
