import {
  Action,
  combineReducers,
  createFeatureSelector,
  createSelector
} from '@ngrx/store';
import * as fromRoot from '../../reducers';
import * as fromWorkouts from './workouts.reducer';

export const workoutsFeatureKey = 'workouts';

export interface WorkoutsState {
  [fromWorkouts.workoutsFeatureKey]: fromWorkouts.State;
}

export interface State extends fromRoot.State {
  [workoutsFeatureKey]: WorkoutsState;
}

export function reducers(state: WorkoutsState, action: Action): WorkoutsState {
  return combineReducers({
    [fromWorkouts.workoutsFeatureKey]: fromWorkouts.reducer,
  })(state, action);
}

/*
 * Workouts Selectors
 */
export const selectWorkoutsState = createFeatureSelector<State, WorkoutsState>(
  workoutsFeatureKey
);

export const selectWorkoutEntitiesState = createSelector(
  selectWorkoutsState,
  (state) => state.workouts
);

export const {
  selectIds: selectWorkoutIds,
  selectEntities: selectWorkoutEntities,
  selectAll: selectAllWorkouts,
  selectTotal: selectTotalWorkouts,
} = fromWorkouts.adapter.getSelectors(selectWorkoutEntitiesState);

export const selectWorkoutsQuery = createSelector(
  selectWorkoutEntitiesState,
  fromWorkouts.query
);

export const selectWorkoutsProviderId = createSelector(
  selectWorkoutEntitiesState,
  fromWorkouts.providerId
);

export const selectWorkoutsProgramId = createSelector(
  selectWorkoutEntitiesState,
  fromWorkouts.programId
);

export const selectWorkoutsSortOrder = createSelector(
  selectWorkoutEntitiesState,
  fromWorkouts.sortOrder
);

export const selectWorkoutsStartDate = createSelector(
  selectWorkoutEntitiesState,
  fromWorkouts.startDate
);

export const selectWorkoutsEndDate = createSelector(
  selectWorkoutEntitiesState,
  fromWorkouts.endDate
);

export const selectWorkoutsLoading = createSelector(
  selectWorkoutEntitiesState,
  fromWorkouts.loading
);

export const selectWorkoutsError = createSelector(
  selectWorkoutEntitiesState,
  fromWorkouts.error
);
