import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { WorkoutsActions } from '../actions';
import { Workout, WorkoutQuery } from '../models';

export const workoutsFeatureKey = 'workouts';

export interface State extends EntityState<Workout> {
  query: WorkoutQuery;
  loading: boolean;
  error: any;
}

export const adapter: EntityAdapter<Workout> = createEntityAdapter<Workout>({
  selectId: (workout: Workout) => workout.id,
});

export const initialState: State = adapter.getInitialState({
  query: {
    providerId: undefined,
    programId: undefined,
    sortOrder: 'desc',
    startDate: null,
    endDate: null,
  },
  loading: false,
  error: null,
});

export const reducer = createReducer(
  initialState,
  on(WorkoutsActions.loadWorkouts, (state) => ({
    ...state,
    loading: true,
    error: null,
  })),
  // tslint:disable-next-line: no-shadowed-variable
  on(WorkoutsActions.loadWorkoutsSuccess, (state, { query, workouts }) =>
    adapter.setAll(workouts, {
      ...state,
      query: {
        ...state.query,
        providerId: query.providerId,
        programId: query.programId,
        sortOrder: query.sortOrder,
        startDate: new Date(
          Math.min.apply(
            null,
            workouts.map((w) => w.date)
          )
        ),
        endDate: new Date(
          Math.max.apply(
            null,
            workouts.map((w) => w.date)
          )
        ),
      },
      loading: false,
      error: null,
    })
  ),
  on(WorkoutsActions.loadWorkoutsSkipped, (state) => ({
    ...state,
    loading: false,
  })),
  // tslint:disable-next-line: no-shadowed-variable
  on(WorkoutsActions.loadWorkoutsFailure, (state, { error }) => ({
    ...state,
    loading: false,
    error,
  })),
  on(WorkoutsActions.clearWorkouts, (state) =>
    adapter.removeAll({ ...state, error: null })
  ),
  on(WorkoutsActions.resetWorkoutQueryParams, (state) => ({
    ...state,
    query: {
      ...state.query,
      startDate: null,
      endDate: null,
    },
  }))
);

export const query = (state: State) => state.query;

export const providerId = (state: State) => state.query.providerId;

export const programId = (state: State) => state.query.programId;

export const sortOrder = (state: State) => state.query.sortOrder;

export const startDate = (state: State) => state.query.startDate;

export const endDate = (state: State) => state.query.endDate;

export const loading = (state: State) => state.loading;

export const error = (state: State) => state.error;
