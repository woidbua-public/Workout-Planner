import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { AuthActions } from '../../actions';
import * as fromAuth from '../../reducers';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
})
export class LoginFormComponent implements OnInit, OnDestroy {
  userSub: Subscription;
  isLoadingSub: Subscription;
  errorSub: Subscription;
  isLoading: boolean;
  errorMessage: string;

  private errorMessages = {
    'auth/invalid-email': 'Invalid email address.',
    'auth/user-disabled': 'The email has been disabled.',
    'auth/user-not-found': 'The email is not registered.',
    'auth/wrong-password': 'Wrong email or password. Please try again.',
  };

  constructor(private store: Store<fromAuth.State>, private router: Router) {}

  ngOnInit(): void {
    this.userSub = this.store
      .select(fromAuth.selectAuthUser)
      .subscribe((user) => {
        if (user) {
          this.router.navigate(['/']);
        }
      });

    this.isLoadingSub = this.store
      .select(fromAuth.selectAuthLoading)
      .subscribe((isLoading) => {
        this.isLoading = isLoading;
      });

    this.errorSub = this.store
      .select(fromAuth.selectAuthError)
      .subscribe((error) => {
        if (error) {
          this.errorMessage = this.errorMessages[error.code];
        } else {
          this.errorMessage = null;
        }
      });
  }

  onSubmit(form: NgForm): void {
    if (form.invalid) {
      return;
    }

    const email = form.value.email;
    const password = form.value.password;

    this.store.dispatch(AuthActions.login({ email, password }));
  }

  ngOnDestroy(): void {
    this.userSub.unsubscribe();
    this.isLoadingSub.unsubscribe();
    this.errorSub.unsubscribe();
  }
}
