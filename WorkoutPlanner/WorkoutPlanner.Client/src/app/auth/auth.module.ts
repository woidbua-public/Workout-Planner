import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { LoginFormComponent } from './components';
import { AuthPageComponent } from './containers';
import { AuthEffects } from './effects';
import * as fromAuth from './reducers';

export const COMPONENTS = [LoginFormComponent];

export const CONTAINERS = [AuthPageComponent];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    StoreModule.forFeature(fromAuth.authFeatureKey, fromAuth.reducers),
    EffectsModule.forFeature([AuthEffects]),
  ],
  declarations: [COMPONENTS, CONTAINERS],
})
export class AuthModule {}
