import {
  Action,
  combineReducers,
  createFeatureSelector,
  createSelector
} from '@ngrx/store';
import * as fromRoot from '../../reducers';
import * as fromAuth from './auth.reducer';

export const authFeatureKey = 'auth';

export interface AuthState {
  [fromAuth.authFeatureKey]: fromAuth.State;
}

export interface State extends fromRoot.State {
  [authFeatureKey]: AuthState;
}

export function reducers(state: AuthState, action: Action): AuthState {
  return combineReducers({
    [fromAuth.authFeatureKey]: fromAuth.reducer,
  })(state, action);
}

/*
 * Auth Selectors
 */
export const selectAuthState = createFeatureSelector<State, AuthState>(
  authFeatureKey
);

export const selectAuthEntitiesState = createSelector(
  selectAuthState,
  (state) => state.auth
);

export const selectAuthUser = createSelector(
  selectAuthEntitiesState,
  fromAuth.user
);

export const selectAuthLoading = createSelector(
  selectAuthEntitiesState,
  fromAuth.loading
);

export const selectAuthError = createSelector(
  selectAuthEntitiesState,
  fromAuth.error
);
