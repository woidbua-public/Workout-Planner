import { createReducer, on } from '@ngrx/store';
import { User } from 'src/app/shared/models';
import { AuthActions } from '../actions';

export const authFeatureKey = 'auth';

export interface State {
  triedAutoLogin: boolean;
  user: User | null;
  loading: boolean;
  error: any;
}

export const initialState: State = {
  triedAutoLogin: false,
  user: null,
  loading: false,
  error: null,
};

export const reducer = createReducer(
  initialState,
  on(AuthActions.login, (state) => ({
    ...state,
    loading: true,
    error: null,
  })),
  // tslint:disable-next-line: no-shadowed-variable
  on(AuthActions.loginSuccess, (state, { user }) => ({
    ...state,
    user,
    loading: false,
    error: null,
  })),
  // tslint:disable-next-line: no-shadowed-variable
  on(AuthActions.loginFailure, (state, { error }) => ({
    ...state,
    user: null,
    loading: false,
    error,
  })),
  // tslint:disable-next-line: no-shadowed-variable
  on(AuthActions.autoLoginSuccess, (state, { user }) => ({
    ...state,
    user,
    triedAutoLogin: true,
  })),
  on(AuthActions.autoLoginFailure, (state) => ({
    ...state,
    triedAutoLogin: true,
  })),
  on(AuthActions.logout, (state) => ({
    ...state,
    triedAutoLogin: false,
    user: null,
    error: null,
  }))
);

export const triedAutoLogin = (state: State) => state.triedAutoLogin;

export const user = (state: State) => state.user;

export const loading = (state: State) => state.loading;

export const error = (state: State) => state.error;
