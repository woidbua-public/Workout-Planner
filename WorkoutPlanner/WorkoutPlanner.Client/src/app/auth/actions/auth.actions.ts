import { createAction, props } from '@ngrx/store';
import { User } from 'src/app/shared/models';

export const autoLoginSuccess = createAction(
  '[Auth] Auto Login Success',
  props<{ user: User }>()
);

export const autoLoginFailure = createAction('[Auth] Auto Login Failure');

export const login = createAction(
  '[Auth] Login',
  props<{ email: string; password: string }>()
);

export const loginSuccess = createAction(
  '[Auth] Login Success',
  props<{ user: User }>()
);

export const loginFailure = createAction(
  '[Auth] Login Failure',
  props<{ error: any }>()
);

export const logout = createAction('[Auth] Logout');
