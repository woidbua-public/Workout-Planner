// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyB_JsyJn-h1ta2xEMbdeyj7eFs9DJbQNuI',
    authDomain: 'woidbua-workout-planner.firebaseapp.com',
    databaseURL: 'https://woidbua-workout-planner.firebaseio.com',
    projectId: 'woidbua-workout-planner',
    storageBucket: 'woidbua-workout-planner.appspot.com',
    messagingSenderId: '606832170773',
    appId: '1:606832170773:web:f03f6b3a3e263605ac7b1c',
    measurementId: 'G-H3X1J1SNJQ',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
