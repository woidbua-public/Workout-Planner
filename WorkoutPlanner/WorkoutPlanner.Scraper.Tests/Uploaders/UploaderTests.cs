﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using NSubstitute;
using NUnit.Framework;
using WorkoutPlanner.Core.Converters;
using WorkoutPlanner.Core.Readers;
using WorkoutPlanner.Domain.Builders;
using WorkoutPlanner.Domain.Programs;
using WorkoutPlanner.Domain.Providers;
using WorkoutPlanner.Domain.Workouts;
using WorkoutPlanner.Persistence.Converters;
using WorkoutPlanner.Persistence.Repositories;
using WorkoutPlanner.Scraper.Configs;

namespace WorkoutPlanner.Scraper.Uploaders
{
    [TestFixture]
    public class UploaderTests
    {
        [SetUp]
        public void SetUp()
        {
            _config = Substitute.For<IConfig>();
            _repository = Substitute.For<IRepository>();
            _readerFactory = Substitute.For<IReaderFactory>();

            _reader = Substitute.For<IReader>();
            _readerFactory.Create(Arg.Any<string>()).Returns(_reader);

            _converter = new JsonEntityConverter();

            _sut = Substitute.ForPartsOf<Uploader>(_config, _repository, _readerFactory, _converter);
        }


        private IConfig _config;
        private IRepository _repository;

        private IReader _reader;
        private IReaderFactory _readerFactory;

        private JsonEntityConverter _converter;

        private IUploader _sut;


        [Test]
        public async Task RepositoryShouldAddProvider()
        {
            // Arrange
            var provider = ProviderBuilder.Typical().Build();
            _reader.Read().Returns(JsonConverter.SerializeObject(provider));
            _repository.AddProvider(Arg.Any<IProvider>()).Returns(true);
            _repository.AddProgram(Arg.Any<IProgram>()).Returns(true);
            _repository.AddWorkout(Arg.Any<IWorkout>()).Returns(true);

            // Act
            await _sut.Upload();

            // Assert
            Expression<Predicate<IProvider>> comparison = p =>
                p.Name == provider.Name && p.Programs.Count == provider.Programs.Count;
            await _repository.Received(1).AddProvider(Arg.Is(comparison));
        }


        [Test]
        public void AddProviderShouldThrowExceptionWhenEntityCouldNotBeAdded()
        {
            // Arrange
            var provider = ProviderBuilder.Typical().Build();
            _reader.Read().Returns(JsonConverter.SerializeObject(provider));
            _repository.AddProvider(Arg.Any<IProvider>()).Returns(false);
            _repository.AddProgram(Arg.Any<IProgram>()).Returns(true);
            _repository.AddWorkout(Arg.Any<IWorkout>()).Returns(true);

            // Act
            Task AsyncTestDelegate() => _sut.Upload();

            // Assert

            Assert.ThrowsAsync<InvalidOperationException>(AsyncTestDelegate);
        }


        [Test]
        public async Task RepositoryShouldAddPrograms()
        {
            // Arrange
            var provider = ProviderBuilder.Typical().Build();
            _reader.Read().Returns(JsonConverter.SerializeObject(provider));
            _repository.AddProvider(Arg.Any<IProvider>()).Returns(true);
            _repository.AddProgram(Arg.Any<IProgram>()).Returns(true);
            _repository.AddWorkout(Arg.Any<IWorkout>()).Returns(true);

            // Act
            await _sut.Upload();

            // Assert
            foreach (var program in provider.Programs)
                await _repository.Received(1).AddProgram(Arg.Is<IProgram>(p => p.Name == program.Name));
        }


        [Test]
        public void AddProgramsShouldThrowExceptionWhenEntityCouldNotBeAdded()
        {
            // Arrange
            var provider = ProviderBuilder.Typical().Build();
            _reader.Read().Returns(JsonConverter.SerializeObject(provider));
            _repository.AddProvider(Arg.Any<IProvider>()).Returns(true);
            _repository.AddProgram(Arg.Any<IProgram>()).Returns(false);
            _repository.AddWorkout(Arg.Any<IWorkout>()).Returns(true);

            // Act
            Task AsyncTestDelegate() => _sut.Upload();

            // Assert

            Assert.ThrowsAsync<InvalidOperationException>(AsyncTestDelegate);
        }


        [Test]
        public async Task RepositoryShouldAddWorkouts()
        {
            // Arrange
            var provider = ProviderBuilder.Typical().Build();
            _reader.Read().Returns(JsonConverter.SerializeObject(provider));
            _repository.AddProvider(Arg.Any<IProvider>()).Returns(true);
            _repository.AddProgram(Arg.Any<IProgram>()).Returns(true);
            _repository.AddWorkout(Arg.Any<IWorkout>()).Returns(true);

            // Act
            await _sut.Upload();

            // Assert
            foreach (var workout in provider.Programs.SelectMany(p => p.Workouts))
                await _repository.Received(1).AddWorkout(Arg.Is<IWorkout>(w => w.Name == workout.Name));
        }


        [Test]
        public void AddWorkoutsShouldThrowExceptionWhenEntityCouldNotBeAdded()
        {
            // Arrange
            var provider = ProviderBuilder.Typical().Build();
            _reader.Read().Returns(JsonConverter.SerializeObject(provider));
            _repository.AddProvider(Arg.Any<IProvider>()).Returns(true);
            _repository.AddProgram(Arg.Any<IProgram>()).Returns(true);
            _repository.AddWorkout(Arg.Any<IWorkout>()).Returns(false);

            // Act
            Task AsyncTestDelegate() => _sut.Upload();

            // Assert

            Assert.ThrowsAsync<InvalidOperationException>(AsyncTestDelegate);
        }
    }
}
