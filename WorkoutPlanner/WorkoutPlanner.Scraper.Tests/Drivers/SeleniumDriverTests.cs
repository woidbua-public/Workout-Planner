﻿using FluentAssertions;
using NUnit.Framework;
using WorkoutPlanner.Core.Constants;

namespace WorkoutPlanner.Scraper.Drivers
{
    [TestFixture]
    public class SeleniumDriverTests : BaseDriverTests
    {
        [Test]
        [Explicit]
        [Category(TestCategories.SeleniumTest)]
        public override void FindElementByIdShouldReturnNullWhenNoElementFound()
        {
            // Arrange
            using var sut = new SeleniumDriver(true);

            // Act
            var actual = sut.FindElementById(string.Empty);

            // Assert
            actual.Should().BeNull();
        }


        [Test]
        [Explicit]
        [Category(TestCategories.SeleniumTest)]
        public override void FindElementByXPathShouldReturnNullWhenNoElementFound()
        {
            // Arrange
            using var sut = new SeleniumDriver(true);

            // Act
            var actual = sut.FindElementByXPath(string.Empty);

            // Assert
            actual.Should().BeNull();
        }


        [Test]
        [Explicit]
        [Category(TestCategories.SeleniumTest)]
        public override void FindElementsByXPathShouldReturnEmptyListWhenNoElementFound()
        {
            // Arrange
            using var sut = new SeleniumDriver(true);

            // Act
            var actual = sut.FindElementsByXPath(string.Empty);

            // Assert
            actual.Should().BeEmpty();
        }


        [Test]
        [Explicit]
        [Category(TestCategories.SeleniumTest)]
        public override void FindElementByCssSelectorShouldReturnNullWhenNoElementFound()
        {
            // Arrange
            using var sut = new SeleniumDriver(true);

            // Act
            var actual = sut.FindElementByCssSelector(string.Empty);

            // Assert
            actual.Should().BeNull();
        }


        [Test]
        [Explicit]
        [Category(TestCategories.SeleniumTest)]
        public override void FindElementsByCssSelectorShouldReturnEmptyListWhenNoElementFound()
        {
            // Arrange
            using var sut = new SeleniumDriver(true);

            // Act
            var actual = sut.FindElementsByCssSelector(string.Empty);

            // Assert
            actual.Should().BeEmpty();
        }
    }
}
