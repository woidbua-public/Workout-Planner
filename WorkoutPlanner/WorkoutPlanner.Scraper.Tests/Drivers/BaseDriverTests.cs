﻿using NUnit.Framework;

namespace WorkoutPlanner.Scraper.Drivers
{
    [TestFixture]
    public abstract class BaseDriverTests
    {
        [Test]
        public abstract void FindElementByIdShouldReturnNullWhenNoElementFound();


        [Test]
        public abstract void FindElementByXPathShouldReturnNullWhenNoElementFound();


        [Test]
        public abstract void FindElementsByXPathShouldReturnEmptyListWhenNoElementFound();


        [Test]
        public abstract void FindElementByCssSelectorShouldReturnNullWhenNoElementFound();


        [Test]
        public abstract void FindElementsByCssSelectorShouldReturnEmptyListWhenNoElementFound();
    }
}
