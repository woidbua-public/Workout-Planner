﻿using System;
using System.Text;
using AutoFixture;
using WorkoutPlanner.Core.Fixtures.Builders;

namespace WorkoutPlanner.Scraper.Fixtures.Builders
{
    public sealed class HtmlBuilder : Builder<string>
    {
        private Lazy<DateTime> _date = new Lazy<DateTime>(default(DateTime));
        private Lazy<string> _name = new Lazy<string>(default(string));
        private Lazy<string> _content = new Lazy<string>(default(string));
        private Lazy<string> _url = new Lazy<string>(default(string));


        protected override string GenerateInstance()
        {
            var stringBuilder = new StringBuilder();

            stringBuilder.AppendLine("<html>");
            stringBuilder.AppendLine($"<date>{_date.Value}</date>");
            stringBuilder.AppendLine($"<name>{_name.Value}</name>");
            stringBuilder.AppendLine($"<content>{_content.Value}</content>");
            stringBuilder.AppendLine($"<url>{_url.Value}</url>");
            stringBuilder.AppendLine("</html>");

            return stringBuilder.ToString();
        }


        public static HtmlBuilder Empty()
        {
            return new HtmlBuilder();
        }


        public static HtmlBuilder Random()
        {
            return Empty()
                   .WithDate(Fixture.Create<DateTime>())
                   .WithName(Fixture.Create<string>())
                   .WithContent(Fixture.Create<string>())
                   .WithUrl(Fixture.Create<string>());
        }


        public HtmlBuilder WithDate(DateTime value)
        {
            return WithDate(() => value);
        }


        public HtmlBuilder WithDate(Func<DateTime> func)
        {
            _date = new Lazy<DateTime>(func);
            return this;
        }


        public HtmlBuilder WithName(string value)
        {
            return WithName(() => value);
        }


        public HtmlBuilder WithName(Func<string> func)
        {
            _name = new Lazy<string>(func);
            return this;
        }


        public HtmlBuilder WithContent(string value)
        {
            return WithContent(() => value);
        }


        public HtmlBuilder WithContent(Func<string> func)
        {
            _content = new Lazy<string>(func);
            return this;
        }


        public HtmlBuilder WithUrl(string value)
        {
            return WithUrl(() => value);
        }


        public HtmlBuilder WithUrl(Func<string> func)
        {
            _url = new Lazy<string>(func);
            return this;
        }
    }
}
