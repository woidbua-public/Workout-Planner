﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using NSubstitute;
using NUnit.Framework;
using WorkoutPlanner.Core.Services.FileSystems;
using WorkoutPlanner.Core.Services.SleepServices;
using WorkoutPlanner.Core.Writers;
using WorkoutPlanner.Scraper.Configs;
using WorkoutPlanner.Scraper.Drivers;

namespace WorkoutPlanner.Scraper.Crawlers
{
    public class UrlCrawlerTests
    {
        private const string StartUrl = "https://start-url.com";
        private const string NextPageUrl = StartUrl + "/NextPage";
        private const string FolderPath = "FolderPath";
        private const string FolderPathHtml = "FolderPathHtml";

        private static readonly string FilePathUrls = Path.Combine(FolderPath, "urls.txt");

        private static readonly List<string> PageUrls = new List<string>
        {
            $"{StartUrl}/file1/",
            $"{StartUrl}/file2/",
            $"{StartUrl}/file3/"
        };

        private IFileSystemService _fileSystemService;
        private IDriver _driver;
        private IDriverFactory _driverFactory;
        private IConfig _config;
        private IWriter _writer;
        private IWriterFactory _writerFactory;
        private ISleepService _sleepService;

        private MockUrlCrawler _sut;


        [SetUp]
        public void SetUp()
        {
            _fileSystemService = Substitute.For<IFileSystemService>();

            _driver = Substitute.For<IDriver>();
            _driverFactory = Substitute.For<IDriverFactory>();
            _driverFactory.Create().Returns(_driver);

            _config = Substitute.For<IConfig>();
            _config.StartUrl.Returns(StartUrl);
            _config.FolderPath.Returns(FolderPath);
            _config.FolderPathHtml.Returns(FolderPathHtml);
            _config.FilePathUrls.Returns(FilePathUrls);
            _config.Login(_driver).Returns(true);

            _writer = Substitute.For<IWriter>();
            _writerFactory = Substitute.For<IWriterFactory>();
            _writerFactory.Create(FilePathUrls).Returns(_writer);

            _sleepService = Substitute.For<ISleepService>();

            _sut = new MockUrlCrawler(_config, _fileSystemService, _driverFactory, _writerFactory, _sleepService);
        }


        [Test]
        public void ShouldCreateFolderPathWhenNotExists()
        {
            // Arrange

            // Act
            _sut.Crawl();

            // Assert
            _fileSystemService.Received(1).CreateDirectory(FolderPath);
        }


        [Test]
        public void ShouldSkipCreateFolderPathWhenExists()
        {
            // Arrange
            _fileSystemService.DirectoryExists(FolderPath).Returns(true);

            // Act
            _sut.Crawl();

            // Assert
            _fileSystemService.Received(0).CreateDirectory(FolderPath);
        }


        [Test]
        public void ShouldCreateDriver()
        {
            // Arrange

            // Act
            _sut.Crawl();

            // Assert
            _driverFactory.Received(1).Create();
        }


        [Test]
        public void ShouldCancelFetchingWhenLoginFailed()
        {
            // Arrange
            _config.Login(_driver).Returns(false);

            // Act
            _sut.Crawl();

            // Assert
            _driver.Received(0).NavigateTo(StartUrl);
        }


        [Test]
        public void ShouldNavigateToStartUrl()
        {
            // Arrange

            // Act
            _sut.Crawl();

            // Assert
            _driver.Received(1).NavigateTo(StartUrl);
        }


        [Test]
        public void ShouldCreateWriterForUrlsFile()
        {
            // Arrange

            // Act
            _sut.Crawl();

            // Assert
            _writerFactory.Received(1).Create(FilePathUrls);
        }


        [Test]
        public void ShouldStoreFetchedUrls()
        {
            // Arrange

            // Act
            _sut.Crawl();

            // Assert
            _writer.Received(3).WriteLine(Arg.Any<string>());
        }


        [Test]
        public void ShouldStoreFetchedUrlsFromMultiplePages()
        {
            // Arrange
            _driver.NavigateTo(NextPageUrl).Returns(true, true, false);

            // Act
            _sut.Crawl();

            // Assert
            _writer.Received(9).WriteLine(Arg.Any<string>());
        }


        [Test]
        public void ShouldStopWhenAlreadyExistingHtmlIsReached()
        {
            // Arrange
            var htmlFile = Path.Combine(FolderPath, "file2.html");
            _fileSystemService.GetFilePaths(FolderPathHtml).Returns(new[] {htmlFile});
            _config.GenerateFilePath(PageUrls[0]).Returns("abc");
            _config.GenerateFilePath(PageUrls[1]).Returns(htmlFile);
            _config.GenerateFilePath(PageUrls[2]).Returns("abc");

            // Act
            _sut.Crawl();

            // Assert
            _writer.Received(1).WriteLine(Arg.Any<string>());
        }


        [Test]
        public void ShouldFinishFetchingWhenNoUrlsFetchedFromPage()
        {
            // Arrange
            _driver.NavigateTo(NextPageUrl).Returns(true);
            _sut.HasPageUrls = false;

            // Act
            _sut.Crawl();

            // Assert
            _writer.Received(0).WriteLine(Arg.Any<string>());
        }


        public class MockUrlCrawler : UrlCrawler
        {
            public MockUrlCrawler(
                IConfig config,
                IFileSystemService fileSystemService,
                IDriverFactory driverFactory,
                IWriterFactory writerFactory,
                ISleepService sleepService
            ) : base(config, fileSystemService, driverFactory, writerFactory, sleepService)
            {
            }


            public bool HasPageUrls { get; set; } = true;


            protected override IEnumerable<string> FetchPageUrls(IDriver driver)
            {
                return HasPageUrls ? PageUrls : Enumerable.Empty<string>();
            }


            protected override bool NavigateToNextPage(IDriver driver)
            {
                return driver.NavigateTo(NextPageUrl);
            }
        }
    }
}
