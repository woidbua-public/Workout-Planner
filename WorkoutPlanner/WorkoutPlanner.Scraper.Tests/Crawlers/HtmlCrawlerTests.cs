﻿using System.IO;
using NSubstitute;
using NUnit.Framework;
using WorkoutPlanner.Core.Readers;
using WorkoutPlanner.Core.Services.FileSystems;
using WorkoutPlanner.Core.Writers;
using WorkoutPlanner.Scraper.Configs;
using WorkoutPlanner.Scraper.Drivers;

namespace WorkoutPlanner.Scraper.Crawlers
{
    public class HtmlCrawlerTests
    {
        private const string StartUrl = "https://start-url.com";
        private const string FolderPathHtml = "FolderPathHtml";
        private const string FilePathUrls = "FilePathUrls.txt";
        private const string HtmlContent = "html";
        private const string FileName = "fileName";

        private static readonly string FilePath = Path.Combine(FolderPathHtml, FileName) + ".html";
        private static readonly string[] SampleUrls = {"url1", "url2", "url3"};

        private IFileSystemService _fileSystemService;

        private IDriver _driver;
        private IDriverFactory _driverFactory;

        private IConfig _config;

        private IReader _reader;
        private IReaderFactory _readerFactory;

        private IWriter _writer;
        private IWriterFactory _writerFactory;

        private HtmlCrawler _sut;


        [SetUp]
        public void SetUp()
        {
            _fileSystemService = Substitute.For<IFileSystemService>();

            _driver = Substitute.For<IDriver>();
            _driver.GetHtml().Returns(HtmlContent);
            _driverFactory = Substitute.For<IDriverFactory>();
            _driverFactory.Create().Returns(_driver);

            _config = Substitute.For<IConfig>();
            _config.StartUrl.Returns(StartUrl);
            _config.FolderPathHtml.Returns(FolderPathHtml);
            _config.FilePathUrls.Returns(FilePathUrls);
            _config.Login(_driver).Returns(true);
            _config.GenerateFilePath(Arg.Any<string>()).Returns(FilePath);

            _reader = Substitute.For<IReader>();
            _readerFactory = Substitute.For<IReaderFactory>();
            _readerFactory.Create(FilePathUrls).Returns(_reader);

            _writer = Substitute.For<IWriter>();
            _writerFactory = Substitute.For<IWriterFactory>();
            _writerFactory.Create(FilePath).Returns(_writer);

            _sut = Substitute.ForPartsOf<HtmlCrawler>(
                _config,
                _fileSystemService,
                _driverFactory,
                _readerFactory,
                _writerFactory
            );
        }


        [Test]
        public void ShouldCreateFolderPathHtmlWhenNotExists()
        {
            // Arrange

            // Act
            _sut.Crawl();

            // Assert
            _fileSystemService.Received(1).CreateDirectory(FolderPathHtml);
        }


        [Test]
        public void ShouldSkipCreateFolderPathHtmlWhenExists()
        {
            // Arrange
            _fileSystemService.DirectoryExists(FolderPathHtml).Returns(true);

            // Act
            _sut.Crawl();

            // Assert
            _fileSystemService.Received(0).CreateDirectory(FolderPathHtml);
        }


        [Test]
        public void ShouldCreateReaderForUrlsFile()
        {
            // Arrange

            // Act
            _sut.Crawl();

            // Assert
            _readerFactory.Received(1).Create(FilePathUrls);
        }


        [Test]
        public void ShouldCreateDriver()
        {
            // Arrange

            // Act
            _sut.Crawl();

            // Assert
            _driverFactory.Received(1).Create();
        }


        [Test]
        public void ShouldCancelFetchingWhenLoginFailed()
        {
            // Arrange
            _config.Login(_driver).Returns(false);

            // Act
            _sut.Crawl();

            // Assert
            _driver.Received(0).NavigateTo(StartUrl);
        }


        [Test]
        public void ShouldNavigateToStartUrl()
        {
            // Arrange

            // Act
            _sut.Crawl();

            // Assert
            _driver.Received(1).NavigateTo(StartUrl);
        }


        [Test]
        public void ShouldNavigateToAllUrlsInFile()
        {
            // Arrange
            SetupReaderForSampleUrls();

            // Act
            _sut.Crawl();

            // Assert
            foreach (var url in SampleUrls)
                _driver.Received(1).NavigateTo(url);
        }


        [Test]
        public void ShouldSaveHtmlFilesToHtmlFolderPath()
        {
            // Arrange
            SetupReaderForSampleUrls();

            // Act
            _sut.Crawl();

            // Assert
            _writerFactory.Received(3).Create(FilePath);
            _writer.Received(3).Write(HtmlContent);
        }


        private void SetupReaderForSampleUrls()
        {
            _reader.ReadLines().Returns(SampleUrls);
        }
    }
}
