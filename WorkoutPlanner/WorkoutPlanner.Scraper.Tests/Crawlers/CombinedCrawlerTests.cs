﻿using NSubstitute;
using NUnit.Framework;
using WorkoutPlanner.Scraper.Drivers;

namespace WorkoutPlanner.Scraper.Crawlers
{
    [TestFixture]
    public class CombinedCrawlerTests
    {
        [SetUp]
        public void SetUp()
        {
            _driverFactory = Substitute.For<IDriverFactory>();
            _urlCrawler = Substitute.For<IUrlCrawler>();
            _htmlCrawler = Substitute.For<IHtmlCrawler>();
            _sut = Substitute.ForPartsOf<CombinedCrawler>(_driverFactory, _urlCrawler, _htmlCrawler);
        }


        private IDriverFactory _driverFactory;
        private IUrlCrawler _urlCrawler;
        private IHtmlCrawler _htmlCrawler;
        private ICombinedCrawler _sut;


        [Test]
        public void CrawlUrlsShouldInvokeCrawlMethodFromUrlCrawler()
        {
            // Arrange

            // Act
            _sut.CrawlUrls();

            // Assert
            _urlCrawler.Received(1).Crawl();
        }


        [Test]
        public void CrawlHtmlShouldInvokeCrawlMethodFromHtmlCrawler()
        {
            // Arrange

            // Act
            _sut.CrawlHtml();

            // Assert
            _htmlCrawler.Received(1).Crawl();
        }


        [Test]
        public void CrawlShouldInvokeRequiredMethodsFromCrawlers()
        {
            // Arrange

            // Act
            _sut.Crawl();

            // Assert
            _urlCrawler.Received(1).Setup();
            _urlCrawler.Received(1).Login(Arg.Any<IDriver>());
            _urlCrawler.Received(1).Fetch(Arg.Any<IDriver>());
            _urlCrawler.Received(0).Crawl();
            _htmlCrawler.Received(1).Setup();
            _htmlCrawler.Received(0).Login(Arg.Any<IDriver>());
            _htmlCrawler.Received(1).Fetch(Arg.Any<IDriver>());
            _htmlCrawler.Received(0).Crawl();
        }
    }
}
