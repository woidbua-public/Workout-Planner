﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using NSubstitute;
using NSubstitute.ExceptionExtensions;
using NUnit.Framework;
using WorkoutPlanner.Core.Converters;
using WorkoutPlanner.Core.Readers;
using WorkoutPlanner.Core.Services.FileSystems;
using WorkoutPlanner.Core.Writers;
using WorkoutPlanner.Domain.Builders;
using WorkoutPlanner.Domain.Workouts;
using WorkoutPlanner.Scraper.Configs;
using WorkoutPlanner.Scraper.Extractors;
using WorkoutPlanner.Scraper.Fixtures.Builders;

namespace WorkoutPlanner.Scraper.Parsers
{
    public class HtmlParserTests
    {
        private const string FolderPathHtml = "FolderPathHtml";
        private const string FilePathParseErrors = "FilePathParseErrors.txt";
        private const string ProviderName = "Provider Name";
        private const string ProgramName = "Program Name";
        private const string ProviderSourceUrl = "Provider Source Url";
        private const string ProviderImageUrl = "Provider Image Url";
        private const string ProgramSourceUrl = "Program Source Url";
        private const string ProgramImageUrl = "Program Image Url";

        private static readonly List<string> FileNames = new List<string>
        {
            "file1.html",
            "file2.html",
            "file3.html"
        };

        private static readonly List<string> HtmlFiles =
            FileNames.Select(fn => Path.Combine(FolderPathHtml, fn)).ToList();

        private static readonly List<IWorkout> Workouts = new List<IWorkout>
        {
            WorkoutBuilder.Simple().WithSourceUrl(ProgramSourceUrl).WithImageUrl(ProgramImageUrl).Build(),
            WorkoutBuilder.Simple().WithSourceUrl(ProgramSourceUrl).WithImageUrl(ProgramImageUrl).Build(),
            WorkoutBuilder.Simple().WithSourceUrl(ProgramSourceUrl).WithImageUrl(ProgramImageUrl).Build()
        };

        private static readonly List<string> HtmlContent = new List<string>
        {
            GenerateHtml(Workouts[0]),
            GenerateHtml(Workouts[1]),
            GenerateHtml(Workouts[2])
        };

        private IConfig _config;
        private IFileSystemService _fileSystemService;

        private IReader _reader;
        private IReaderFactory _readerFactory;

        private IExtractorFactory _extractorFactory;

        private IWriter _writer;
        private IWriterFactory _writerFactory;

        private HtmlParser _sut;


        private static string GenerateHtml(IWorkout workout)
        {
            return HtmlBuilder.Empty()
                              .WithDate(workout.Date)
                              .WithName(workout.Name)
                              .WithContent(workout.Content)
                              .WithUrl(workout.Url)
                              .Build();
        }


        [SetUp]
        public void SetUp()
        {
            _config = Substitute.For<IConfig>();
            _config.FolderPathHtml.Returns(FolderPathHtml);
            _config.FilePathParseErrors.Returns(FilePathParseErrors);
            _config.ProviderName.Returns(ProviderName);
            _config.ProgramName.Returns(ProgramName);
            _config.ProviderSourceUrl.Returns(ProviderSourceUrl);
            _config.ProviderImageUrl.Returns(ProviderImageUrl);
            _config.ProgramSourceUrl.Returns(ProgramSourceUrl);
            _config.ProgramImageUrl.Returns(ProgramImageUrl);

            _fileSystemService = Substitute.For<IFileSystemService>();
            _fileSystemService.GetFilePaths(FolderPathHtml).Returns(HtmlFiles);

            _reader = Substitute.For<IReader>();
            _reader.Read().Returns(HtmlContent[0], HtmlContent.Skip(1).ToArray());
            _readerFactory = Substitute.For<IReaderFactory>();
            _readerFactory.Create(Arg.Any<string>()).Returns(_reader);

            _extractorFactory = Substitute.For<IExtractorFactory>();
            _extractorFactory.Create(Arg.Any<string>()).Returns(args => new HtmlAgilityExtractor(args.Arg<string>()));

            _writer = Substitute.For<IWriter>();
            _writerFactory = Substitute.For<IWriterFactory>();
            _writerFactory.Create(_config.FilePathJson).Returns(_writer);
            _writerFactory.Create(_config.FilePathParseErrors).Returns(_writer);

            _sut = new MockHtmlParser(_config, _fileSystemService, _readerFactory, _extractorFactory, _writerFactory);
        }


        [Test]
        public void ShouldLoadHtmlFiles()
        {
            // Arrange

            // Act
            _sut.Parse();

            // Assert
            _fileSystemService.Received(1).GetFilePaths(FolderPathHtml);
        }


        [Test]
        public void ShouldLoadHtmlContentForAllGivenHtmlFiles()
        {
            // Arrange

            // Act
            _sut.Parse();

            // Assert
            _reader.Received(HtmlFiles.Count).Read();
        }


        [Test]
        public void ShouldStoreWorkoutsToJsonFile()
        {
            // Arrange
            var program = ProgramBuilder.Default()
                                        .WithName(ProgramName)
                                        .WithSourceUrl(ProgramSourceUrl)
                                        .WithImageUrl(ProgramImageUrl)
                                        .Build();
            Workouts.ForEach(w => program.AddWorkout(w));
            var provider = ProviderBuilder.Default()
                                          .WithName(ProviderName)
                                          .WithSourceUrl(ProviderSourceUrl)
                                          .WithImageUrl(ProviderImageUrl)
                                          .Build();
            provider.AddProgram(program);
            var expected = JsonConverter.SerializeObject(provider);

            // Act
            _sut.Parse();

            // Assert
            _writer.Received(1).Write(expected);
        }


        [Test]
        public void ShouldWriteHtmlFilePathToErrorsFileWhenParsingFailed()
        {
            // Arrange
            const int htmlFileIndex = 1;
            _extractorFactory.Create(HtmlContent[htmlFileIndex]).Throws(ef => new NullReferenceException());

            // Act
            _sut.Parse();

            // Assert
            _writerFactory.Received(1).Create(FilePathParseErrors);
            _writer.Received(1).WriteLine(Arg.Is<string>(x => x.StartsWith(HtmlFiles[htmlFileIndex])));
        }


        [Test]
        public void ShouldNotAddNullWorkoutToListWhenParsingFailed()
        {
            // Arrange
            const int htmlFileIndex = 1;
            _extractorFactory.Create(HtmlContent[htmlFileIndex]).Throws(ef => new NullReferenceException());
            var program = ProgramBuilder.Default()
                                        .WithName(ProgramName)
                                        .WithSourceUrl(ProgramSourceUrl)
                                        .WithImageUrl(ProgramImageUrl)
                                        .Build();
            Workouts.Where((hf, i) => i != htmlFileIndex).ToList().ForEach(w => program.AddWorkout(w));
            var provider = ProviderBuilder.Default()
                                          .WithName(ProviderName)
                                          .WithSourceUrl(ProviderSourceUrl)
                                          .WithImageUrl(ProviderImageUrl)
                                          .Build();
            provider.AddProgram(program);
            var expected = JsonConverter.SerializeObject(provider);

            // Act
            _sut.Parse();

            // Assert
            _writerFactory.Received(1).Create(FilePathParseErrors);
            _writer.Received(1).WriteLine(Arg.Is<string>(x => x.StartsWith(HtmlFiles[htmlFileIndex])));
            _writer.Received(1).Write(expected);
        }


        public class MockHtmlParser : HtmlParser
        {
            public MockHtmlParser(
                IConfig config,
                IFileSystemService fileSystemService,
                IReaderFactory readerFactory,
                IExtractorFactory extractorFactory,
                IWriterFactory writerFactory
            ) : base(config, fileSystemService, readerFactory, extractorFactory, writerFactory)
            {
            }


            protected override string ExtractName(IExtractor extractor)
            {
                return extractor.GetInnerHtml("name");
            }


            protected override DateTime ExtractDate(IExtractor extractor)
            {
                return DateTime.Parse(extractor.GetInnerHtml("date"));
            }


            protected override string ExtractContent(IExtractor extractor)
            {
                return extractor.GetInnerHtml("content");
            }


            protected override string ExtractUrl(IExtractor extractor)
            {
                return extractor.GetInnerHtml("url");
            }
        }
    }
}
