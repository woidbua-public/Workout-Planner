﻿using FluentAssertions;
using NSubstitute;
using NUnit.Framework;
using WorkoutPlanner.Scraper.Configs;
using WorkoutPlanner.Scraper.Crawlers;
using WorkoutPlanner.Scraper.Drivers;
using WorkoutPlanner.Scraper.Parsers;
using WorkoutPlanner.Scraper.Uploaders;

namespace WorkoutPlanner.Scraper.Sources
{
    public class SourceTests
    {
        private const string ProviderName = "Provider Name";
        private const string ProgramName = "Program Name";

        private IConfig _config;
        private IDriverFactory _driverFactory;
        private IUrlCrawler _urlCrawler;
        private IHtmlCrawler _htmlCrawler;
        private ICombinedCrawler _combinedCrawler;
        private IHtmlParser _htmlParser;
        private IUploader _uploader;

        private Source _sut;


        [SetUp]
        public void SetUp()
        {
            _config = Substitute.For<IConfig>();
            _config.ProviderName.Returns(ProviderName);
            _config.ProgramName.Returns(ProgramName);
            _driverFactory = Substitute.For<IDriverFactory>();
            _urlCrawler = Substitute.For<IUrlCrawler>();
            _htmlCrawler = Substitute.For<IHtmlCrawler>();
            _combinedCrawler = Substitute.ForPartsOf<CombinedCrawler>(_driverFactory, _urlCrawler, _htmlCrawler);
            _htmlParser = Substitute.For<IHtmlParser>();
            _uploader = Substitute.For<IUploader>();

            _sut = Substitute.ForPartsOf<Source>(_config, _combinedCrawler, _htmlParser, _uploader);
        }


        [Test]
        public void NameShouldBeGeneratedCorrectly()
        {
            // Arrange

            // Act

            // Assert
            _sut.Name.Should().Be($"{ProviderName} - {ProgramName}");
        }


        [Test]
        public void ExecuteUrlCrawlerShouldInvokeCorrectCombinedCrawlerMethod()
        {
            // Arrange

            // Act
            _sut.ExecuteUrlCrawler();

            // Assert
            _combinedCrawler.Received(1).CrawlUrls();
        }


        [Test]
        public void ExecuteHtmlCrawlerShouldInvokeCorrectCombinedCrawlerMethod()
        {
            // Arrange

            // Act
            _sut.ExecuteHtmlCrawler();

            // Assert
            _combinedCrawler.Received(1).CrawlHtml();
        }


        [Test]
        public void ExecuteCrawlersShouldInvokeCorrectCombinedCrawlerMethod()
        {
            // Arrange

            // Act
            _sut.ExecuteCrawlers();

            // Assert
            _combinedCrawler.Received(1).Crawl();
        }


        [Test]
        public void ExecuteParserShouldInvokeParseMethodFromHtmlParser()
        {
            // Arrange

            // Act
            _sut.ExecuteParser();

            // Assert
            _htmlParser.Received(1).Parse();
        }


        [Test]
        public void ExecuteUploaderShouldInvokeUploadMethodFromUploader()
        {
            // Arrange

            // Act
            _sut.ExecuteUploader();

            // Assert
            _uploader.Received(1).Upload();
        }
    }
}
