﻿using System;
using NUnit.Framework;

namespace WorkoutPlanner.Scraper.Configs
{
    [TestFixture]
    public abstract class BaseConfigTests
    {
        public static readonly string BaseFolderPath =
            Environment.GetEnvironmentVariable(Resource.EnvironmentPath_WorkoutPlanner_Crawlers);


        [Test]
        public abstract void SuccessfulLoginShouldReturnTrue();


        [Test]
        public abstract void FailedLoginShouldReturnFalse();


        [Test]
        public abstract void GeneratedFilePathShouldHaveCorrectFormat();
    }
}
