﻿using System.IO;
using FluentAssertions;
using NUnit.Framework;
using WorkoutPlanner.Scraper.Drivers;

namespace WorkoutPlanner.Scraper.Configs
{
    [TestFixture]
    public class ConfigTests
    {
        [SetUp]
        public void SetUp()
        {
            _sut = new MockConfig();
        }


        private const string ConfigProviderName = "Provider Name";
        private const string ConfigProviderSourceUrl = "Config Provider Source Url";
        private const string ConfigProviderImageUrl = "Config Provider Image Url";
        private const string ConfigProgramSourceUrl = "Config Program Source Url";
        private const string ConfigProgramImageUrl = "Config Program Image Url";
        private const string ConfigProgramName = "Program Name";
        private const string ConfigSourceName = "provider-name-program-name";
        private const string ConfigBaseUrl = "Base Url";
        private const string ConfigStartUrl = "Start Url";

        private Config _sut;


        [Test]
        public void ProviderNameShouldBeDefinedCorrectly()
        {
            // Arrange

            // Act

            // Assert
            _sut.ProviderName.Should().Be(ConfigProviderName);
        }


        [Test]
        public void ProviderSourceUrlShouldBeDefinedCorrectly()
        {
            // Arrange

            // Act

            // Assert
            _sut.ProviderSourceUrl.Should().Be(ConfigProviderSourceUrl);
        }


        [Test]
        public void ProviderImageUrlShouldBeDefinedCorrectly()
        {
            // Arrange

            // Act

            // Assert
            _sut.ProviderImageUrl.Should().Be(ConfigProviderImageUrl);
        }


        [Test]
        public void ProgramSourceUrlShouldBeDefinedCorrectly()
        {
            // Arrange

            // Act

            // Assert
            _sut.ProgramSourceUrl.Should().Be(ConfigProgramSourceUrl);
        }


        [Test]
        public void ProgramImageUrlShouldBeDefinedCorrectly()
        {
            // Arrange

            // Act

            // Assert
            _sut.ProgramImageUrl.Should().Be(ConfigProgramImageUrl);
        }


        [Test]
        public void ProgramNameShouldBeDefinedCorrectly()
        {
            // Arrange

            // Act

            // Assert
            _sut.ProgramName.Should().Be(ConfigProgramName);
        }


        [Test]
        public void SourceNameShouldBeDefinedCorrectly()
        {
            // Arrange

            // Act

            // Assert
            _sut.SourceName.Should().Be(ConfigSourceName);
        }


        [Test]
        public void BaseUrlShouldBeDefinedCorrectly()
        {
            // Arrange

            // Act

            // Assert
            _sut.BaseUrl.Should().Be(ConfigBaseUrl);
        }


        [Test]
        public void StartUrlShouldBeDefinedCorrectly()
        {
            // Arrange

            // Act

            // Assert
            _sut.StartUrl.Should().Be(ConfigStartUrl);
        }


        [Test]
        public void BaseFolderPathShouldBeDefinedCorrectly()
        {
            // Arrange

            // Act

            // Assert
            _sut.BaseFolderPath.Should().NotBeEmpty();
        }


        [Test]
        public void FolderPathShouldBeDefinedCorrectly()
        {
            // Arrange

            // Act

            // Assert
            var expected = Path.Combine(BaseConfigTests.BaseFolderPath, ConfigSourceName);
            _sut.FolderPath.Should().Be(expected);
        }


        [Test]
        public void HtmlFolderPathShouldBeDefinedCorrectly()
        {
            // Arrange

            // Act

            // Assert
            var expected = Path.Combine(BaseConfigTests.BaseFolderPath, ConfigSourceName, "html");
            _sut.FolderPathHtml.Should().Be(expected);
        }


        [Test]
        public void UrlsFilePathShouldBeDefinedCorrectly()
        {
            // Arrange

            // Act

            // Assert
            var expected = Path.Combine(BaseConfigTests.BaseFolderPath, ConfigSourceName, "urls.txt");
            _sut.FilePathUrls.Should().Be(expected);
        }


        [Test]
        public void JsonFilePathShouldBeDefinedCorrectly()
        {
            // Arrange

            // Act

            // Assert
            var expected = Path.Combine(BaseConfigTests.BaseFolderPath, ConfigSourceName, "parsed.json");
            _sut.FilePathJson.Should().Be(expected);
        }


        [Test]
        public void ParseErrorsFilePathShouldBeDefinedCorrectly()
        {
            // Arrange

            // Act

            // Assert
            var expected = Path.Combine(BaseConfigTests.BaseFolderPath, ConfigSourceName, "parse_errors.txt");
            _sut.FilePathParseErrors.Should().Be(expected);
        }


        [Test]
        public void GeneratedFilePathFromFetchedUrlShouldHaveCorrectFormat()
        {
            // Arrange
            const string url = "test-url";

            // Act
            var actual = _sut.GenerateFilePath(url);

            // Assert
            var expected = Path.Combine(BaseConfigTests.BaseFolderPath, ConfigSourceName, "html", $"{url}.html");
            actual.Should().Be(expected);
        }


        private class MockConfig : Config
        {
            public override string ProviderName => ConfigProviderName;

            public override string ProviderSourceUrl => ConfigProviderSourceUrl;

            public override string ProviderImageUrl => ConfigProviderImageUrl;

            public override string ProgramSourceUrl => ConfigProgramSourceUrl;

            public override string ProgramImageUrl => ConfigProgramImageUrl;

            public override string ProgramName => ConfigProgramName;

            public override string BaseUrl => ConfigBaseUrl;

            public override string StartUrl => ConfigStartUrl;


            public override bool Login(IDriver driver)
            {
                return true;
            }


            protected override string GenerateFileName(string url)
            {
                return url;
            }
        }
    }
}
