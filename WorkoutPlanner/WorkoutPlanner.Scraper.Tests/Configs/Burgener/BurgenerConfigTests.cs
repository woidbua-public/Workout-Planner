﻿using FluentAssertions;
using NSubstitute;
using NUnit.Framework;
using WorkoutPlanner.Scraper.Drivers;

namespace WorkoutPlanner.Scraper.Configs.Burgener
{
    [TestFixture]
    public abstract class BurgenerConfigTests : BaseConfigTests
    {
        protected Config Sut;


        [Test]
        public override void SuccessfulLoginShouldReturnTrue()
        {
            // Arrange
            using var driver = Substitute.For<IDriver>();

            // Act
            var actual = Sut.Login(driver);

            // Assert
            actual.Should().BeTrue();
        }


        [Test]
        public override void FailedLoginShouldReturnFalse()
        {
            Assert.Pass("Does not require login.");
        }
    }
}
