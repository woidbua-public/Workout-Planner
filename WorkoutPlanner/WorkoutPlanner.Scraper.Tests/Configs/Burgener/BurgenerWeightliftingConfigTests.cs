﻿using System.Diagnostics.CodeAnalysis;
using System.IO;
using FluentAssertions;
using NUnit.Framework;

namespace WorkoutPlanner.Scraper.Configs.Burgener
{
    public class BurgenerWeightliftingConfigTests : BurgenerConfigTests
    {
        [SetUp]
        public void SetUp()
        {
            Sut = new BurgenerWeightliftingConfig();
        }


        [Test]
        [SuppressMessage("ReSharper", "StringLiteralTypo")]
        public override void GeneratedFilePathShouldHaveCorrectFormat()
        {
            // Arrange
            const string url =
                "https://www.burgenerstrength.com/freeweightlifting/2020/10/24/cvc4zbbalck8hpx69d9s7udey2wj44";

            // Act
            var actual = Sut.GenerateFilePath(url);

            // Assert
            var expected = Path.Combine(
                BaseFolderPath,
                Sut.SourceName,
                "html",
                "freeweightlifting_2020_10_24_cvc4zbbalck8hpx69d9s7udey2wj44.html"
            );
            actual.Should().Be(expected);
        }
    }
}
