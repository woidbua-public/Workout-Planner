﻿using System.IO;
using FluentAssertions;
using NUnit.Framework;

namespace WorkoutPlanner.Scraper.Configs.Burgener
{
    [TestFixture]
    public class BurgenerPerformanceConfigTests : BurgenerConfigTests
    {
        [SetUp]
        public void SetUp()
        {
            Sut = new BurgenerPerformanceConfig();
        }


        [Test]
        public override void GeneratedFilePathShouldHaveCorrectFormat()
        {
            // Arrange
            const string url = "https://www.burgenerstrength.com/performance/2020/9/28/week-2";

            // Act
            var actual = Sut.GenerateFilePath(url);

            // Assert
            var expected = Path.Combine(BaseFolderPath, Sut.SourceName, "html", "performance_2020_9_28_week-2.html");
            actual.Should().Be(expected);
        }
    }
}
