﻿using System.IO;
using FluentAssertions;
using NSubstitute;
using NUnit.Framework;
using WorkoutPlanner.Scraper.Drivers;

namespace WorkoutPlanner.Scraper.Configs.PushJerk
{
    public class PushJerkStandardConfigTests : BaseConfigTests
    {
        [SetUp]
        public void SetUp()
        {
            _sut = new PushJerkStandardConfig();
        }


        private Config _sut;


        [Test]
        public override void SuccessfulLoginShouldReturnTrue()
        {
            // Arrange
            using var driver = Substitute.For<IDriver>();

            // Act
            var actual = _sut.Login(driver);

            // Assert
            actual.Should().BeTrue();
        }


        [Test]
        public override void FailedLoginShouldReturnFalse()
        {
            Assert.Pass("Does not require login.");
        }


        [Test]
        public override void GeneratedFilePathShouldHaveCorrectFormat()
        {
            // Arrange
            const string url = "http://pushjerk.com/tue-oct-13-2020/";

            // Act
            var actual = _sut.GenerateFilePath(url);

            // Assert
            var expected = Path.Combine(BaseFolderPath, _sut.SourceName, "html", "tue-oct-13-2020.html");
            actual.Should().Be(expected);
        }
    }
}
