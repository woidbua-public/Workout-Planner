﻿using System.IO;
using FluentAssertions;
using NUnit.Framework;

namespace WorkoutPlanner.Scraper.Configs.Invictus
{
    [TestFixture]
    public class InvictusCompetitionConfigTests : InvictusConfigTests
    {
        [SetUp]
        public void SetUp()
        {
            Sut = new InvictusCompetitionConfig();
        }


        [Test]
        public override void GeneratedFilePathShouldHaveCorrectFormat()
        {
            // Arrange
            const string url =
                "https://www.crossfitinvictus.com/invictus-competition-program/october-9-2020-competition/";

            // Act
            var actual = Sut.GenerateFilePath(url);

            // Assert
            var expected = Path.Combine(BaseFolderPath, Sut.SourceName, "html", "october-9-2020-competition.html");
            actual.Should().Be(expected);
        }
    }
}
