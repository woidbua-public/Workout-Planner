﻿using System;
using FluentAssertions;
using NSubstitute;
using NSubstitute.ExceptionExtensions;
using NUnit.Framework;
using WorkoutPlanner.Core.Constants;
using WorkoutPlanner.Scraper.Drivers;

namespace WorkoutPlanner.Scraper.Configs.Invictus
{
    [TestFixture]
    public abstract class InvictusConfigTests : BaseConfigTests
    {
        protected Config Sut;


        [Test]
        [Explicit]
        [Category(TestCategories.SeleniumTest)]
        public override void SuccessfulLoginShouldReturnTrue()
        {
            // Arrange
            using IDriver driver = new SeleniumDriver();

            // Act
            var result = Sut.Login(driver);

            // Assert
            result.Should().BeTrue();
        }


        [Test]
        public override void FailedLoginShouldReturnFalse()
        {
            // Arrange
            using var driver = Substitute.For<IDriver>();
            driver.FindElementByXPath(Arg.Any<string>()).Throws(d => new Exception());

            // Act
            var result = Sut.Login(driver);

            // Assert
            result.Should().BeFalse();
        }
    }
}
