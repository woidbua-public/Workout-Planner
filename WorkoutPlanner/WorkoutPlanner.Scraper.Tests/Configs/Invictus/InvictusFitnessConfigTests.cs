﻿using System.IO;
using FluentAssertions;
using NSubstitute;
using NUnit.Framework;
using WorkoutPlanner.Scraper.Drivers;

namespace WorkoutPlanner.Scraper.Configs.Invictus
{
    public class InvictusFitnessConfigTests : InvictusConfigTests
    {
        [SetUp]
        public void SetUp()
        {
            Sut = new InvictusFitnessConfig();
        }


        [Test]
        public override void SuccessfulLoginShouldReturnTrue()
        {
            // Arrange
            using var driver = Substitute.For<IDriver>();

            // Act
            var actual = Sut.Login(driver);

            // Assert
            actual.Should().BeTrue();
        }


        [Test]
        public override void FailedLoginShouldReturnFalse()
        {
            Assert.Pass("Does not require login.");
        }


        [Test]
        public override void GeneratedFilePathShouldHaveCorrectFormat()
        {
            // Arrange
            const string url = "https://www.crossfitinvictus.com/wod/november-17-2020-fitness";

            // Act
            var actual = Sut.GenerateFilePath(url);

            // Assert
            var expected = Path.Combine(BaseFolderPath, Sut.SourceName, "html", "november-17-2020-fitness.html");
            actual.Should().Be(expected);
        }
    }
}
