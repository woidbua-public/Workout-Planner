﻿using System.IO;
using FluentAssertions;
using NSubstitute;
using NUnit.Framework;
using WorkoutPlanner.Scraper.Drivers;

namespace WorkoutPlanner.Scraper.Configs.Invictus
{
    public class InvictusPerformanceConfigTests : InvictusConfigTests
    {
        [SetUp]
        public void SetUp()
        {
            Sut = new InvictusPerformanceConfig();
        }


        [Test]
        public override void SuccessfulLoginShouldReturnTrue()
        {
            // Arrange
            using var driver = Substitute.For<IDriver>();

            // Act
            var actual = Sut.Login(driver);

            // Assert
            actual.Should().BeTrue();
        }


        [Test]
        public override void FailedLoginShouldReturnFalse()
        {
            Assert.Pass("Does not require login.");
        }


        [Test]
        public override void GeneratedFilePathShouldHaveCorrectFormat()
        {
            // Arrange
            const string url = "https://www.crossfitinvictus.com/wod/november-13-2020-performance";

            // Act
            var actual = Sut.GenerateFilePath(url);

            // Assert
            var expected = Path.Combine(BaseFolderPath, Sut.SourceName, "html", "november-13-2020-performance.html");
            actual.Should().Be(expected);
        }
    }
}
