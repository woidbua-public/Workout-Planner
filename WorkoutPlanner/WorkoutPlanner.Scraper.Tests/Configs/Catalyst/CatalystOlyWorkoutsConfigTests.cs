﻿using System.IO;
using FluentAssertions;
using NUnit.Framework;

namespace WorkoutPlanner.Scraper.Configs.Catalyst
{
    public class CatalystOlyWorkoutsConfigTests : CatalystConfigTests
    {
        [SetUp]
        public void SetUp()
        {
            Sut = new CatalystOlyWorkoutsConfig();
        }


        [Test]
        public override void GeneratedFilePathShouldHaveCorrectFormat()
        {
            // Arrange
            const string url = "https://www.catalystathletics.com/olympic-weightlifting-workout/5058/December-4-2020/";

            // Act
            var actual = Sut.GenerateFilePath(url);

            // Assert
            var expected = Path.Combine(BaseFolderPath, Sut.SourceName, "html", "december-4-2020.html");
            actual.Should().Be(expected);
        }
    }
}
