﻿using System;
using System.IO;
using FluentAssertions;
using NSubstitute;
using NSubstitute.ExceptionExtensions;
using NUnit.Framework;
using WorkoutPlanner.Core.Constants;
using WorkoutPlanner.Scraper.Drivers;

namespace WorkoutPlanner.Scraper.Configs.CompTrain
{
    [TestFixture]
    public class CompTrainIndividualsConfigTests : BaseConfigTests
    {
        [SetUp]
        public void SetUp()
        {
            _sut = new CompTrainIndividualsConfig();
        }


        private Config _sut;


        [Test]
        [Explicit]
        [Category(TestCategories.SeleniumTest)]
        public override void SuccessfulLoginShouldReturnTrue()
        {
            // Arrange
            using IDriver driver = new SeleniumDriver();

            // Act
            var result = _sut.Login(driver);

            // Assert
            result.Should().BeTrue();
        }


        [Test]
        public override void FailedLoginShouldReturnFalse()
        {
            // Arrange
            using var driver = Substitute.For<IDriver>();
            driver.NavigateTo(Arg.Any<string>()).Throws(d => new Exception());

            // Act
            var result = _sut.Login(driver);

            // Assert
            result.Should().BeFalse();
        }


        [Test]
        public override void GeneratedFilePathShouldHaveCorrectFormat()
        {
            // Arrange
            const string url = "https://comptrain.createsend1.com/t/d-l-ctivuy-ykxuikyc-n/#Friday_10.09.2020";

            // Act
            var actual = _sut.GenerateFilePath(url);

            // Assert
            var expected = Path.Combine(BaseFolderPath, _sut.SourceName, "html", "friday_10_09_2020.html");
            actual.Should().Be(expected);
        }


        [Test]
        public void GeneratedFilePathWithMultipleDaysShouldHaveCorrectFormat()
        {
            // Arrange
            const string url =
                "https://comptrain.createsend1.com/t/d-l-ctivuy-ykxuikyc-n/#Friday_10.09.2020#Saturday_10.10.2020";

            // Act
            var actual = _sut.GenerateFilePath(url);

            // Assert
            var expected = Path.Combine(
                BaseFolderPath,
                _sut.SourceName,
                "html",
                "friday_10_09_2020_saturday_10_10_2020.html"
            );
            actual.Should().Be(expected);
        }
    }
}
