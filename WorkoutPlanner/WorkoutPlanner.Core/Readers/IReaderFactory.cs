﻿using System.IO;

namespace WorkoutPlanner.Core.Readers
{
    public interface IReaderFactory
    {
        IReader Create(string filePath);

        IReader Create(Stream stream);
    }
}
