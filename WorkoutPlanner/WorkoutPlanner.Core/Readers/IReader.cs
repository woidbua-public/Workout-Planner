﻿using System;
using System.Collections.Generic;

namespace WorkoutPlanner.Core.Readers
{
    public interface IReader : IDisposable
    {
        IEnumerable<string> ReadLines();

        string Read();
    }
}
