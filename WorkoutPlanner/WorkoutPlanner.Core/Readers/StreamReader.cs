﻿using System;
using System.Collections.Generic;
using System.IO;

namespace WorkoutPlanner.Core.Readers
{
    public sealed class StreamReader : IReader
    {
        private readonly System.IO.StreamReader _streamReader;

        private bool _disposed;


        public StreamReader(string filePath)
        {
            _streamReader = new System.IO.StreamReader(filePath);
        }


        public StreamReader(Stream stream)
        {
            _streamReader = new System.IO.StreamReader(stream);
        }


        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        public string Read()
        {
            return _streamReader.ReadToEnd();
        }


        public IEnumerable<string> ReadLines()
        {
            while (!_streamReader.EndOfStream)
                yield return _streamReader.ReadLine();
        }


        private void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
                _streamReader?.Dispose();

            _disposed = true;
        }


        ~StreamReader() => Dispose(false);
    }
}
