﻿using System.Threading;

namespace WorkoutPlanner.Core.Services.SleepServices
{
    public sealed class ThreadSleepService : ISleepService
    {
        public void SleepMilliseconds(int milliseconds)
        {
            Thread.Sleep(milliseconds);
        }


        public void SleepSeconds(int seconds)
        {
            SleepMilliseconds(seconds * 1000);
        }
    }
}
