﻿namespace WorkoutPlanner.Core.Services.SleepServices
{
    public interface ISleepService
    {
        void SleepMilliseconds(int milliseconds);

        void SleepSeconds(int seconds);
    }
}
