﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace WorkoutPlanner.Core.Services.FileSystems
{
    // ReSharper disable once InconsistentNaming
    public sealed class IOFileSystemService : IFileSystemService
    {
        public bool FileExists(string filePath)
        {
            return File.Exists(filePath);
        }


        public void CreateFile(string filePath)
        {
            var directoryPath = Path.GetDirectoryName(filePath);
            CreateDirectory(directoryPath);
            File.Create(filePath).Close();
        }


        public bool DirectoryExists(string directoryPath)
        {
            return Directory.Exists(directoryPath);
        }


        public void CreateDirectory(string directoryPath)
        {
            Directory.CreateDirectory(directoryPath);
        }


        public IEnumerable<string> GetFilePaths(string directoryPath)
        {
            return Directory.Exists(directoryPath) ? Directory.GetFiles(directoryPath) : Enumerable.Empty<string>();
        }
    }
}
