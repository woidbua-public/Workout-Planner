﻿using System.Collections.Generic;

namespace WorkoutPlanner.Core.Services.FileSystems
{
    public interface IFileSystemService
    {
        bool FileExists(string filePath);

        void CreateFile(string filePath);

        bool DirectoryExists(string directoryPath);

        void CreateDirectory(string directoryPath);

        IEnumerable<string> GetFilePaths(string directoryPath);
    }
}
