﻿using System;
using System.Collections.Generic;
using System.IO;

namespace WorkoutPlanner.Core.Writers
{
    public sealed class StreamWriter : IWriter
    {
        private readonly System.IO.StreamWriter _streamWriter;

        private bool _disposed;


        public StreamWriter(string filePath)
        {
            _streamWriter = new System.IO.StreamWriter(filePath) {AutoFlush = true};
        }


        public StreamWriter(Stream stream)
        {
            _streamWriter = new System.IO.StreamWriter(stream) {AutoFlush = true};
        }


        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        public void Write(string content)
        {
            _streamWriter.Write(content);
        }


        public void WriteLine(string text)
        {
            _streamWriter.WriteLine(text);
        }


        public void WriteLines(IEnumerable<string> texts)
        {
            foreach (var text in texts)
                WriteLine(text);
        }


        private void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
                _streamWriter?.Dispose();

            _disposed = true;
        }


        ~StreamWriter() => Dispose(false);
    }
}
