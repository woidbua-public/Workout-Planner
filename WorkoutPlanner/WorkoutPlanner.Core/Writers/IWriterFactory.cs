﻿using System.IO;

namespace WorkoutPlanner.Core.Writers
{
    public interface IWriterFactory
    {
        IWriter Create(string filePath);

        IWriter Create(Stream stream);
    }
}
