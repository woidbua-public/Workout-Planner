﻿using System;
using System.Collections.Generic;

namespace WorkoutPlanner.Core.Writers
{
    public interface IWriter : IDisposable
    {
        void Write(string content);

        void WriteLine(string text);

        void WriteLines(IEnumerable<string> texts);
    }
}
