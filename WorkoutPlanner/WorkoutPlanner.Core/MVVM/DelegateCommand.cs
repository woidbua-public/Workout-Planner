﻿using System;

namespace WorkoutPlanner.Core.MVVM
{
    public sealed class DelegateCommand : DelegateCommandBase
    {
        private readonly Action _executeMethod;
        private readonly Func<bool> _canExecuteMethod;


        public DelegateCommand(Action executeMethod) : this(executeMethod, () => true)
        {
        }


        public DelegateCommand(Action executeMethod, Func<bool> canExecuteMethod)
        {
            if (executeMethod == null || canExecuteMethod == null)
                throw new ArgumentNullException(nameof(executeMethod), Resource.ExceptionMessage_DelegateCannotBeNull);

            _executeMethod = executeMethod;
            _canExecuteMethod = canExecuteMethod;
        }


        public bool CanExecute()
        {
            return _canExecuteMethod();
        }


        protected override bool CanExecute(object parameter)
        {
            return CanExecute();
        }


        public void Execute()
        {
            _executeMethod();
        }


        protected override void Execute(object parameter)
        {
            Execute();
        }
    }


    public sealed class DelegateCommand<T> : DelegateCommandBase
    {
        private readonly Action<T> _executeMethod;
        private readonly Func<T, bool> _canExecuteMethod;


        public DelegateCommand(Action<T> executeMethod) : this(executeMethod, (o) => true)
        {
        }


        public DelegateCommand(Action<T> executeMethod, Func<T, bool> canExecuteMethod)
        {
            if (executeMethod == null || canExecuteMethod == null)
                throw new ArgumentNullException(nameof(executeMethod), Resource.ExceptionMessage_DelegateCannotBeNull);

            _executeMethod = executeMethod;
            _canExecuteMethod = canExecuteMethod;
        }


        public bool CanExecute(T parameter)
        {
            return _canExecuteMethod(parameter);
        }


        protected override bool CanExecute(object parameter)
        {
            return CanExecute((T) parameter);
        }


        public void Execute(T parameter)
        {
            _executeMethod(parameter);
        }


        protected override void Execute(object parameter)
        {
            Execute((T) parameter);
        }
    }
}
