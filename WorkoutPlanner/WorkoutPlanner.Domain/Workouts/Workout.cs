﻿using System;
using WorkoutPlanner.Domain.Common;
using WorkoutPlanner.Domain.Programs;

namespace WorkoutPlanner.Domain.Workouts
{
    public sealed class Workout : Entity<Workout>, IWorkout
    {
        public override string Id
        {
            get
            {
                var titleSlug = GenerateTitleSlug();
                var dateSlug = GenerateDateSlug();
                var programSlug = GenerateProgramSlug();

                return SlugHelper.GenerateSlug($"{programSlug} {dateSlug} {titleSlug}".Trim());
            }
        }

        public DateTime Date { get; set; }

        public string Content { get; set; }

        public string Url { get; set; }

        public IProgram Program { get; set; }


        IWorkout ICloneable<IWorkout>.Clone()
        {
            return Clone();
        }


        public override Workout Clone()
        {
            return (Workout) MemberwiseClone();
        }


        private string GenerateTitleSlug()
        {
            return string.IsNullOrWhiteSpace(Name) ? string.Empty : SlugHelper.GenerateSlug(Name.Trim());
        }


        private string GenerateDateSlug()
        {
            return Date == DateTime.MinValue ? string.Empty : SlugHelper.GenerateSlug(Date.Date.ToShortDateString());
        }


        private string GenerateProgramSlug()
        {
            return Program == null || string.IsNullOrWhiteSpace(Program.Name)
                ? string.Empty
                : SlugHelper.GenerateSlug(Program.Name.Trim());
        }
    }
}
