﻿using System;
using Newtonsoft.Json;
using WorkoutPlanner.Domain.Common;
using WorkoutPlanner.Domain.Programs;

namespace WorkoutPlanner.Domain.Workouts
{
    public interface IWorkout : IEntity<IWorkout>
    {
        [JsonProperty(PropertyName = "date")]
        DateTime Date { get; set; }

        [JsonProperty(PropertyName = "content")]
        string Content { get; set; }

        [JsonProperty(PropertyName = "url")]
        string Url { get; set; }

        [JsonIgnore]
        [JsonProperty(PropertyName = "program")]
        IProgram Program { get; set; }
    }
}
