﻿using System;
using Slugify;

namespace WorkoutPlanner.Domain.Common
{
    public abstract class Entity<T> : IEntity<T>
        where T : Entity<T>
    {
        protected Entity()
        {
            SlugHelper = new SlugHelper();
        }


        protected SlugHelper SlugHelper { get; }

        public abstract string Id { get; }

        public string Name { get; set; }

        public string SourceUrl { get; set; }

        public string ImageUrl { get; set; }

        public abstract T Clone();


        object ICloneable.Clone()
        {
            return Clone();
        }
    }
}
