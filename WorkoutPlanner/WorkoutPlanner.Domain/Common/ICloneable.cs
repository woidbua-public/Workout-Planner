﻿using System;

namespace WorkoutPlanner.Domain.Common
{
    public interface ICloneable<out T> : ICloneable
        where T : ICloneable<T>
    {
        new T Clone();
    }
}
