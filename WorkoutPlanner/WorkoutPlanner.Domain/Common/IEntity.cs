﻿using Newtonsoft.Json;

namespace WorkoutPlanner.Domain.Common
{
    public interface IEntity<out T> : ICloneable<T>
        where T : IEntity<T>
    {
        [JsonProperty(PropertyName = "id")]
        string Id { get; }

        [JsonProperty(PropertyName = "name")]
        string Name { get; set; }

        [JsonProperty(PropertyName = "sourceUrl")]
        string SourceUrl { get; set; }

        [JsonProperty(PropertyName = "imageUrl")]
        public string ImageUrl { get; set; }
    }
}
