﻿using System.Collections.Generic;
using Newtonsoft.Json;
using WorkoutPlanner.Domain.Common;
using WorkoutPlanner.Domain.Programs;

namespace WorkoutPlanner.Domain.Providers
{
    public interface IProvider : IEntity<IProvider>
    {
        [JsonProperty(PropertyName = "programs")]
        IList<IProgram> Programs { get; }

        void AddProgram(IProgram program);

        void ClearPrograms();
    }
}
