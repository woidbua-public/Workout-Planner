﻿using System.Collections.Generic;
using WorkoutPlanner.Domain.Common;
using WorkoutPlanner.Domain.Programs;

namespace WorkoutPlanner.Domain.Providers
{
    public sealed class Provider : Entity<Provider>, IProvider
    {
        private readonly List<IProgram> _programs;


        public Provider()
        {
            _programs = new List<IProgram>();
        }


        public override string Id
        {
            get
            {
                var nameSlug = GenerateNameSlug();

                return SlugHelper.GenerateSlug($"{nameSlug}".Trim());
            }
        }

        public IList<IProgram> Programs => _programs;


        public void AddProgram(IProgram program)
        {
            program.Provider = this;
            _programs.Add(program);
        }


        public void ClearPrograms()
        {
            _programs.Clear();
        }


        IProvider ICloneable<IProvider>.Clone()
        {
            return Clone();
        }


        public override Provider Clone()
        {
            return (Provider) MemberwiseClone();
        }


        private string GenerateNameSlug()
        {
            return string.IsNullOrWhiteSpace(Name) ? string.Empty : SlugHelper.GenerateSlug(Name.Trim());
        }
    }
}
