﻿using System.Collections.Generic;
using WorkoutPlanner.Domain.Common;
using WorkoutPlanner.Domain.Providers;
using WorkoutPlanner.Domain.Workouts;

namespace WorkoutPlanner.Domain.Programs
{
    public sealed class Program : Entity<Program>, IProgram
    {
        private readonly List<IWorkout> _workouts;


        public Program()
        {
            _workouts = new List<IWorkout>();
        }


        public override string Id
        {
            get
            {
                var titleSlug = GenerateNameSlug();
                var providerSlug = GenerateProviderSlug();

                return SlugHelper.GenerateSlug($"{providerSlug} {titleSlug}".Trim());
            }
        }

        public IProvider Provider { get; set; }

        public IList<IWorkout> Workouts => _workouts;


        public void AddWorkout(IWorkout workout)
        {
            workout.Program = this;
            _workouts.Add(workout);
        }


        public void ClearWorkouts()
        {
            _workouts.Clear();
        }


        IProgram ICloneable<IProgram>.Clone()
        {
            return Clone();
        }


        public override Program Clone()
        {
            return (Program) MemberwiseClone();
        }


        private string GenerateNameSlug()
        {
            return string.IsNullOrWhiteSpace(Name) ? string.Empty : SlugHelper.GenerateSlug(Name.Trim());
        }


        private string GenerateProviderSlug()
        {
            return Provider == null || string.IsNullOrWhiteSpace(Provider.Name)
                ? string.Empty
                : SlugHelper.GenerateSlug(Provider.Name.Trim());
        }
    }
}
