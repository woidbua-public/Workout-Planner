﻿using System.Collections.Generic;
using Newtonsoft.Json;
using WorkoutPlanner.Domain.Common;
using WorkoutPlanner.Domain.Providers;
using WorkoutPlanner.Domain.Workouts;

namespace WorkoutPlanner.Domain.Programs
{
    public interface IProgram : IEntity<IProgram>
    {
        [JsonIgnore]
        [JsonProperty(PropertyName = "provider")]
        IProvider Provider { get; set; }

        [JsonProperty(PropertyName = "workouts")]
        IList<IWorkout> Workouts { get; }

        void AddWorkout(IWorkout workout);

        void ClearWorkouts();
    }
}
