﻿using System.Windows;
using Ninject;

namespace WorkoutPlanner.Scraper.UI
{
    public partial class App : Application
    {
        private void OnStartup(object sender, StartupEventArgs e)
        {
            var kernel = new StandardKernel(new Bootstrapper());
            ShowMainWindow(kernel);
        }


        private static void ShowMainWindow(StandardKernel kernel)
        {
            var window = kernel.Get<MainWindow>();
            window.DataContext = kernel.Get<MainWindowViewModel>();
            window.Show();
        }
    }
}
