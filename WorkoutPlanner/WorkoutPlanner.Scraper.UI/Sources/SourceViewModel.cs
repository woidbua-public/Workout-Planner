﻿using System;
using System.Threading.Tasks;
using WorkoutPlanner.Core.MVVM;
using WorkoutPlanner.Scraper.Sources;

namespace WorkoutPlanner.Scraper.UI.Sources
{
    public sealed class SourceViewModel : BindableBase
    {
        private readonly ISource _source;
        private readonly Action<bool> _actionIsBusy;


        public SourceViewModel(ISource source, Action<bool> actionIsBusy)
        {
            _source = source;
            _actionIsBusy = actionIsBusy;

            CrawlUrlsAsyncCommand = new AsyncDelegateCommand(CrawlUrlsAsync);
            CrawlHtmlAsyncCommand = new AsyncDelegateCommand(CrawlHtmlAsync);
            CrawlAllAsyncCommand = new AsyncDelegateCommand(CrawlAllAsync);
            ParseAsyncCommand = new AsyncDelegateCommand(ParseAsync);
            CrawlAndParseAsyncCommand = new AsyncDelegateCommand(CrawlAndParseAsync);
            UploadAsyncCommand = new AsyncDelegateCommand(UploadAsync);
            ExecuteAllAsyncCommand = new AsyncDelegateCommand(ExecuteAllAsync);
        }


        public string Name => _source.Name;

        public AsyncDelegateCommand CrawlUrlsAsyncCommand { get; }

        public AsyncDelegateCommand CrawlHtmlAsyncCommand { get; }

        public AsyncDelegateCommand CrawlAllAsyncCommand { get; }

        public AsyncDelegateCommand ParseAsyncCommand { get; }

        public AsyncDelegateCommand CrawlAndParseAsyncCommand { get; }

        public AsyncDelegateCommand UploadAsyncCommand { get; }

        public AsyncDelegateCommand ExecuteAllAsyncCommand { get; }


        private async Task CrawlUrlsAsync()
        {
            await DoActionAsync(() => _source.ExecuteUrlCrawler());
        }


        private async Task CrawlHtmlAsync()
        {
            await DoActionAsync(() => _source.ExecuteHtmlCrawler());
        }


        private async Task CrawlAllAsync()
        {
            await DoActionAsync(() => _source.ExecuteCrawlers());
        }


        private async Task ParseAsync()
        {
            await DoActionAsync(() => _source.ExecuteParser());
        }


        private async Task CrawlAndParseAsync()
        {
            await DoActionAsync(
                () =>
                {
                    _source.ExecuteCrawlers();
                    _source.ExecuteParser();
                }
            );
        }


        private async Task UploadAsync()
        {
            await DoActionAsync(() => { _source.ExecuteUploader(); });
        }


        internal async Task ExecuteAllAsync()
        {
            await DoActionAsync(
                () =>
                {
                    _source.ExecuteCrawlers();
                    _source.ExecuteParser();
                    _source.ExecuteUploader();
                }
            );
        }


        private async Task DoActionAsync(Action function)
        {
            _actionIsBusy(true);
            await Task.Run(function);
            _actionIsBusy(false);
        }
    }
}
