﻿using Ninject.Extensions.Factory;
using Ninject.Modules;
using WorkoutPlanner.Core.Readers;
using WorkoutPlanner.Core.Services.FileSystems;
using WorkoutPlanner.Core.Services.SleepServices;
using WorkoutPlanner.Core.Writers;
using WorkoutPlanner.Persistence.Converters;
using WorkoutPlanner.Persistence.Mappings.Firestore;
using WorkoutPlanner.Persistence.Repositories;
using WorkoutPlanner.Persistence.Repositories.Firestore;
using WorkoutPlanner.Scraper.Drivers;
using WorkoutPlanner.Scraper.Extractors;
using WorkoutPlanner.Scraper.Sources;
using WorkoutPlanner.Scraper.Sources.Catalyst;
using WorkoutPlanner.Scraper.Sources.CompTrain;
using WorkoutPlanner.Scraper.Sources.Invictus;
using WorkoutPlanner.Scraper.Sources.PushJerk;

namespace WorkoutPlanner.Scraper.UI
{
    public sealed class Bootstrapper : NinjectModule
    {
        public override void Load()
        {
            Bind<IFileSystemService>().To<IOFileSystemService>().InSingletonScope();
            Bind<ISleepService>().To<ThreadSleepService>().InSingletonScope();
            Bind<IJsonEntityConverter>().To<JsonEntityConverter>().InSingletonScope();
            Bind<IFirestoreMapping>().To<FirestoreMapping>().InSingletonScope();
            Bind<IRepository>().To<ProdFirestoreRepository>().InSingletonScope();

            Bind<IReaderFactory>().ToFactory();
            Bind<IReader>().To<StreamReader>();

            Bind<IWriterFactory>().ToFactory();
            Bind<IWriter>().To<StreamWriter>();

            Bind<IDriverFactory>().ToFactory();
            Bind<IDriver>().To<SeleniumDriver>();

            Bind<IExtractorFactory>().ToFactory();
            Bind<IExtractor>().To<HtmlAgilityExtractor>();

            //Bind<ISource>().To<BurgenerPerformanceSource>();
            //Bind<ISource>().To<BurgenerWeightliftingSource>();
            Bind<ISource>().To<CatalystOlyWorkoutsSource>();
            Bind<ISource>().To<CompTrainIndividualsSource>();
            Bind<ISource>().To<InvictusCompetitionSource>();
            Bind<ISource>().To<InvictusPerformanceSource>();
            Bind<ISource>().To<InvictusFitnessSource>();
            Bind<ISource>().To<PushJerkStandardSource>();
        }
    }
}
