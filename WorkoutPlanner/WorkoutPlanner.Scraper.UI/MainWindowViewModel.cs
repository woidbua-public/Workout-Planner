﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using WorkoutPlanner.Core.MVVM;
using WorkoutPlanner.Scraper.Sources;
using WorkoutPlanner.Scraper.UI.Sources;

namespace WorkoutPlanner.Scraper.UI
{
    public sealed class MainWindowViewModel : BindableBase
    {
        private bool _isBusy;


        public MainWindowViewModel(IEnumerable<ISource> sources)
        {
            Sources = new ObservableCollection<SourceViewModel>();
            foreach (var source in sources.OrderBy(s => s.Name))
                Sources.Add(new SourceViewModel(source, b => IsBusy = b));

            ExecuteEverythingAsyncCommand = new AsyncDelegateCommand(ExecuteEverythingAsync);
        }


        public bool IsBusy
        {
            get => _isBusy;
            set => SetProperty(ref _isBusy, value);
        }

        public ObservableCollection<SourceViewModel> Sources { get; }

        public AsyncDelegateCommand ExecuteEverythingAsyncCommand { get; }


        private async Task ExecuteEverythingAsync()
        {
            foreach (var source in Sources)
                await source.ExecuteAllAsync();
        }
    }
}
